CREATE
DATABASE IF NOT EXISTS vstudy;
USE
vstudy;

CREATE TABLE user
(
    id            int          NOT NULL AUTO_INCREMENT,
    code          varchar(64) unique,
    username      varchar(255) NOT NULL unique,
    password      varchar(255) NOT NULL,
    email         varchar(255),
    is_enabled    int,
    auth_provider varchar(64),
    role          varchar(64),
    status        varchar(64),
    device_token  varchar(1000),

    PRIMARY KEY (id)
)ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


CREATE TABLE user_profile
(
    id             int NOT NULL AUTO_INCREMENT,
    user_id        int NOT NULL,
    full_name      varchar(255),
    display_name   varchar(255),
    description    text,
    address        text,
    gender         varchar(32),
    age            int,
    avatar_img_url text,
    favorite_room  text,
    updated_at     datetime,

    PRIMARY KEY (id),
    CONSTRAINT fk_profile_user FOREIGN KEY (user_id) REFERENCES user (id)
)ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE user_friend
(
    id        int NOT NULL AUTO_INCREMENT,
    user_id   int NOT NULL,
    friend_id int NOT NULL,
    status    varchar(64) default 'A',

    PRIMARY KEY (id),
    CONSTRAINT fk_user_friend1 FOREIGN KEY (user_id) REFERENCES user (id),
    CONSTRAINT fk_user_friend2 FOREIGN KEY (friend_id) REFERENCES user (id)
)ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE topic
(
    id         int AUTO_INCREMENT,
    name       varchar(255),
    color      varchar(255),
    created_at datetime,
    created_by varchar(255),
    updated_at datetime,
    updated_by datetime,

    PRIMARY KEY (id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE room
(
    id            int AUTO_INCREMENT,
    name          varchar(255) NOT NULL,
    code          varchar(64)  NOT NULL,
    description   text,
    host_id       int          NOT NULL,
    room_type     varchar(64)  NOT NULL,
    capacity      int,
    total_member  int,
    version       int,
    status        varchar(64) DEFAULT 'A',
    favored_by    text,
    topics        text,
    whiteboard_id varchar(255),
    storage_id    varchar(255),
    created_at    datetime,
    created_by    varchar(255),
    updated_at    datetime,
    updated_by    varchar(255),

    PRIMARY KEY (id),
    CONSTRAINT fk_room_user FOREIGN KEY (host_id) REFERENCES user (id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE room_topic
(
    id      SERIAL,
    room_id int NOT NULL,
    topic_name  NOT NULL,

    primary key (id)
);

CREATE TABLE room_configuration
(
    id             int AUTO_INCREMENT,
    room_id        int NOT NULL,
    background_url text,
    password       varchar(255),
    timer_left     datetime,

    PRIMARY KEY (id),
    CONSTRAINT fk_room_configuration FOREIGN KEY (room_id) REFERENCES room (id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE room_timer
(
    id          int AUTO_INCREMENT,
    room_id     int NOT NULL,
    updated_at  datetime,
    status      varchar(255),
    left_hour   int,
    left_minute int,
    left_second int,

    PRIMARY KEY (id),
    CONSTRAINT fk_timer_room FOREIGN KEY (room_id) REFERENCES room (id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE notification
(
    id          int AUTO_INCREMENT,
    sender     int NOT NULL,
    receiver     int NOT NULL,
    title varchar(256),
    body varchar(1000),
    event_type varchar(64),
    event_time DATETIME,
    status      varchar(255),

    PRIMARY KEY (id),
    CONSTRAINT fk_noti_send FOREIGN KEY (sender) REFERENCES user (id),
    CONSTRAINT fk_noti_receive FOREIGN KEY (receiver) REFERENCES user (id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE room_note
(
    id          int AUTO_INCREMENT,
    room_id     int NOT NULL,
    header      varchar(255),
    description text,
    status      varchar(255),

    PRIMARY KEY (id),
    CONSTRAINT fk_general_note_room FOREIGN KEY (room_id) REFERENCES room (id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE room_file
(
    id                int AUTO_INCREMENT,
    room_id           int NOT NULL,
    storage_folder_id varchar(255),
    file_id           varchar(255),
    created_at        datetime,
    created_by        varchar(255),
    updated_at        datetime,
    updated_by        varchar(255),

    PRIMARY KEY (id),
    CONSTRAINT fk_rs_room FOREIGN KEY (room_id) REFERENCES room (id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE task
(
    id          int AUTO_INCREMENT,
    code        VARCHAR(255),
    user_id     int NOT NULL,
    room_id     int NOT NULL,
    type        varchar(64),
    description varchar(255),
    scheme      varchar(255),
    status      varchar(255),
    created_at  datetime,
    created_by  varchar(255),
    updated_at  datetime,
    updated_by  varchar(255),

    PRIMARY KEY (id),
    CONSTRAINT fk_task_user FOREIGN KEY (user_id) REFERENCES user (id),
    CONSTRAINT fk_task_room FOREIGN KEY (room_id) REFERENCES room (id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE note
(
    id          int AUTO_INCREMENT,
    code        varchar(64) NOT NULL,
    user_id     int         NOT NULL,
    room_id     int         NOT NULL,
    header      varchar(255),
    created_at  datetime,
    created_by  varchar(255),
    updated_at  datetime,
    updated_by  varchar(255),

    description varchar(255),
    status      varchar(255),

    PRIMARY KEY (id),
    CONSTRAINT fk_note_user FOREIGN KEY (user_id) REFERENCES user (id),
    CONSTRAINT fk_note_room FOREIGN KEY (room_id) REFERENCES room (id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


CREATE TABLE channel
(
    id             int AUTO_INCREMENT,
    room_id        int         NOT NULL,
    background_url text,
    theme          varchar(64) NOT NULL,

    PRIMARY KEY (id),
    CONSTRAINT fk_channel_configuration FOREIGN KEY (room_id) REFERENCES room (id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE room_member
(
    user_id    int          NOT NULL,
    room_id    int          NOT NULL,
    role_type  varchar(255) NOT NULL,
    status     varchar(255),
    is_joining int,
    created_at datetime,
    created_by varchar(255),
    updated_at datetime,
    updated_by varchar(255),

    PRIMARY KEY (id),
    CONSTRAINT fk_room_member FOREIGN KEY (user_id) REFERENCES user (id),
    CONSTRAINT fk_room_room FOREIGN KEY (room_id) REFERENCES room (id)
)

CREATE TABLE seq_next
(
    seq_name varchar(50) NOT NULL,
    cur_val  int(11) unsigned NOT NULL,
    PRIMARY KEY (seq_name)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


DELIMITER
//
CREATE PROCEDURE seqNext(IN seqName varchar (100), OUT nextVal int)
BEGIN
START TRANSACTION;
INSERT INTO seq_next(seq_name, cur_val)
values (seqName, 1) on duplicate key
UPDATE cur_val = cur_val + 1;
SELECT cur_val
INTO nextVal
FROM seq_next
WHERE seq_name = seqName;
COMMIT;
END
