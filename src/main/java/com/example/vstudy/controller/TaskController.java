package com.example.vstudy.controller;

import com.example.vstudy.constant.ETaskStatus;
import com.example.vstudy.constant.ETaskType;
import com.example.vstudy.controller.base.BaseController;
import com.example.vstudy.model.req.CreateTaskReq;
import com.example.vstudy.model.req.DeleteTaskReq;
import com.example.vstudy.model.req.GetDoneTaskReq;
import com.example.vstudy.model.req.SearchTaskParam;
import com.example.vstudy.model.req.UpdateTaskReq;
import com.example.vstudy.service.TaskService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("v1/task")
@AllArgsConstructor
public class TaskController extends BaseController {
    private final TaskService taskService;

    @PostMapping("")
    public ResponseEntity<?> createTask(@RequestBody @Valid CreateTaskReq req) {
        return success(taskService.createTask(req));
    }

    @PutMapping("")
    public ResponseEntity<?> updateTask(@RequestBody @Valid UpdateTaskReq req) {
        taskService.updateTask(req);
        return success(null);
    }

    @PutMapping("/delete")
    public ResponseEntity<?> deleteTask(@RequestBody @Valid DeleteTaskReq deleteTaskReq) {
        taskService.deleteTask(deleteTaskReq);
        return success(null);
    }

    @PostMapping("/done")
    public ResponseEntity<?> getMemberDoneTask(@RequestBody @Valid GetDoneTaskReq req) {
        return success(taskService.getMemberDoneTask(req));
    }
    @GetMapping("")
    public ResponseEntity<?> getTaskList(
            @RequestParam(value = "room_code", required = true) String roomCode,
            @RequestParam(value = "type", required = false) ETaskType type,
            @RequestParam(value = "status", required = false) ETaskStatus status,
            @RequestParam(value = "scheme", required = false) String scheme) {

        SearchTaskParam params = SearchTaskParam.builder()
                .roomCode(roomCode)
                .type(type)
                .status(status)
                .scheme(scheme)
                .build();
        return success(taskService.getTaskList(params));
    }
}
