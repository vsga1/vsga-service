package com.example.vstudy.controller;

import com.example.vstudy.constant.ERoomType;
import com.example.vstudy.controller.base.BaseController;
import com.example.vstudy.model.req.CreateRoomReq;
import com.example.vstudy.model.req.SearchRoomReq;
import com.example.vstudy.model.req.UpdateRoomPasswordReq;
import com.example.vstudy.model.req.UpdateRoomReq;
import com.example.vstudy.model.res.RoomRes;
import com.example.vstudy.service.RoomService;
import com.example.vstudy.service.StorageService;
import com.example.vstudy.support.resolver.OffsetPageable;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.time.OffsetDateTime;
import java.util.Set;

@RestController
@RequestMapping("v1/room")
@AllArgsConstructor
@Validated
public class RoomController extends BaseController {
    private final RoomService roomService;

    private final StorageService storageService;


    @GetMapping("/search")
    public ResponseEntity<?> searchRoom(
            @RequestParam(name = "name",required = false) String name,
            @RequestParam(name = "topics",required = false) Set<String> topics,
            @RequestParam(name = "code",required = false) String code,
            @RequestParam(name = "keyword",required = false) String keyword,
            @RequestParam(name = "created_from",required = false) OffsetDateTime createdFrom,
            @RequestParam(name = "created_to",required = false) OffsetDateTime createdTo,
            @RequestParam(name = "created_by",required = false) String createdBy,
            @RequestParam(name = "type",required = false) ERoomType type,
            @RequestParam(name = "is_favored",defaultValue = "false",required = false) Boolean isFavored,
            @RequestParam(name = "is_my_room",defaultValue = "false",required = false) Boolean isMyRoom,
            @OffsetPageable Pageable pageReq) {

        SearchRoomReq searchRoomReq = SearchRoomReq.builder()
                .code(code)
                .topics(topics)
                .name(name)
                .keyword(keyword)
                .createdFrom(createdFrom)
                .createdTo(createdTo)
                .createdBy(createdBy)
                .roomType(type)
                .isFavored(isFavored)
                .isMyRoom(isMyRoom)
                .build();
        Page<RoomRes> roomResPage = roomService.searchRoom(searchRoomReq,pageReq);
        return success(roomResPage,pageReq);
    }

    @GetMapping("/latest")
    public ResponseEntity<?> getLatestRoom() {
        return success(roomService.getLatestRooms());
    }
    @GetMapping("/detail/{code}")
    public ResponseEntity<?> getOneRoom(@PathVariable String code) {
        return success(roomService.getRoomDetail(code));
    }

    @GetMapping("")
    public ResponseEntity<?> getRoomList(
            @OffsetPageable Pageable pageReq) {
        Page<RoomRes> roomResPage = roomService.getRoomList(pageReq);
        return success(roomResPage, pageReq);
    }

    @PostMapping("")
    public ResponseEntity<?> createRoom(@Valid @RequestBody CreateRoomReq req) throws GeneralSecurityException, IOException {
        return success(roomService.createRoom(req));
    }
    @PutMapping("/{roomCode}")
    public ResponseEntity<?> updateRoom(@PathVariable String roomCode,
                                        @Valid @RequestBody UpdateRoomReq req) {
        return success(roomService.updateRoom(roomCode,req));
    }
    @PutMapping("/{roomCode}/password")
    public ResponseEntity<?> updateRoomPassword(@PathVariable String roomCode,
                                        @Valid @RequestBody UpdateRoomPasswordReq req) {
        roomService.updateRoomPassword(roomCode,req);
        return success(null);
    }
    @GetMapping("/{roomCode}/favor")
    public ResponseEntity<?> addFavoriteRoom(@PathVariable String roomCode) {
        roomService.addFavoriteRoom(roomCode);
        return success(null);
    }
    @GetMapping("/{roomCode}/disfavor")
    public ResponseEntity<?> removeFavoriteRoom(@PathVariable String roomCode) {
        roomService.removeFavoriteRoom(roomCode);
        return success(null);
    }
    @GetMapping("/{roomCode}/resource")
    public ResponseEntity<?> getRoomResource(@PathVariable String roomCode) throws GeneralSecurityException, IOException {
        return success(roomService.getRoomResources(roomCode));
    }
    @PostMapping("/{roomCode}/resource")
    public ResponseEntity<?> createResource(@PathVariable String roomCode,
                                            @RequestBody @NotNull MultipartFile file) throws GeneralSecurityException, IOException {
        return success(roomService.createResource(roomCode,file));
    }
    @PostMapping("/{roomCode}/resource/folder")
    public ResponseEntity<?> createFolder(@PathVariable String roomCode) throws GeneralSecurityException, IOException {
        return success(roomService.createFolder(roomCode));
    }

    @DeleteMapping("/{roomCode}/resource/{fileId}")
    public ResponseEntity<?> deleteFile(@PathVariable String roomCode,
                                            @PathVariable String fileId) throws GeneralSecurityException, IOException {
        roomService.deleteFile(roomCode,fileId);
        return success(null);
    }
}
