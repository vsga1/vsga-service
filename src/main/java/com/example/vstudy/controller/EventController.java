package com.example.vstudy.controller;

import com.example.vstudy.controller.base.BaseController;
import com.example.vstudy.model.req.DemoteReq;
import com.example.vstudy.model.req.JoinRoomReq;
import com.example.vstudy.model.req.PromoteReq;
import com.example.vstudy.model.req.RoomNoteCreateReq;
import com.example.vstudy.model.req.RoomNoteUpdateReq;
import com.example.vstudy.model.req.TimerCreateReq;
import com.example.vstudy.model.req.TimerUpdateReq;
import com.example.vstudy.service.EventService;
import com.example.vstudy.service.RoomNoteService;
import com.example.vstudy.service.RoomService;
import com.example.vstudy.service.RoomTimerService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("v1/event")
@AllArgsConstructor
public class EventController extends BaseController {
    private final RoomService roomService;
    private final EventService eventService;
    private final RoomTimerService roomTimerService;
    private final RoomNoteService roomNoteService;

    @PostMapping("/join-room")
    public ResponseEntity<?> joinRoom(@RequestBody JoinRoomReq req) {
        return success(eventService.enterRoom(req));
    }

    @GetMapping("/get-member")
    public ResponseEntity<?> getCurrentMembers(@RequestParam(name = "room_code", required = true) String roomCode) {
        return success(eventService.getCurrentMembers(roomCode));
    }

    @GetMapping("/out-room")
    public ResponseEntity<?> outRoom(@RequestParam(name = "room_code", required = true) String roomCode) {
        eventService.outRoom(roomCode);
        return success(null);
    }

    @PostMapping("/timer")
    public ResponseEntity<?> createTimer(@RequestBody @Valid TimerCreateReq req) {
        return success(roomTimerService.createRoomTimer(req));
    }

    @GetMapping("/timer")
    public ResponseEntity<?> getTimer(@RequestParam(value = "room_code", required = true) String roomCode) {
        return success(roomTimerService.getRoomTimer(roomCode));
    }

    @PutMapping("/timer")
    public ResponseEntity<?> updateTimer(@RequestBody @Valid TimerUpdateReq req) {
        return success(roomTimerService.updateRoomTimer(req));
    }

    @PostMapping("/note")
    public ResponseEntity<?> createNote(@RequestBody @Valid RoomNoteCreateReq req) {
        return success(roomNoteService.createRoomNote(req));
    }

    @GetMapping("/note")
    public ResponseEntity<?> getRoomNote(@RequestParam(value = "room_code", required = true) String roomCode) {
        return success(roomNoteService.getRoomNote(roomCode));
    }

    @PutMapping("/note")
    public ResponseEntity<?> updateRoomNote(@RequestBody @Valid RoomNoteUpdateReq req) {
        return success(roomNoteService.updateRoomNote(req));
    }

    @PostMapping("/promote")
    public ResponseEntity<?> promoteMember(@RequestBody @Valid PromoteReq req) {
        eventService.promoteMember(req);
        return success(null);
    }

    @PostMapping("/demote")
    public ResponseEntity<?> demoteMember(@RequestBody @Valid DemoteReq req) {
        eventService.demoteMember(req);
        return success(null);
    }
}
