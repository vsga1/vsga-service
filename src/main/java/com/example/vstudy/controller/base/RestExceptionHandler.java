package com.example.vstudy.controller.base;

import com.example.vstudy.constant.ErrorCode;
import com.example.vstudy.exception.BusinessException;
import com.example.vstudy.exception.InvalidParameterException;
import com.example.vstudy.exception.JWTTokenException;
import com.example.vstudy.support.web.BaseResponseData;
import com.example.vstudy.support.web.ResponseSupport;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.Set;

@Order(Ordered.HIGHEST_PRECEDENCE)
@RestControllerAdvice
@Log4j2
@AllArgsConstructor
public class RestExceptionHandler {
    private final ResponseSupport responseSupport;

    @ExceptionHandler(BusinessException.class)
    public ResponseEntity<Object> handle(HttpServletRequest request, BusinessException e) {
        log.info(e.getMessage());
        return new ResponseEntity<>(responseSupport.errorResponse(e.getCode()), HttpStatus.OK);
    }

    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public ResponseEntity<Object> handle(HttpServletRequest request, HttpRequestMethodNotSupportedException e) {
        log.info(e.getMessage());
        e.printStackTrace();
        return new ResponseEntity<>(
                responseSupport.errorResponse(ErrorCode.BAD_REQUEST.getValue(), "Request method not supported"),
                HttpStatus.BAD_REQUEST);
    }
    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<Object> handle(HttpServletRequest request, HttpMessageNotReadableException e) {
        log.info(e.getMessage());
        e.printStackTrace();
        return new ResponseEntity<>(
                responseSupport.errorResponse(ErrorCode.BAD_REQUEST.getValue(), "Request body is not readable"),
                HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Object> handle(HttpServletRequest request, MethodArgumentNotValidException e) {
        log.info(e.getMessage());
        e.printStackTrace();
        String msg = e.getBindingResult().getAllErrors().get(0).getDefaultMessage();
        if(StringUtils.isEmpty(msg)) {
            msg = "Invalid request body/parameter";
        }
        return new ResponseEntity<>(
                responseSupport.errorResponse(ErrorCode.BAD_REQUEST.getValue(), msg),
                HttpStatus.BAD_REQUEST);
    }
    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<Object> handle(HttpServletRequest request, ConstraintViolationException e) {
        StringBuilder builder = new StringBuilder();
        String msg = null;
        if(e.getConstraintViolations() != null && !e.getConstraintViolations().isEmpty()) {
            Set<ConstraintViolation<?>> violations = e.getConstraintViolations();
            ConstraintViolation<?> violation = violations.stream().findFirst().orElse(null);
        }
        if(msg == null) {
            msg = "Invalid request body/parameter";
        }
        return new ResponseEntity<>(
                responseSupport.errorResponse(ErrorCode.BAD_REQUEST.getValue(), msg),
                HttpStatus.BAD_REQUEST);
    }
    @ExceptionHandler(MissingServletRequestParameterException.class)
    public ResponseEntity<Object> handle(HttpServletRequest request, MissingServletRequestParameterException e) {
        return new ResponseEntity<>( responseSupport.errorResponse(HttpStatus.BAD_REQUEST.value(), "Missing request parameter"),
                HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(InvalidDataAccessApiUsageException.class)
    public ResponseEntity<Object> handle(HttpServletRequest request, InvalidDataAccessApiUsageException e) {
        log.info(e.getMessage());
        e.printStackTrace();
        BaseResponseData baseRes = BaseResponseData.error(HttpStatus.BAD_REQUEST.value(),
                "Invalid data");
        return new ResponseEntity<>(baseRes, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(InvalidParameterException.class)
    public ResponseEntity<Object> handle(HttpServletRequest request, InvalidParameterException e) {
        String msg = e.getMessage();
        if(msg == null || msg.length() <= 0) {
            msg = "Invalid request parameter";
        }
        BaseResponseData baseRes = BaseResponseData.error(HttpStatus.BAD_REQUEST.value(),
                msg);
        return new ResponseEntity<>(baseRes, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public ResponseEntity<Object> handle(HttpServletRequest request, MethodArgumentTypeMismatchException e) {

        BaseResponseData baseRes = BaseResponseData.error(HttpStatus.BAD_REQUEST.value(), e.getMessage());
        return new ResponseEntity<>(baseRes, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handle(HttpServletRequest request, Exception e) {
        log.info(e.getMessage());
        e.printStackTrace();
        BaseResponseData baseRes = BaseResponseData.error(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Internal server error");
        return new ResponseEntity<>(baseRes, HttpStatus.INTERNAL_SERVER_ERROR);
    }
    @ExceptionHandler(JWTTokenException.class)
    public ResponseEntity<Object> handle(HttpServletRequest request, JWTTokenException e) {

        BaseResponseData baseRes = BaseResponseData.error(e.getCode());
        return new ResponseEntity<>(baseRes, HttpStatus.OK);
    }
}