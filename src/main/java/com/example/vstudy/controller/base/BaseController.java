package com.example.vstudy.controller.base;

import com.example.vstudy.constant.ErrorCode;
import com.example.vstudy.support.web.BaseResponseData;
import com.example.vstudy.support.web.ResponseSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

public abstract class BaseController {
    @Autowired
    protected ResponseSupport responseSupport;

    protected ResponseEntity<BaseResponseData> success(Object data) {
        BaseResponseData responseData = new BaseResponseData(
                responseSupport.createMeta(ErrorCode.SUCCESS.getValue()), data);
        return new ResponseEntity<>(responseData, HttpStatus.OK);
    }

    protected ResponseEntity<BaseResponseData> success(Object data, long offset, int limit, long total) {
        BaseResponseData respData = new BaseResponseData(responseSupport.createMetaList(
                ErrorCode.SUCCESS.getValue(), offset, limit, total), data);
        return new ResponseEntity<>(respData, HttpStatus.OK);
    }

    protected ResponseEntity<BaseResponseData> success(Page<?> page, Pageable pageReq) {
        List<?> resBody = null;
        long totalEle = 0L;
        if (page != null) {
            resBody = page.getContent();
            totalEle = page.getTotalElements();
        }
        BaseResponseData respData = new BaseResponseData(
                responseSupport.createMetaList(
                        ErrorCode.SUCCESS.getValue(),
                        pageReq.getOffset(),
                        pageReq.getPageSize(),
                        totalEle
                ),
                resBody
        );
        return new ResponseEntity<>(respData, HttpStatus.OK);
    }

    protected ResponseEntity<BaseResponseData> success(Page<?> page, long offset, int limit) {
        BaseResponseData respData = new BaseResponseData(responseSupport.createMetaList(ErrorCode.SUCCESS.getValue(),
                offset, limit, (int) page.getTotalElements()), page.getContent());
        return new ResponseEntity<>(respData, HttpStatus.OK);
    }
}
