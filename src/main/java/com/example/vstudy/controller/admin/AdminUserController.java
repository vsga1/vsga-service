package com.example.vstudy.controller.admin;

import com.example.vstudy.constant.ECommonStatus;
import com.example.vstudy.controller.base.BaseController;
import com.example.vstudy.model.req.SearchUserReq;
import com.example.vstudy.model.res.UserProfileRes;
import com.example.vstudy.service.AccountService;
import com.example.vstudy.support.resolver.OffsetPageable;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@RestController
@RequestMapping("v1/admin/user")
@AllArgsConstructor
@Validated
public class AdminUserController extends BaseController {

    private final AccountService accountService;

    @GetMapping("/search")
    public ResponseEntity<?> searchRoom(
            @RequestParam(name = "username", required = false) String username,
            @RequestParam(name = "code", required = false) String code,
            @RequestParam(name = "email", required = false) String email,
            @RequestParam(name = "created_from", required = false) LocalDateTime createdFrom,
            @RequestParam(name = "created_to", required = false) LocalDateTime createdTo,
            @RequestParam(name = "status", required = false) ECommonStatus status,
            @RequestParam(name = "is_enabled", required = false) Boolean isEnabled,
            @OffsetPageable Pageable pageReq) {

        SearchUserReq searchUserReq = SearchUserReq.builder()
                .code(code)
                .username(username)
                .email(email)
                .createdFrom(createdFrom)
                .createdTo(createdTo)
                .status(status)
                .isEnabled(isEnabled)
                .build();
        Page<UserProfileRes> roomResPage = accountService.searchUser(searchUserReq, pageReq);
        return success(roomResPage, pageReq);
    }

    @GetMapping("/{username}/lock")
    ResponseEntity<?> lockUser(@PathVariable String username) {
        accountService.lockUser(username);
        return success(null);
    }
    @GetMapping("/{username}/unlock")
    ResponseEntity<?> unlockUser(@PathVariable String username) {
        accountService.unlockUser(username);
        return success(null);
    }

    @GetMapping("/summary")
    public ResponseEntity<?> getSummary(@RequestParam Integer months) {
        return success(accountService.getUserSummary(months));
    }
}
