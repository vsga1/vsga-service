package com.example.vstudy.controller.admin;

import com.example.vstudy.constant.ECommonStatus;
import com.example.vstudy.controller.base.BaseController;
import com.example.vstudy.model.req.SearchUserReq;
import com.example.vstudy.model.res.TopicRes;
import com.example.vstudy.model.res.UserProfileRes;
import com.example.vstudy.service.AccountService;
import com.example.vstudy.service.TopicService;
import com.example.vstudy.support.resolver.OffsetPageable;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@RestController
@RequestMapping("v1/admin/topic")
@AllArgsConstructor
@Validated
public class AdminTopicController extends BaseController {

    private final TopicService topicService;

    @GetMapping("/search")
    public ResponseEntity<?> searchRoom(
            @RequestParam(name = "topic", required = false) String username,
            @OffsetPageable Pageable pageReq) {

        Page<TopicRes> roomResPage = topicService.searchTopicAdmin(username, pageReq);
        return success(roomResPage, pageReq);
    }

    @GetMapping("/{topic}/delete")
    ResponseEntity<?> lockUser(@PathVariable String topic) {
        topicService.lock(topic);
        return success(null);
    }
}
