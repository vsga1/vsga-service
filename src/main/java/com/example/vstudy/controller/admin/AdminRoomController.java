package com.example.vstudy.controller.admin;

import com.example.vstudy.constant.ERoomType;
import com.example.vstudy.controller.base.BaseController;
import com.example.vstudy.model.req.SearchRoomReq;
import com.example.vstudy.model.res.RoomRes;
import com.example.vstudy.service.RoomService;
import com.example.vstudy.support.resolver.OffsetPageable;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.OffsetDateTime;
import java.util.Set;

@RestController
@RequestMapping("v1/admin/room")
@AllArgsConstructor
@Validated
public class AdminRoomController extends BaseController {
    private final RoomService roomService;


    @GetMapping("/search")
    public ResponseEntity<?> searchRoom(
            @RequestParam(name = "name", required = false) String name,
            @RequestParam(name = "topics", required = false) Set<String> topics,
            @RequestParam(name = "code", required = false) String code,
            @RequestParam(name = "keyword", required = false) String keyword,
            @RequestParam(name = "created_from", required = false) OffsetDateTime createdFrom,
            @RequestParam(name = "created_to", required = false) OffsetDateTime createdTo,
            @RequestParam(name = "created_by", required = false) String createdBy,
            @RequestParam(name = "type", required = false) ERoomType type,
            @OffsetPageable Pageable pageReq) {

        SearchRoomReq searchRoomReq = SearchRoomReq.builder()
                .code(code)
                .topics(topics)
                .name(name)
                .keyword(keyword)
                .createdFrom(createdFrom)
                .createdTo(createdTo)
                .createdBy(createdBy)
                .roomType(type)
                .build();
        Page<RoomRes> roomResPage = roomService.adminSearchRoom(searchRoomReq, pageReq);
        return success(roomResPage, pageReq);
    }

    @GetMapping("{roomCode}/lock")
    public ResponseEntity<?> lockRoom(@PathVariable String roomCode) {
        roomService.lockRoom(roomCode);
        return success(null);
    }

    @GetMapping("{roomCode}/unlock")
    public ResponseEntity<?> unlockRoom(@PathVariable String roomCode) {
        roomService.unlockRoom(roomCode);
        return success(null);
    }

    @GetMapping("/summary")
    public ResponseEntity<?> getSummary() {
        return success(roomService.roomSummary());
    }
}
