package com.example.vstudy.controller;

import com.example.vstudy.controller.base.BaseController;
import com.example.vstudy.model.req.CreateAccountReq;
import com.example.vstudy.model.req.LoginReq;
import com.example.vstudy.model.req.ResetPasswordReq;
import com.example.vstudy.model.req.UpdatePasswordReq;
import com.example.vstudy.model.req.UpdateProfileReq;
import com.example.vstudy.model.req.UploadAvatarReq;
import com.example.vstudy.service.AccountService;
import com.example.vstudy.service.GoogleService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.security.GeneralSecurityException;

@RestController
@RequestMapping("v1/account")
@AllArgsConstructor
public class AccountController extends BaseController {
    private final AccountService accountService;
    private final GoogleService googleService;

    @PostMapping("/login")
    ResponseEntity<?> login(@RequestBody LoginReq req) {
        return success(accountService.login(req));
    }
    @PostMapping("/admin/login")
    ResponseEntity<?> loginAdmin(@RequestBody LoginReq req) {
        return success(accountService.loginAdmin(req));
    }
    @PostMapping("/google/login")
    ResponseEntity<?> loginGoogle(@RequestBody LoginReq req) throws GeneralSecurityException, IOException {
        return success(googleService.verifyToken(req.getToken()));
    }


    @PostMapping("/registration")
    ResponseEntity<?> registration(@RequestBody CreateAccountReq req) {
        accountService.createAccount(req);
        return success(null);
    }

    @GetMapping("/{code}")
    ResponseEntity<?> getUserProfile(@PathVariable String code) {
        return success(accountService.getUserProfile(code));
    }

    @GetMapping("/reset-password")
    ResponseEntity<?> forgotPassword(@RequestParam String email) {
        accountService.sendMailResetPassword(email);
        return success(null);
    }

    @PutMapping("")
    ResponseEntity<?> updateAccountProfile(@RequestBody UpdateProfileReq req) {
        return success(accountService.updateProfile(req));
    }

    @PostMapping("/profile/avatar")
    ResponseEntity<?> uploadAvatarImage(@RequestBody UploadAvatarReq req) {
        accountService.uploadAvatar(req);
        return success(null);
    }
    @PutMapping("/password")
    ResponseEntity<?> updateAccountPassword(@RequestBody ResetPasswordReq req) {
        accountService.handleResetPassword(req);
        return success(null);
    }

    @GetMapping("/profile")
    ResponseEntity<?> getUserProfile() {
        return success(accountService.getCurrentUserProfile());
    }

    @GetMapping("/activate")
    ResponseEntity<?> activateEmail(@RequestParam(name = "token", required = true) String token) {
        accountService.activateEmail(token);
        return success(null);
    }

    @PutMapping("/update-password")
    public ResponseEntity<?> updatePassword(@RequestBody UpdatePasswordReq req) {
        accountService.updatePassword(req);
        return success(null);
    }

    @GetMapping("/search-friend")
    public ResponseEntity<?> searchFriend(@RequestParam String name) {
        return success(accountService.searchFriend(name));
    }

    @PostMapping("/device-token")
    public ResponseEntity<?> addDeviceToken(@RequestParam String token) {
        accountService.addDeviceToken(token);
        return success(null);
    }
}
