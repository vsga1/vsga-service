package com.example.vstudy.controller;

import com.example.vstudy.controller.base.BaseController;
import com.example.vstudy.model.req.CreateNoteReq;
import com.example.vstudy.model.req.UpdateNoteReq;
import com.example.vstudy.service.NoteService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("v1/room/{roomCode}/notes")
@AllArgsConstructor
@Validated
public class NoteController extends BaseController {
    private final NoteService noteService;

    @GetMapping("")
    public ResponseEntity<?> getNoteListByRoomAndUser(@PathVariable String roomCode) {
        return success(noteService.getNoteListByRoomAndUser(roomCode));
    }

    @PostMapping("")
    public ResponseEntity<?> createNote(@Valid @RequestBody CreateNoteReq req, @PathVariable String roomCode) {
        return success( noteService.createNote(req, roomCode));
    }
    @PutMapping("")
    public ResponseEntity<?> updateNote(@Valid @RequestBody UpdateNoteReq req, @PathVariable String roomCode) {
        return success( noteService.updateNote(req, roomCode));
    }
}
