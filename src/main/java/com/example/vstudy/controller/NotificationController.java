package com.example.vstudy.controller;

import com.example.vstudy.controller.base.BaseController;
import com.example.vstudy.model.req.CreateTopicReq;
import com.example.vstudy.notification.Notice;
import com.example.vstudy.notification.NotificationService;
import com.example.vstudy.service.TopicService;
import com.example.vstudy.support.resolver.OffsetPageable;
import com.google.firebase.messaging.BatchResponse;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("v1/notification")
@AllArgsConstructor
public class NotificationController extends BaseController {
    private final NotificationService notificationService;

    @GetMapping("")
    public ResponseEntity<?> getNotifications(@OffsetPageable Pageable pageable) {
        return success(notificationService.getNotices(pageable),pageable);
    }
    @GetMapping("/unread")
    public ResponseEntity<?> getTotalUnread() {
        return success(notificationService.getTotalUnread());
    }
    @PutMapping("/{id}/update-status")
    public ResponseEntity<?> updateStatus(@PathVariable Integer id) {
        notificationService.updateStatus(id);
        return success(null);
    }

    @PutMapping("/read-all")
    public ResponseEntity<?> markReadAll() {
        notificationService.markReadAll();
        return success(null);
    }
   @PostMapping("/send")
   public ResponseEntity<?> sendNotification(@RequestBody @Valid Notice notice) {
       return success(notificationService.sendNotification(notice));
   }
    @PutMapping("/{id}/delete")
    public ResponseEntity<?> deleteNoti(@PathVariable Integer id) {
        notificationService.deleteNoti(id);
        return success(null);
    }
    @PostMapping("/add-friend")
    public ResponseEntity<?> sendAddFriendNoti(@RequestBody @Valid Notice notice) {
        return success(notificationService.addFriendNoti(notice));
    }
    @PostMapping("/add-friend/decline")
    public ResponseEntity<?> sendDeclineAddFriendNoti(@RequestBody @Valid Notice notice) {
        notificationService.declineAddFriendNoti(notice);
        return success(null);
    }
    @PostMapping("/invite")
    public ResponseEntity<?> sendInviteNoti(@RequestBody @Valid Notice notice) {
        return success(notificationService.inviteNoti(notice));
    }
}
