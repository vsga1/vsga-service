package com.example.vstudy.controller;

import com.example.vstudy.controller.base.BaseController;
import com.example.vstudy.model.req.CreateTopicReq;
import com.example.vstudy.service.TopicService;
import com.example.vstudy.support.resolver.OffsetPageable;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("v1/topic")
@AllArgsConstructor
public class TopicController extends BaseController {
    private final TopicService topicService;

    @PostMapping("")
    public ResponseEntity<?> createTopics(@RequestBody @Valid CreateTopicReq req) {
        topicService.createTopics(req);
        return success(null);
    }

    @GetMapping("")
    public ResponseEntity<?> getTopics(@RequestParam String name) {
        return success(topicService.getTopics(name));
    }
    @DeleteMapping("")
    public ResponseEntity<?> deleteTopics(@RequestParam String name) {
        topicService.deleteTopic(name);
        return success(null);
    }
    @GetMapping("/suggest")
    public ResponseEntity<?> getTopics() {
        return success(topicService.suggestTopics());
    }

}
