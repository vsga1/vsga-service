package com.example.vstudy.controller;

import com.example.vstudy.controller.base.BaseController;
import com.example.vstudy.model.req.AddFriendReq;
import com.example.vstudy.service.FriendService;
import com.example.vstudy.support.resolver.OffsetPageable;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("v1/friend")
@AllArgsConstructor
public class FriendController extends BaseController {
    private final FriendService friendService;

    @PostMapping("/add-friend")
    public ResponseEntity<?> addFriend(@RequestBody @Valid AddFriendReq req) {
        friendService.addFriend(req.getUsername());
        return success(null);
    }

    @PostMapping("/unfriend")
    public ResponseEntity<?> unfriend(@RequestBody @Valid AddFriendReq req) {
        friendService.unfriend(req.getUsername());
        return success(null);
    }

    @GetMapping("/list")
    public ResponseEntity<?> friendList(@RequestParam(name = "name",required = false) String name,
                                            @OffsetPageable Pageable pageable) {
        return success(friendService.getListFriend(pageable),pageable);
    }
}
