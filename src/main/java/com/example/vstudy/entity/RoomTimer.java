package com.example.vstudy.entity;

import com.example.vstudy.constant.ETimerStatus;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.time.OffsetDateTime;

@Entity
@Setter
@Getter
@Table(name = "room_timer")
public class RoomTimer {
    private static final long serialVersionUID = 104985989415133293L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "room_id")
    private Room room;

    @Column(name = "updated_at")
    private OffsetDateTime updatedAt;

    @Column(name = "status")
    @Convert(converter = ETimerStatus.Converter.class)
    private ETimerStatus status;

    @Column(name = "left_hour")
    private Integer leftHour;

    @Column(name = "left_minute")
    private Integer leftMinute;

    @Column(name = "left_second")
    private Integer leftSecond;

}
