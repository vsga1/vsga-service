package com.example.vstudy.entity;

import com.example.vstudy.constant.ENoteStatus;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Setter
@Getter
@Table(name = "room_note")
public class RoomNote  {
    private static final long serialVersionUID = 1760433949055987141L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "room_id")
    private Room room;

    private String description;

    private String header;

    @Column(name = "status")
    @Convert(converter = ENoteStatus.Converter.class)
    private ENoteStatus status;
}
