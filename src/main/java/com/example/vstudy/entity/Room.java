package com.example.vstudy.entity;

import com.example.vstudy.constant.ECommonStatus;
import com.example.vstudy.constant.ERoomType;
import com.example.vstudy.converter.ListStringConverter;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.time.OffsetDateTime;
import java.util.List;

@Entity
@Setter
@Getter
@Table(name = "room")
public class Room extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 104985989415133293L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String name;
    private String code;
    private String description;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "host_id")
    private User user;

    @Column(name = "room_type")
    @Convert(converter = ERoomType.Converter.class)
    private ERoomType roomType;

    private int capacity;

    private int totalMember;

    @Column(name = "favored_by")
    @Convert(converter = ListStringConverter.class)
    private List<String> favoredBy;

    @Version
    private Integer version;

    @OneToOne(mappedBy = "room")
    private RoomConfiguration configuration;
    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "room_id")
    private List<RoomTopic> topics;

    @Column(name = "storage_folder_id")
    private String storageFolderId;

    @Column(name = "whiteboard_id")
    private String whiteboardId;

    @Column(name = "status")
    @Convert(converter = ECommonStatus.Converter.class)
    private ECommonStatus status = ECommonStatus.ACTIVE;


    public void incr(int val) {
        this.totalMember += val;
    }

    public void decr(int val) {
        this.totalMember -= val;
    }
}
