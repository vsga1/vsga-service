package com.example.vstudy.entity;

import com.example.vstudy.constant.ECommonStatus;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Setter
@Getter
@Table(name = "topic")
public class Topic extends BaseEntity {
    private static final long serialVersionUID = 1987266025511898972L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String name;

    private String color;

    private String code;

}
