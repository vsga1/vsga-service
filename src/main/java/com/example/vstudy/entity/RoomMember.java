package com.example.vstudy.entity;

import com.example.vstudy.constant.ECommonStatus;
import com.example.vstudy.constant.ERoomRoleType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Setter
@Getter
@Table(name = "room_member")
public class RoomMember extends BaseEntity {
    private static final long serialVersionUID = -7655832453210595213L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User user;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "room_id")
    private Room room;

    @Column(name = "role_type")
    @Convert(converter = ERoomRoleType.Converter.class)
    private ERoomRoleType roleType;

    private Boolean isJoining;

    @Version
    private Integer version;

    @Column(name = "status")
    @Convert(converter = ECommonStatus.Converter.class)
    private ECommonStatus status = ECommonStatus.ACTIVE;
}
