package com.example.vstudy.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Setter
@Getter
@Table(name = "room_configuration")
public class RoomConfiguration implements Serializable {
    private static final long serialVersionUID = 8420985654658272036L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "room_id")
    private Room room;

    private String backgroundUrl;

    private String password;

    private LocalDateTime timer;

}
