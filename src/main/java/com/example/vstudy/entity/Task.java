package com.example.vstudy.entity;

import com.example.vstudy.constant.ETaskStatus;
import com.example.vstudy.constant.ETaskType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Setter
@Getter
@Table(name = "task")
public class Task extends BaseEntity {

    private static final long serialVersionUID = 6948740127323184045L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String code;
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User user;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "room_id")
    private Room room;

    private String description;

    @Convert(converter = ETaskStatus.Converter.class)
    private ETaskStatus status;

    @Column(name = "type")
    @Convert(converter = ETaskType.Converter.class)
    private ETaskType taskType;

    private String scheme;
}
