package com.example.vstudy.entity;

import com.example.vstudy.constant.EEventStatus;
import com.example.vstudy.constant.EEventType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.time.Instant;
import java.time.LocalDateTime;

@Entity
@Setter
@Getter
@Table(name = "notification")
public class NotificationEntity {
    private static final long serialVersionUID = 1987266025511898972L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String title;

    private String body;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "sender")
    private User sender;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "receiver")
    private User receiver;

    @Column(name = "event_type")
    @Convert(converter = EEventType.Converter.class)
    private EEventType eventType;

    private Instant eventTime;

    @Column(name = "status")
    @Convert(converter = EEventStatus.Converter.class)
    private EEventStatus status;

    private String payload;
}
