package com.example.vstudy.entity;

import com.example.vstudy.constant.EAccountRole;
import com.example.vstudy.constant.EAuthProvider;
import com.example.vstudy.constant.ECommonStatus;
import com.example.vstudy.converter.ListStringConverter;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Setter
@Getter
@Table(name = "user")
public class User extends BaseEntity {
    private static final long serialVersionUID = -9189297416482041321L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String username;

    private String password;

    private String code;

    private String email;

    private Boolean isEnabled = Boolean.FALSE;

    @Column(name = "auth_provider")
    @Convert(converter = EAuthProvider.Converter.class)
    private EAuthProvider authProvider;

    @OneToOne(mappedBy = "user")
    private UserProfile profile;

    @Column(name = "status")
    @Convert(converter = ECommonStatus.Converter.class)
    private ECommonStatus status = ECommonStatus.ACTIVE;

    @Column(name = "role")
    @Convert(converter = EAccountRole.Converter.class)
    private EAccountRole role;

    @Column(name = "device_token")
    @Convert(converter = ListStringConverter.class)
    private List<String> deviceTokens;
}
