package com.example.vstudy.entity;

import com.example.vstudy.constant.EGender;
import com.example.vstudy.converter.ListStringConverter;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Setter
@Getter
@Table(name = "user_profile")
public class UserProfile extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 14359454035849916L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @OneToOne(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id",referencedColumnName = "id")
    private User user;

    private String fullName;

    private String displayName;

    private String description;

    private String address;

    @Column(name = "gender")
    @Convert(converter = EGender.Converter.class)
    private EGender gender;

    private Integer age;

    private String avatarImgUrl;

}
