package com.example.vstudy.converter;

import org.springframework.util.CollectionUtils;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

@Converter
public class ListStringConverter implements AttributeConverter<List<String>, String> {
    private static final String END_LIST = "\\|\\|";

    @Override
    public String convertToDatabaseColumn(List<String> attributes) {
        if (CollectionUtils.isEmpty(attributes)) {
            return null;
        }
        return String.join("|", attributes);
    }

    @Override
    public List<String> convertToEntityAttribute(String dbData) {
        if (dbData == null) {
            return new ArrayList<>();
        }
        dbData = dbData.replaceAll(END_LIST, "");
        return Arrays.asList(dbData.split("\\|", -1));
    }
}
