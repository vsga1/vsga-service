package com.example.vstudy.util;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DateTimeUtil {
    public static String toDateTimeStringWithFormat(LocalDateTime localDateTime, String format) {
        try {
            return localDateTime.format(DateTimeFormatter.ofPattern(format));
        } catch (Exception ex) {
            throw new IllegalArgumentException("Invalid datetime.");
        }
    }
    public static String toDateStringWithFormat(LocalDate localDate, String format) {
        try {
            return localDate.format(DateTimeFormatter.ofPattern(format));
        } catch (Exception ex) {
            throw new IllegalArgumentException("Invalid date.");
        }
    }
}
