package com.example.vstudy.repository;

import com.example.vstudy.constant.ECommonStatus;
import com.example.vstudy.entity.Room;
import com.example.vstudy.entity.Topic;
import com.example.vstudy.entity.User;
import com.example.vstudy.model.req.SearchRoomReq;
import com.example.vstudy.model.res.RoomRes;
import org.hibernate.LockOptions;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.stereotype.Repository;

import javax.persistence.LockModeType;
import javax.persistence.QueryHint;
import java.util.List;
import java.util.Optional;

public interface CustomRoomRepository {
    Page<Room> searchRoom(SearchRoomReq req,List<Topic> topicList, Pageable pageReq);
    Page<Room> searchRoomAdmin(SearchRoomReq req, Pageable pageReq);
}
