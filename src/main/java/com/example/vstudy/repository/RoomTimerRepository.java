package com.example.vstudy.repository;

import com.example.vstudy.entity.Room;
import com.example.vstudy.entity.RoomTimer;

import java.util.Optional;

public interface RoomTimerRepository extends BaseRepository<RoomTimer, Integer> {
    Optional<RoomTimer> findByRoom(Room room);
}
