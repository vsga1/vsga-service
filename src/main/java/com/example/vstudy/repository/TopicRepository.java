package com.example.vstudy.repository;

import com.example.vstudy.entity.Room;
import com.example.vstudy.entity.Topic;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TopicRepository extends JpaRepository<Topic,Integer> {
    @Query("select t from Topic t where t.name like :topic")
    List<Topic>  findTopics(String topic);
    @Query("select t from Topic t where t.name like :topic or :topic is null")
    Page<Topic> searchTopics(String topic, Pageable pageable);

    @Query(nativeQuery = true,value = "select * from topic t limit 5")
    List<Topic>  suggestTopics();

    Optional<Topic> findTopicByName(String name);
    List<Topic> findAllByNameIn(List<String> names);


}
