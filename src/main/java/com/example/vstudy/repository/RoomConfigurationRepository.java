package com.example.vstudy.repository;

import com.example.vstudy.entity.Room;
import com.example.vstudy.entity.RoomConfiguration;
import org.springframework.stereotype.Repository;

@Repository
public interface RoomConfigurationRepository extends BaseRepository<RoomConfiguration, Integer> {
    RoomConfiguration findByRoom(Room room);
}
