package com.example.vstudy.repository;

import com.example.vstudy.entity.Room;
import com.example.vstudy.entity.RoomTopic;
import com.example.vstudy.entity.Topic;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RoomTopicRepository extends JpaRepository<RoomTopic,Integer> {
    List<RoomTopic> findAllByRoom(Room room);

    @Query("select rt.topic.name,count(rt.topic) from RoomTopic rt group by rt.topic order by count(rt.topic) desc")
    List<Object[]> summaryTopic();

    @Query("select distinct(r.room) from RoomTopic r where r.topic IN (:topics)")
    List<Room> findRoomByTopic(List<Topic> topics);

    List<RoomTopic> findAllByTopic(Topic topic);

}
