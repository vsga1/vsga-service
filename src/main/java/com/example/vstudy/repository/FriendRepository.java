package com.example.vstudy.repository;

import com.example.vstudy.constant.ECommonStatus;
import com.example.vstudy.entity.Topic;
import com.example.vstudy.entity.User;
import com.example.vstudy.entity.UserFriend;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface FriendRepository extends JpaRepository<UserFriend,Integer> {
    Optional<UserFriend>  findUserFriendByUserAndFriendAndStatus(User user, User friend,ECommonStatus status);

    List<UserFriend> getAllByUserAndStatus(User user, ECommonStatus status);

    Page<UserFriend> findUserFriendByUserAndStatus(User user,ECommonStatus status,Pageable pageable);
}
