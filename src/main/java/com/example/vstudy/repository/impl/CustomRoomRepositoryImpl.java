package com.example.vstudy.repository.impl;

import com.example.vstudy.entity.Room;
import com.example.vstudy.entity.Topic;
import com.example.vstudy.model.req.SearchRoomReq;
import com.example.vstudy.model.res.RoomRes;
import com.example.vstudy.repository.CustomRoomRepository;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.CollectionUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@AllArgsConstructor
public class CustomRoomRepositoryImpl implements CustomRoomRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Page<Room> searchRoom(SearchRoomReq req, List<Topic> topics, Pageable pageReq) {
        StringBuilder countQueryStatement = new StringBuilder(150)
                .append("SELECT COUNT(e.id) FROM Room e ");

        StringBuilder selectQueryStatement = new StringBuilder(150)
                .append("SELECT e FROM Room e ");
        if(!CollectionUtils.isEmpty(topics)) {
            countQueryStatement.append(" LEFT JOIN RoomTopic rt ON e = rt.room ");
            selectQueryStatement.append(" LEFT JOIN RoomTopic rt ON e = rt.room ");
        }
        ArrayList<String> whereClauses = new ArrayList<>();
        whereClauses.add(" WHERE e.status = 'ACTIVE' AND e.roomType = 'PUBLIC' ");

        Map<String,Object> paramsMap = new HashMap<>();
        if(StringUtils.isNotEmpty(req.getCode())) {
            whereClauses.add(" e.code = :code ");
            paramsMap.put("code",req.getCode());
        }
        if(StringUtils.isNotEmpty(req.getName())) {
            req.setName("%" + req.getName() + "%");
            whereClauses.add(" e.name LIKE :name ");
            paramsMap.put("name",req.getName());
        }
        if(StringUtils.isNotEmpty(req.getKeyword())) {
            req.setKeyword("%" + req.getKeyword() + "%");
            whereClauses.add(" ( e.name LIKE :keyword OR e.code LIKE :keyword ) ");
            paramsMap.put("keyword",req.getKeyword());
        }
        if(StringUtils.isNotEmpty(req.getCreatedBy())) {
            whereClauses.add(" e.createdBy = :createdBy ");
            paramsMap.put("createdBy",req.getCreatedBy());
        }
        if(req.getRoomType() != null) {
            whereClauses.add("  e.roomType = :roomType ");
            paramsMap.put("roomType",req.getRoomType());
        }
        if(req.getCreatedFrom() != null) {
            whereClauses.add("  e.createdAt >= :createdFrom  ");
            paramsMap.put("createdFrom",req.getCreatedFrom());
        }
        if(req.getCreatedTo() != null) {
            whereClauses.add("  e.createdAt >= :createdTo  ");
            paramsMap.put("createdTo",req.getCreatedTo());
        }
        if(Boolean.TRUE.equals(req.getIsMyRoom())) {
            whereClauses.add(" ( e.user.id =:userId1 ) ");
            paramsMap.put("userId1",req.getUserId());
        }
        if(Boolean.TRUE.equals(req.getIsFavored())) {
            whereClauses.add(" ( :username in (e.favoredBy) ) ");
            paramsMap.put("username",req.getUsername());
        }
        if(!CollectionUtils.isEmpty(topics)) {
            whereClauses.add(" rt.topic in (:topics) ");
            paramsMap.put("topics",topics);
        }

        countQueryStatement
                .append(String.join(" AND ", whereClauses));
        selectQueryStatement
                .append(String.join(" AND ", whereClauses));

        String[] sortConditions = pageReq.getSort().toString().split(":");
        selectQueryStatement
                .append(" ORDER BY e.")
                .append(sortConditions[0])
                .append(" ")
                .append(sortConditions[1]);
        TypedQuery<Room> query = entityManager.createQuery(String.valueOf(selectQueryStatement),Room.class);
        TypedQuery<Long> countQuery = entityManager.createQuery(String.valueOf(countQueryStatement),Long.class);
        for(Map.Entry<String,Object> entry : paramsMap.entrySet()) {
            query.setParameter(entry.getKey(),entry.getValue());
            countQuery.setParameter(entry.getKey(),entry.getValue());
        }
        List<Room> queryRoomList = new ArrayList<>();
        Long total = countQuery.getSingleResult();
        if(total > 0 ){
            queryRoomList = query
                    .setFirstResult((int) pageReq.getOffset())
                    .setMaxResults(pageReq.getPageSize())
                    .getResultList();
        }
        return new PageImpl<>(queryRoomList,pageReq,total);
    }

    @Override
    public Page<Room> searchRoomAdmin(SearchRoomReq req, Pageable pageReq) {
        StringBuilder countQueryStatement = new StringBuilder(150)
                .append("SELECT COUNT(e.id) FROM Room e ");
        StringBuilder selectQueryStatement = new StringBuilder(150)
                .append("SELECT e FROM Room e ");

        ArrayList<String> whereClauses = new ArrayList<>();
        whereClauses.add(" WHERE e.status != null ");

        Map<String,Object> paramsMap = new HashMap<>();
        if(StringUtils.isNotEmpty(req.getCode())) {
            whereClauses.add(" e.code = :code ");
            paramsMap.put("code",req.getCode());
        }
        if(StringUtils.isNotEmpty(req.getName())) {
            req.setName("%" + req.getName() + "%");
            whereClauses.add(" e.name LIKE :name ");
            paramsMap.put("name",req.getName());
        }
        if(StringUtils.isNotEmpty(req.getKeyword())) {
            req.setKeyword("%" + req.getKeyword() + "%");
            whereClauses.add(" ( e.name LIKE :keyword OR e.code LIKE :keyword ) ");
            paramsMap.put("keyword",req.getKeyword());
        }
        if(StringUtils.isNotEmpty(req.getCreatedBy())) {
            whereClauses.add(" e.createdBy = :createdBy ");
            paramsMap.put("createdBy",req.getCreatedBy());
        }
        if(req.getRoomType() != null) {
            whereClauses.add("  e.roomType = :roomType ");
            paramsMap.put("roomType",req.getRoomType());
        }
        if(req.getCreatedFrom() != null) {
            whereClauses.add("  e.createdAt >= :createdFrom  ");
            paramsMap.put("createdFrom",req.getCreatedFrom());
        }
        if(req.getCreatedTo() != null) {
            whereClauses.add("  e.createdAt >= :createdTo  ");
            paramsMap.put("createdTo",req.getCreatedTo());
        }
        if(Boolean.TRUE.equals(req.getIsMyRoom())) {
            whereClauses.add(" ( e.user.id =:userId1 ) ");
            paramsMap.put("userId1",req.getUserId());
        }
        if(Boolean.TRUE.equals(req.getIsFavored())) {
            whereClauses.add(" ( :username in (e.favoredBy) ) ");
            paramsMap.put("username",req.getUsername());
        }
//        String[] sortConditions = pageReq.getSort().toString().split(":");
//        selectQueryStatement
//                .append(" ORDER BY e.")
//                .append(sortConditions[0])
//                .append(" ")
//                .append(sortConditions[1]);


        countQueryStatement
                .append(String.join(" AND ", whereClauses));
        selectQueryStatement
                .append(String.join(" AND ", whereClauses));


        TypedQuery<Room> query = entityManager.createQuery(String.valueOf(selectQueryStatement),Room.class);
        TypedQuery<Long> countQuery = entityManager.createQuery(String.valueOf(countQueryStatement),Long.class);
        for(Map.Entry<String,Object> entry : paramsMap.entrySet()) {
            query.setParameter(entry.getKey(),entry.getValue());
            countQuery.setParameter(entry.getKey(),entry.getValue());
        }
        List<Room> queryRoomList = new ArrayList<>();
        Long total = countQuery.getSingleResult();
        if(total > 0 ){
            queryRoomList = query
                    .setFirstResult((int) pageReq.getOffset())
                    .setMaxResults(pageReq.getPageSize())
                    .getResultList();
        }
        return new PageImpl<>(queryRoomList,pageReq,total);
    }
}
