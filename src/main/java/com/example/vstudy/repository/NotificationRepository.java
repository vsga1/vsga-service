package com.example.vstudy.repository;

import com.example.vstudy.constant.EEventStatus;
import com.example.vstudy.constant.EEventType;
import com.example.vstudy.entity.NotificationEntity;
import com.example.vstudy.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface NotificationRepository extends BaseRepository<NotificationEntity, Integer> {
    Page<NotificationEntity> getAllByReceiverOrderByEventTimeDesc(User user, Pageable pageable);

    @Query("update NotificationEntity ne set ne.status = :status where ne in (:notificationEntities) ")
    int updateStatus(List<NotificationEntity> notificationEntities, EEventStatus status);

    int countAllByReceiverAndStatus(User user,EEventStatus status);

    @Modifying
    @Query("update NotificationEntity ne set ne.status = :status where 1 = 1")
    int updateMarkReadAll(EEventStatus status);

    @Query("select ne from NotificationEntity ne where ne.eventType =:type " +
            " and ne.status =:status " +
            " and ne.sender =:sender " +
            " and ne.receiver =:receiver " +
            " and ne.eventTime < :time")
    Optional<NotificationEntity> findNotiFriendReq(User sender,
                                                   User receiver,
                                                   EEventType type,
                                                   EEventStatus status,
                                                   Instant time);
}
