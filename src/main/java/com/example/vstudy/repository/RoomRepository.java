package com.example.vstudy.repository;

import com.example.vstudy.constant.ECommonStatus;
import com.example.vstudy.entity.Room;
import com.example.vstudy.entity.User;
import org.hibernate.LockOptions;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.stereotype.Repository;

import javax.persistence.LockModeType;
import javax.persistence.QueryHint;
import java.util.Optional;
import java.util.Set;

@Repository
public interface RoomRepository extends BaseRepository<Room, Integer>,CustomRoomRepository {
    Optional<Room> findRoomByCodeAndStatus(String code, ECommonStatus status);

    @Lock(LockModeType.WRITE)
    @QueryHints({@QueryHint(name = "javax.persistence.lock.timeout", value = LockOptions.WAIT_FOREVER + "")})
    @Query("select r from Room r where r.code =:code and r.status = :status")
    Optional<Room> lockRoom(String code, ECommonStatus status);

    Page<Room> findAllByUser(User user, Pageable pageable);

    @Query("select count(r.id) from Room r where r.status =:status")
    int countRoomByStatus(ECommonStatus status);

}
