package com.example.vstudy.repository;

import com.example.vstudy.entity.RoomFile;

import java.util.List;
import java.util.Optional;

public interface RoomFileRepository extends BaseRepository<RoomFile, Integer> {
    List<RoomFile> getRoomFilesByStorageFolderId(String folderId);

    Optional<RoomFile> findRoomFileByStorageFolderIdAndFileId(String storageFolderId, String fileId);

    boolean existsByStorageFolderIdAndFileId(String storageFolderId,String fileId);
}
