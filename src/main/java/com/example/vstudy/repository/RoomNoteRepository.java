package com.example.vstudy.repository;

import com.example.vstudy.entity.Room;
import com.example.vstudy.entity.RoomNote;

import java.util.Optional;

public interface RoomNoteRepository extends BaseRepository<RoomNote, Integer> {
    Optional<RoomNote> findByRoom(Room room);
}
