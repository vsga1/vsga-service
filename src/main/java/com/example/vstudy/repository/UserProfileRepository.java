package com.example.vstudy.repository;

import com.example.vstudy.entity.User;
import com.example.vstudy.entity.UserProfile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserProfileRepository extends JpaRepository<UserProfile,Integer> {
    UserProfile findUserProfileByUser(User user);
}
