package com.example.vstudy.repository;

import com.example.vstudy.constant.ECommonStatus;
import com.example.vstudy.entity.Room;
import com.example.vstudy.entity.RoomMember;
import com.example.vstudy.entity.User;
import org.hibernate.LockOptions;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.stereotype.Repository;

import javax.persistence.LockModeType;
import javax.persistence.QueryHint;
import java.util.List;
import java.util.Optional;

@Repository
public interface RoomMemberRepository extends JpaRepository<RoomMember,Integer> {
    Optional<RoomMember> findRoomMemberByUserAndRoomAndStatus(User user,Room room, ECommonStatus status);

    @Query("select rm from RoomMember rm where rm.room =:room and rm.roleType = 'HOST' and rm.status = 'ACTIVE'")
    RoomMember getHostByRoom(Room room);

    @Lock(LockModeType.WRITE)
    @QueryHints({@QueryHint(name = "javax.persistence.lock.timeout", value = LockOptions.WAIT_FOREVER + "")})
    @Query("select rm from RoomMember rm where rm.room = :room and rm.isJoining = true and rm.status = 'ACTIVE'")
    List<RoomMember> getCurrentMembers(Room room);

    @Lock(LockModeType.READ)
    @QueryHints({@QueryHint(name = "javax.persistence.lock.timeout", value = LockOptions.WAIT_FOREVER + "")})
    @Query("select rm from RoomMember rm where rm.room = :room and rm.user = :user and rm.status = :status")
    Optional<RoomMember> lockAndCheck(User user,Room room, ECommonStatus status);

    @Query("select rm from RoomMember rm where rm.user = :user and rm.status = :status order by rm.updatedAt DESC ")
    List<RoomMember> getRoomsByMember(User user,ECommonStatus status);

    @Query("select count(rm.id) from RoomMember rm where rm.room = :room and rm.isJoining = :isJoining and rm.status = :status")
    int countCurrentMember(Room room,Boolean isJoining,ECommonStatus status);
}
