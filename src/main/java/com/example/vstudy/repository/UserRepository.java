package com.example.vstudy.repository;

import com.example.vstudy.constant.EAccountRole;
import com.example.vstudy.constant.ECommonStatus;
import com.example.vstudy.entity.User;
import org.apache.tomcat.jni.Local;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User,Integer> {

    Optional<User> findUserByCodeAndStatus(String code, ECommonStatus status);

    @Query("select u from User u where u.username =:username " +
            " and u.role =:role " +
            " and u.status =:status ")
    Optional<User> findAdmin(String username, EAccountRole role, ECommonStatus status);

    Optional<User> findUserByEmailAndStatus(String email, ECommonStatus status);

    Optional<User> findUserByUsernameAndStatus(String username, ECommonStatus status);
    Optional<User> findUserByUsername(String username);


    @Query("select u from User u where  u.role != 'ADMIN' AND " +
            " (u.username LIKE :username OR :username IS NULL) AND " +
            " (u.code = :code OR :code IS NULL) AND " +
            " (u.email = :email OR :email IS NULL) AND " +
            " (u.status = :status OR :status IS NULL) AND " +
            " (u.createdAt >= :createdFrom OR :createdFrom IS NULL) AND " +
            " (u.createdAt <= :isEnabled OR :createdTo IS NULL) AND " +
            " (u.isEnabled =:isEnabled OR :isEnabled IS NULL)")
    Page<User> searchUser(String username,
                          String code,
                          String email,
                          ECommonStatus status,
                          LocalDateTime createdFrom,
                          LocalDateTime createdTo,
                          Boolean isEnabled,
                          Pageable pageable);
    @Query("select count(u.id) from User u where u.status =:status and u.role != 'ADMIN' ")
    int countUserByStatus(ECommonStatus status);

    @Query("select count(u.id) from User u where u.createdAt >= :createdAt and u.role != 'ADMIN' ")
    int countUserThisMonth(LocalDateTime createdAt);

    @Query("select u from User u where u.status =:status" +
            " AND ( u.username like :name " +
            " OR u.profile.displayName like :name " +
            " OR u.profile.fullName like :name )")
    List<User> searchFriend(String name, ECommonStatus status);
}
