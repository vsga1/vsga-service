package com.example.vstudy.repository;

import com.example.vstudy.constant.ECommonStatus;
import com.example.vstudy.entity.Note;
import com.example.vstudy.entity.Room;
import com.example.vstudy.entity.User;

import java.util.List;
import java.util.Optional;

public interface NoteRepository extends BaseRepository<Note, Integer> {
    List<Note> findAllByRoomAndUserAndStatus(Room room, User user, ECommonStatus status);

    Optional<Note> findNoteByCodeAndStatus(String code, ECommonStatus status);
}
