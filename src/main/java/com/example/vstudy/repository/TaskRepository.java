package com.example.vstudy.repository;

import com.example.vstudy.constant.ETaskStatus;
import com.example.vstudy.constant.ETaskType;
import com.example.vstudy.entity.Room;
import com.example.vstudy.entity.Task;
import com.example.vstudy.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TaskRepository extends JpaRepository<Task, Integer> {

    @Query("select T from Task T where T.user = :user AND T.status != 'DELETED' " +
            " and T.room.code =:roomCode " +
            " and (T.status = :status OR :status IS NULL) " +
            " and (T.taskType =:type OR :type IS NULL) " +
            " and (T.scheme =:scheme OR :scheme IS NULL)")
    List<Task> getTaskList(String roomCode,
                           User user,
                           ETaskStatus status,
                           ETaskType type,
                           String scheme);

    Optional<Task> findTaskByCode(String code);

    @Query("select T from Task T where T.room = :room AND T.status != 'DELETED' and T.scheme =:scheme")
    List<Task> findRoomTaskByScheme(Room room, String scheme);

    @Query("select T from Task T where T.room =:room and T.user =:host and T.taskType = 'ROOM_TASK' ")
    List<Task> findRoomTask(Room room,User host);


}
