package com.example.vstudy.notification;

import com.example.vstudy.constant.EEventStatus;
import com.example.vstudy.constant.EEventType;
import com.example.vstudy.constant.ErrorCode;
import com.example.vstudy.entity.NotificationEntity;
import com.example.vstudy.entity.User;
import com.example.vstudy.exception.BusinessException;
import com.example.vstudy.model.res.NoticeRes;
import com.example.vstudy.repository.NotificationRepository;
import com.example.vstudy.service.AccountService;
import com.google.firebase.messaging.BatchResponse;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.MulticastMessage;
import com.google.firebase.messaging.Notification;
import com.google.firebase.messaging.SendResponse;
import com.sun.xml.bind.v2.model.impl.RuntimeBuiltinLeafInfoImpl;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.swing.text.html.Option;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class NotificationService {
    private final FirebaseMessaging firebaseMessaging;
    private final AccountService accountService;
    private final NotificationRepository notificationRepository;

    @Transactional
    public BatchResponse sendNotification(Notice notice) {
        User sender = accountService.getAuthUser();
        User receiver = accountService.findByUserName(notice.getUsername());

        NotificationEntity notificationEntity = new NotificationEntity();
        notificationEntity.setSender(sender);
        notificationEntity.setReceiver(receiver);
        notificationEntity.setTitle(notice.getTitle());
        notificationEntity.setBody(notice.getBody());
        notificationEntity.setEventType(EEventType.valueOf(notice.getData().get("type")));
        notificationEntity.setEventTime(Instant.parse(notice.getData().get("time")));
        notificationEntity.setStatus(EEventStatus.UNREAD);
        notificationRepository.saveAndFlush(notificationEntity);

        if (CollectionUtils.isEmpty(receiver.getDeviceTokens())) {
            return null;
        }

        Notification notification = Notification.builder()
                .setTitle(notice.getTitle())
                .setBody(notice.getBody())
                .setImage(sender.getProfile().getAvatarImgUrl())
                .build();
        if(sender.getProfile().getAvatarImgUrl() != null) {
            notice.getData().put("sender_image_url", sender.getProfile().getAvatarImgUrl());
        }
        notice.getData().put("noti_id", notificationEntity.getId().toString());
        MulticastMessage message = MulticastMessage.builder()
                .addAllTokens(receiver.getDeviceTokens())
                .setNotification(notification)
                .putAllData(notice.getData())
                .build();

        BatchResponse batchResponse = null;
        try {
            batchResponse = firebaseMessaging.sendMulticast(message);
        } catch (FirebaseMessagingException e) {
            log.info("Firebase error {}", e.getMessage());
        }
        if (batchResponse.getFailureCount() > 0) {
            List<SendResponse> responses = batchResponse.getResponses();
            List<String> failedTokens = new ArrayList<>();
            for (int i = 0; i < responses.size(); i++) {
                if (!responses.get(i).isSuccessful()) {
                    failedTokens.add(receiver.getDeviceTokens().get(i));
                }
            }
            log.info("List of tokens that caused failures: " + failedTokens);
        }
        return batchResponse;
    }

    @Transactional
    public BatchResponse addFriendNoti(Notice notice) {
        User sender = accountService.getAuthUser();
        User receiver = accountService.findByUserName(notice.getUsername());

        Optional<NotificationEntity> entity = notificationRepository.findNotiFriendReq(
                sender, receiver,
                EEventType.FRIEND_REQUEST, EEventStatus.UNREAD,
                Instant.now().plusSeconds(86400
                ));
        if (entity.isPresent()) {
            return null;
            }
        NotificationEntity notificationEntity = new NotificationEntity();
        notificationEntity.setSender(sender);
        notificationEntity.setReceiver(receiver);
        notificationEntity.setTitle(notice.getTitle());
        notificationEntity.setBody(notice.getBody());
        notificationEntity.setEventType(EEventType.valueOf(notice.getData().get("type")));
        notificationEntity.setEventTime(Instant.parse(notice.getData().get("time")));
        notificationEntity.setStatus(EEventStatus.UNREAD);
        notificationRepository.saveAndFlush(notificationEntity);

        if (CollectionUtils.isEmpty(receiver.getDeviceTokens())) {
            return null;
        }

        Notification notification = Notification.builder()
                .setTitle(notice.getTitle())
                .setBody(notice.getBody())
                .setImage(sender.getProfile().getAvatarImgUrl())
                .build();
        if(sender.getProfile().getAvatarImgUrl() != null) {
            notice.getData().put("sender_image_url", sender.getProfile().getAvatarImgUrl());
        }
        notice.getData().put("noti_id", notificationEntity.getId().toString());
        MulticastMessage message = MulticastMessage.builder()
                .addAllTokens(receiver.getDeviceTokens())
                .setNotification(notification)
                .putAllData(notice.getData())
                .build();

        BatchResponse batchResponse = null;
        try {
            batchResponse = firebaseMessaging.sendMulticast(message);
        } catch (FirebaseMessagingException e) {
            log.info("Firebase error {}", e.getMessage());
        }
        if (batchResponse.getFailureCount() > 0) {
            List<SendResponse> responses = batchResponse.getResponses();
            List<String> failedTokens = new ArrayList<>();
            for (int i = 0; i < responses.size(); i++) {
                if (!responses.get(i).isSuccessful()) {
                    failedTokens.add(receiver.getDeviceTokens().get(i));
                }
            }
            log.info("List of tokens that caused failures: " + failedTokens);
        }
        return batchResponse;
    }

    @Transactional
    public void declineAddFriendNoti(Notice notice) {
        User sender = accountService.getAuthUser();
        User receiver = accountService.findByUserName(notice.getUsername());

        String id = notice.getData().get("noti_id");
        if (StringUtils.isBlank(id)) {
            throw new BusinessException(ErrorCode.NOTIFICATION_NOT_FOUND, "notification not found");
        }
        NotificationEntity declineNoti = notificationRepository.findById(Integer.valueOf(id))
                .orElseThrow(
                        () -> new BusinessException(ErrorCode.NOTIFICATION_NOT_FOUND, "notification not found")
                );
        notificationRepository.delete(declineNoti);

        if (CollectionUtils.isEmpty(receiver.getDeviceTokens())) {
            return;
        }

        Notification notification = Notification.builder()
                .setTitle(notice.getTitle())
                .setBody(notice.getBody())
                .setImage(sender.getProfile().getAvatarImgUrl())
                .build();
        notice.getData().put("sender_image_url", sender.getProfile().getAvatarImgUrl());
    }
    @Transactional
    public void deleteNoti(Integer id) {
        NotificationEntity declineNoti = notificationRepository.findById(Integer.valueOf(id))
                .orElseThrow(
                        () -> new BusinessException(ErrorCode.NOTIFICATION_NOT_FOUND, "notification not found")
                );
        notificationRepository.delete(declineNoti);
    }
    @Transactional
    public BatchResponse inviteNoti(Notice notice) {
        User sender = accountService.getAuthUser();
        User receiver = accountService.findByUserName(notice.getUsername());

        String roomCode = notice.getData().get("code");
        if (StringUtils.isBlank(roomCode)) {
            throw new BusinessException(ErrorCode.INVITE_ROOM_CODE_INVALID, "invite code not foudn");
        }
        NotificationEntity notificationEntity = new NotificationEntity();
        notificationEntity.setSender(sender);
        notificationEntity.setReceiver(receiver);
        notificationEntity.setTitle(notice.getTitle());
        notificationEntity.setBody(notice.getBody());
        notificationEntity.setEventType(EEventType.valueOf(notice.getData().get("type")));
        notificationEntity.setEventTime(Instant.parse(notice.getData().get("time")));
        notificationEntity.setPayload(notice.getData().get("code"));
        notificationEntity.setStatus(EEventStatus.UNREAD);
        notificationRepository.saveAndFlush(notificationEntity);

        if (CollectionUtils.isEmpty(receiver.getDeviceTokens())) {
            return null;
        }

        Notification notification = Notification.builder()
                .setTitle(notice.getTitle())
                .setBody(notice.getBody())
                .setImage(sender.getProfile().getAvatarImgUrl())
                .build();
        if(sender.getProfile().getAvatarImgUrl() != null) {
            notice.getData().put("sender_image_url", sender.getProfile().getAvatarImgUrl());
        }
        notice.getData().put("noti_id", notificationEntity.getId().toString());
        MulticastMessage message = MulticastMessage.builder()
                .addAllTokens(receiver.getDeviceTokens())
                .setNotification(notification)
                .putAllData(notice.getData())
                .build();

        BatchResponse batchResponse = null;
        try {
            batchResponse = firebaseMessaging.sendMulticast(message);
        } catch (FirebaseMessagingException e) {
            log.info("Firebase error {}", e.getMessage());
        }
        if (batchResponse.getFailureCount() > 0) {
            List<SendResponse> responses = batchResponse.getResponses();
            List<String> failedTokens = new ArrayList<>();
            for (int i = 0; i < responses.size(); i++) {
                if (!responses.get(i).isSuccessful()) {
                    failedTokens.add(receiver.getDeviceTokens().get(i));
                }
            }
            log.info("List of tokens that caused failures: " + failedTokens);
        }
        return batchResponse;
    }

    @Transactional
    public Page<NoticeRes> getNotices(Pageable pageable) {
        User user = accountService.getAuthUser();
        Page<NotificationEntity> notificationEntities =
                notificationRepository.getAllByReceiverOrderByEventTimeDesc(user, pageable);

        notificationRepository.saveAll(notificationEntities);
        return new PageImpl<>(
                notificationEntities
                        .stream()
                        .map(NoticeRes::valueOf)
                        .collect(Collectors.toList()),
                pageable,
                notificationEntities.getTotalElements());
    }

    public void updateStatus(Integer id) {
        NotificationEntity notificationEntity = notificationRepository.findById(id).orElseThrow(
                () -> new BusinessException(ErrorCode.NOTIFICATION_NOT_FOUND, "noti not found")
        );
        notificationEntity.setStatus(EEventStatus.READ);
        notificationRepository.save(notificationEntity);
    }

    public Integer getTotalUnread() {
        User receiver = accountService.getAuthUser();
        return notificationRepository.countAllByReceiverAndStatus(receiver, EEventStatus.UNREAD);
    }

    @Transactional
    public void markReadAll() {
        notificationRepository.updateMarkReadAll(EEventStatus.READ);
    }
}
