package com.example.vstudy.notification;

import com.example.vstudy.constant.EEventType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Notice implements Serializable {
    private final static long serialVersionUID = -8065530054006419697L;

    private String title;

    private String body;

    private Map<String,String> data;

    @NotNull
    private String username;
}
