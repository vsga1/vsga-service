package com.example.vstudy;

import com.example.vstudy.support.data.OffsetSetting;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties(value = {OffsetSetting.class})
public class VStudyApplication  {

    public static void main(String[] args) {
        SpringApplication.run(VStudyApplication.class, args);
    }

}
