package com.example.vstudy.support.audit;

import com.example.vstudy.entity.User;
import com.example.vstudy.service.AccountService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Optional;

@AllArgsConstructor
public class AppAuditorAware implements AuditorAware<String> {
    private final AccountService accountService;


    @Override
    public Optional<String> getCurrentAuditor() {
        String principal = accountService.getAuthPrinciple();
        Optional<String> auditor = Optional.empty();

        if(principal != null) {
            String au = principal;
            auditor = Optional.of(au);
        }
        return auditor;
    }
}
