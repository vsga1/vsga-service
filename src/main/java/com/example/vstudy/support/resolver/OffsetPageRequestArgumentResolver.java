package com.example.vstudy.support.resolver;

import com.example.vstudy.exception.InvalidParameterException;
import com.example.vstudy.support.data.OffsetSetting;
import com.example.vstudy.util.OffsetPageableRequest;
import lombok.AllArgsConstructor;
import org.springframework.core.MethodParameter;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.data.web.SortArgumentResolver;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.ModelAndViewContainer;


@AllArgsConstructor
public class OffsetPageRequestArgumentResolver extends PageableHandlerMethodArgumentResolver {
    public static final String OFFSET = "offset";
    public static final String LIMIT = "limit";
    private SortArgumentResolver sortResolver;
    private OffsetSetting offsetSetting;

    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return parameter.getParameterAnnotation(OffsetPageable.class) != null;
    }

    @Override
    public Pageable resolveArgument(MethodParameter methodParameter, ModelAndViewContainer mavContainer,
                                    NativeWebRequest webRequest, WebDataBinderFactory binderFactory) {
        String offset = webRequest.getParameter(OFFSET);
        String limit = webRequest.getParameter(LIMIT);
        Sort sort = sortResolver.resolveArgument(methodParameter, mavContainer, webRequest, binderFactory);
        if (sort.equals(Sort.unsorted())) {
            sort = Sort.by(Sort.Direction.DESC, "id");
        }

        return new OffsetPageableRequest(
                parseAndApplyDefault(offset, offsetSetting.getDefaultOffset(), offsetSetting.getMinOffset(), null, OffsetSetting.PARAM_OFFSET),
                parseAndApplyDefault(limit, offsetSetting.getDefaultLimit(), offsetSetting.getMinLimit(), offsetSetting.getMaxLimit(), OffsetSetting.PARAM_LIMIT),
                sort
        );
    }

    private Integer parseAndApplyDefault(String parameter, int defaultValue, int min, Integer max, String label) {
        if (!StringUtils.hasText(parameter)) {
            return defaultValue;
        }

        try {
            int value = Integer.parseInt(parameter);
            if (value < min) {
                throw new InvalidParameterException(
                        String.format("'%s' param must be greater than or equal to %s", label, min)
                );
            }
            if (max != null && value > max) {
                throw new InvalidParameterException(
                        String.format("'%s' param must be less than or equal to  %s", label, max)
                );
            }

            return value;
        } catch (NumberFormatException e) {
            return defaultValue;
        }
    }
}