package com.example.vstudy.support.web;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class MetaList extends Meta {
    private static final long serialVersionUID = 6212491539079170585L;

    @JsonProperty("offset")
    private long offset;

    @JsonProperty("limit")
    private int limit;

    @JsonProperty("total")
    private long total;

    public MetaList(int code, String message, long offset, int limit, long total) {
        super(code, message);
        this.offset = offset;
        this.limit = limit;
        this.total = total;
    }

    public static MetaList instance(int code, String message, long offset, int limit, long total) {
        return new MetaList(code, message, offset, limit, total);
    }
}
