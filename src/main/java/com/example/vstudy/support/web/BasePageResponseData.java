package com.example.vstudy.support.web;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
@NoArgsConstructor
public class BasePageResponseData implements Serializable {
    private static final long serialVersionUID = 4659614601926410078L;

    @JsonProperty("meta")
    private MetaList meta;

    @JsonProperty("data")
    private Object data;

    public BasePageResponseData(MetaList meta, Object data) {
        this.meta = meta;
        this.data = data;
    }

    public BasePageResponseData(MetaList meta) {
        this.meta = meta;
    }

    public static BasePageResponseData instance(MetaList meta, Object data) {
        return new BasePageResponseData(meta, data);
    }
}
