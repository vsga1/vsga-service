package com.example.vstudy.support.data;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;

@Getter
@AllArgsConstructor
@ConstructorBinding
@ConfigurationProperties(prefix = "app.offset")
public class OffsetSetting {
    public static final String PARAM_OFFSET = "offset";
    public static final String PARAM_LIMIT = "limit";

    private int defaultOffset;
    private int defaultLimit;
    private int minOffset;
    private int minLimit;
    private int maxLimit;
}
