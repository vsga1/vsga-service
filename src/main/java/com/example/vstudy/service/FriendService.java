package com.example.vstudy.service;

import com.example.vstudy.model.req.CreateTopicReq;
import com.example.vstudy.model.res.TopicRes;
import com.example.vstudy.model.res.UserProfileRes;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface FriendService {
    void addFriend(String username);

    void unfriend(String name);

    Page<UserProfileRes> getListFriend(Pageable pageable);

}
