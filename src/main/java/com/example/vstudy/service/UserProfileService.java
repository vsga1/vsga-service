package com.example.vstudy.service;

import com.example.vstudy.entity.UserProfile;
import com.example.vstudy.model.res.UserProfileRes;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface UserProfileService {
    void save(UserProfile userProfile);

}
