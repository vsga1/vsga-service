package com.example.vstudy.service;

import com.example.vstudy.model.req.CreateNoteReq;
import com.example.vstudy.model.req.UpdateNoteReq;
import com.example.vstudy.model.res.NoteRes;

import java.util.List;

public interface NoteService {
    List<NoteRes> getNoteListByRoomAndUser(String roomCode);

    NoteRes createNote(CreateNoteReq req, String roomCode);

    NoteRes updateNote(UpdateNoteReq req, String roomCode);
}
