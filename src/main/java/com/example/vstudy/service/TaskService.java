package com.example.vstudy.service;

import com.example.vstudy.model.req.CreateTaskReq;
import com.example.vstudy.model.req.DeleteTaskReq;
import com.example.vstudy.model.req.GetDoneTaskReq;
import com.example.vstudy.model.req.SearchTaskParam;
import com.example.vstudy.model.req.UpdateTaskReq;
import com.example.vstudy.model.res.DoneTaskRes;
import com.example.vstudy.model.res.TaskRes;

import java.util.List;

public interface TaskService {
    TaskRes createTask(CreateTaskReq req);

    void updateTask(UpdateTaskReq req);

    void deleteTask(DeleteTaskReq req);

    List<TaskRes> getTaskList(SearchTaskParam params);

    DoneTaskRes getMemberDoneTask(GetDoneTaskReq req);
}
