package com.example.vstudy.service;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.List;
import com.google.api.services.drive.model.File;
import org.springframework.web.multipart.MultipartFile;

public interface StorageService {
    List<File> getAllFiles() throws GeneralSecurityException, IOException;

    File createFile(MultipartFile file,String folderId) throws IOException, GeneralSecurityException;

    File createFolder(String roomName) throws IOException, GeneralSecurityException;

    List<File> getRoomFiles(String folderId) throws IOException, GeneralSecurityException;

    File getFile(String fileId) throws IOException, GeneralSecurityException;

    void deleteFile(String fileId) throws GeneralSecurityException, IOException;
}
