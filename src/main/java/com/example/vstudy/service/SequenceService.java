package com.example.vstudy.service;

public interface SequenceService {
    int getNextSeq(String key, Object... args);
}
