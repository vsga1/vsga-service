package com.example.vstudy.service;

import com.example.vstudy.entity.RoomMember;
import com.example.vstudy.model.req.DemoteReq;
import com.example.vstudy.model.req.JoinRoomReq;
import com.example.vstudy.model.req.PromoteReq;
import com.example.vstudy.model.res.CurrentRoomMemberRes;
import com.example.vstudy.model.res.JoinRoomRes;

public interface EventService {
    RoomMember checkUserInRoom(String roomCode);

    JoinRoomRes enterRoom(JoinRoomReq req);

    void outRoom(String roomCode);

    CurrentRoomMemberRes getCurrentMembers(String roomCode);

    void promoteMember(PromoteReq req);
    void demoteMember(DemoteReq req);

}
