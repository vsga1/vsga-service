package com.example.vstudy.service;

import com.example.vstudy.entity.User;
import com.example.vstudy.model.req.CreateAccountReq;
import com.example.vstudy.model.req.LoginReq;
import com.example.vstudy.model.req.ResetPasswordReq;
import com.example.vstudy.model.req.SearchUserReq;
import com.example.vstudy.model.req.UpdatePasswordReq;
import com.example.vstudy.model.req.UpdateProfileReq;
import com.example.vstudy.model.req.UploadAvatarReq;
import com.example.vstudy.model.res.LoginRes;
import com.example.vstudy.model.res.UserProfileRes;
import com.example.vstudy.model.res.UserSummaryRes;
import com.google.api.services.drive.model.File;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.List;
import java.util.Optional;


public interface AccountService {
    LoginRes loginAdmin(LoginReq req);
    LoginRes login(LoginReq req);

    String getAuthPrinciple();

    void createAccount(CreateAccountReq req);

    UserProfileRes getUserProfile(String code);

    void sendMailResetPassword(String email);

    User getAuthUser();

    Optional<User> findByEmail(String email);

    UserProfileRes updateProfile(UpdateProfileReq req);

    void handleResetPassword(ResetPasswordReq req);

    UserProfileRes getCurrentUserProfile();

    void activateEmail(String token);

    void uploadAvatar(UploadAvatarReq req);

    void updatePassword(UpdatePasswordReq req);

    User findByUserName(String username);

    File uploadAvatarImage(MultipartFile multipartFile) throws GeneralSecurityException, IOException;

    Page<UserProfileRes> searchUser(SearchUserReq req, Pageable pageable);

    void lockUser(String username);

    void unlockUser(String username);

    UserSummaryRes getUserSummary(Integer months);

    List<UserProfileRes> searchFriend(String name);

    void addDeviceToken(String token);

    void save(User user);
}
