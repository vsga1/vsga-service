package com.example.vstudy.service.impl;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.example.vstudy.constant.EAccountRole;
import com.example.vstudy.constant.EAuthProvider;
import com.example.vstudy.constant.ECommonStatus;
import com.example.vstudy.constant.ErrorCode;
import com.example.vstudy.entity.User;
import com.example.vstudy.entity.UserFriend;
import com.example.vstudy.entity.UserProfile;
import com.example.vstudy.exception.BusinessException;
import com.example.vstudy.exception.JWTTokenException;
import com.example.vstudy.model.req.CreateAccountReq;
import com.example.vstudy.model.req.LoginReq;
import com.example.vstudy.model.req.ResetPasswordReq;
import com.example.vstudy.model.req.SearchUserReq;
import com.example.vstudy.model.req.UpdatePasswordReq;
import com.example.vstudy.model.req.UpdateProfileReq;
import com.example.vstudy.model.req.UploadAvatarReq;
import com.example.vstudy.model.res.LoginRes;
import com.example.vstudy.model.res.UserProfileRes;
import com.example.vstudy.model.res.UserSummaryRes;
import com.example.vstudy.repository.FriendRepository;
import com.example.vstudy.repository.UserProfileRepository;
import com.example.vstudy.repository.UserRepository;
import com.example.vstudy.service.AccountService;
import com.example.vstudy.service.MailService;
import com.example.vstudy.service.StorageService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.api.services.drive.model.File;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.entity.ContentType;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.Principal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Service
@AllArgsConstructor
public class AccountServiceImpl implements AccountService, UserDetailsService {
    @Qualifier("getAvatarFolderId")
    private String avatarFolderId;
    private final UserRepository userRepository;

    private final FriendRepository friendRepository;
    private final UserProfileRepository userProfileRepository;
    private final BCryptPasswordEncoder passwordEncoder;
    private final ApplicationEventPublisher eventPublisher;
    private final MailService mailService;
    private final StorageService storageService;

    @Override
    public LoginRes loginAdmin(LoginReq req) {
        User user = userRepository.findAdmin(req.getUsername(), EAccountRole.ADMIN, ECommonStatus.ACTIVE)
                .orElseThrow(
                        () -> new BusinessException(ErrorCode.ACCESS_DENIED, "Access denied")
                );
        if (!req.getPassword().equals(user.getPassword())) {
            throw new BusinessException(ErrorCode.ACCESS_DENIED, "Access denied");
        }
        Algorithm algorithm = Algorithm.HMAC512("secret".getBytes());
        String access_token = JWT.create()
                .withSubject(user.getUsername())
                .sign(algorithm);
        return LoginRes.builder()
                .accessToken(access_token)
                .build();
    }

    @Override
    public LoginRes login(LoginReq req) {
        User user = userRepository.findUserByUsername(req.getUsername())
                .orElseThrow(
                        () -> new BusinessException(ErrorCode.LOGIN_FAILED, "User ot found")
                );
        if (!passwordEncoder.matches(req.getPassword(), user.getPassword())) {
            throw new BusinessException(ErrorCode.LOGIN_FAILED, "User ot found");
        }
        if (ECommonStatus.INACTIVE.equals(user.getStatus())) {
            throw new BusinessException(ErrorCode.USER_INACTIVE, "User inactive");
        }
        Algorithm algorithm = Algorithm.HMAC512("secret".getBytes());
        String access_token = JWT.create()
                .withSubject(user.getUsername())
                .sign(algorithm);
        return LoginRes.builder()
                .accessToken(access_token)
                .build();
    }

    @Override
    public String getAuthPrinciple() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            return (String) authentication.getPrincipal();
        }
        return null;
    }

    @Override
    @Transactional
    public void createAccount(CreateAccountReq req) {
        this.validateRegistrationReq(req);
        User user = new User();
        user.setUsername(req.getUsername());
        user.setPassword(passwordEncoder.encode(req.getPassword()));
        user.setEmail(req.getEmail());
        user.setAuthProvider(EAuthProvider.BASIC);
        UUID uuid = UUID.randomUUID();
        user.setCode(uuid.toString());
        user.setStatus(ECommonStatus.ACTIVE);
        user.setRole(EAccountRole.USER);
        userRepository.save(user);

        UserProfile userProfile = new UserProfile();
        userProfile.setUser(user);
        userProfile.setAvatarImgUrl("https://res.cloudinary.com/huydoan1908/image/upload/v1685714074/default_avatar_yzg7zi.webp");
        userProfileRepository.save(userProfile);

        if (!StringUtils.isEmpty(user.getEmail())) {
            eventPublisher.publishEvent(user.getEmail());
        }
    }

    private void validateRegistrationReq(CreateAccountReq req) {
        Optional<User> userByEmail = userRepository.findUserByEmailAndStatus(req.getEmail(), ECommonStatus.ACTIVE);
        if (userByEmail.isPresent()) {
            throw new BusinessException(ErrorCode.EMAIL_HAS_BEEN_USED, "Email has been used");
        }

        Optional<User> userByUsername = userRepository.findUserByUsernameAndStatus(req.getUsername(), ECommonStatus.ACTIVE);
        if (userByUsername.isPresent()) {
            throw new BusinessException(ErrorCode.USERNAME_HAS_BEEN_USED, "Username has been used");
        }
    }

    @Override
    public UserProfileRes getUserProfile(String code) {
        User user = userRepository.findUserByCodeAndStatus(code, ECommonStatus.ACTIVE)
                .orElseThrow(() -> new BusinessException(ErrorCode.USER_NOT_FOUND, "User code not found"));
        UserProfile userProfile = userProfileRepository.findUserProfileByUser(user);
        return UserProfileRes.valueOf(user, userProfile);
    }

    @Override
    public void sendMailResetPassword(String email) {
        User user = userRepository.findUserByEmailAndStatus(email, ECommonStatus.ACTIVE)
                .orElseThrow(() -> new BusinessException(ErrorCode.EMAIL_NOT_FOUND, String.format("Email not found, email:%s", email)));
        mailService.sendMailResetPassword(email);
    }

    @Override
    public UserProfileRes updateProfile(UpdateProfileReq req) throws BusinessException {
        Principal principal = SecurityContextHolder.getContext().getAuthentication();
        User user = userRepository.findUserByUsernameAndStatus(principal.getName(), ECommonStatus.ACTIVE)
                .orElseThrow(() -> new BusinessException(ErrorCode.USER_NOT_FOUND, "User not found"));
        UserProfile userProfile = userProfileRepository.findUserProfileByUser(user);
        userProfile.setFullName(req.getFullName());
        userProfile.setDisplayName(req.getDisplayName());
        userProfile.setDescription(req.getDescription());
        userProfile.setGender(req.getGender());
        userProfile.setAddress(req.getAddress());
        userProfile.setAge(req.getAge());
        userProfileRepository.save(userProfile);

        return UserProfileRes.valueOf(user, userProfile);
    }

    @Override
    public void handleResetPassword(ResetPasswordReq req) {
        Algorithm algorithm = Algorithm.HMAC512("secret".getBytes());
        JWTVerifier verifier = JWT.require(algorithm).build();
        DecodedJWT decodedJWT = verifier.verify(req.getToken());
        String email = decodedJWT.getSubject();

        User user = userRepository.findUserByEmailAndStatus(email, ECommonStatus.ACTIVE)
                .orElseThrow(() -> new BusinessException(ErrorCode.USER_NOT_FOUND, "User not found"));
        user.setPassword(passwordEncoder.encode(req.getPassword()));
        userRepository.save(user);
    }

    @Override
    public UserProfileRes getCurrentUserProfile() {
        User user = this.getAuthUser();
        UserProfile userProfile = userProfileRepository.findUserProfileByUser(user);
        return UserProfileRes.valueOf(user, userProfile);
    }

    @Override
    public void activateEmail(String token) {
        Algorithm algorithm = Algorithm.HMAC512("secret".getBytes());
        JWTVerifier verifier = JWT.require(algorithm).build();
        DecodedJWT decodedJWT = verifier.verify(token);
        if (decodedJWT.getExpiresAt().before(new Date())) {
            throw new JWTTokenException(ErrorCode.TOKEN_HAS_EXPIRED, "Token is expired");
        }
        String email = decodedJWT.getSubject();

        User user = userRepository.findUserByEmailAndStatus(email, ECommonStatus.ACTIVE)
                .orElseThrow(() -> new BusinessException(ErrorCode.USER_NOT_FOUND, "User not found"));
        user.setIsEnabled(Boolean.TRUE);
        userRepository.save(user);
    }

    @Override
    public void uploadAvatar(UploadAvatarReq req) {
        User user = getAuthUser();
        UserProfile profile = userProfileRepository.findUserProfileByUser(user);
        profile.setAvatarImgUrl(req.getUrl());
        userProfileRepository.save(profile);
    }

    @Override
    public void updatePassword(UpdatePasswordReq req) {
        User user = getAuthUser();
        if (!passwordEncoder.matches(req.getCurrentPassword(), user.getPassword())) {
            throw new BusinessException(ErrorCode.CURRENT_PASSWORD_INCORRECT, "current password incorrect");
        }
        user.setPassword(passwordEncoder.encode(req.getNewPassword()));
        userRepository.save(user);
    }

    @Override
    public User findByUserName(String username) {
        return userRepository.findUserByUsernameAndStatus(username, ECommonStatus.ACTIVE)
                .orElseThrow(
                        () -> new BusinessException(ErrorCode.USER_NOT_FOUND, "Username not found")
                );
    }

    @Override
    public File uploadAvatarImage(MultipartFile uploadFile) throws GeneralSecurityException, IOException {
        User user = this.getAuthUser();
        UserProfile userProfile = userProfileRepository.findUserProfileByUser(user);

        String contentType = uploadFile.getContentType();
        if (!Arrays.asList(ContentType.IMAGE_JPEG.getMimeType(),
                ContentType.IMAGE_PNG.getMimeType(),
                ContentType.IMAGE_SVG.getMimeType(),
                ContentType.IMAGE_GIF.getMimeType()).contains(contentType)) {
            throw new BusinessException(ErrorCode.FILE_FORMAT_INVALID, "Format invalid");
        }
        File file = storageService.createFile(uploadFile, avatarFolderId);
        File avatar = storageService.getFile(file.getId());
        userProfile.setAvatarImgUrl(avatar.getThumbnailLink());
        userProfileRepository.save(userProfile);
        return avatar;
    }

    @Override
    public Page<UserProfileRes> searchUser(SearchUserReq req, Pageable pageable) {
        String username = null;
        if (StringUtils.isNotEmpty(req.getUsername())) {
            username = "%" + req.getUsername() + "%";
        }
        Page<User> users = userRepository.searchUser(
                username,
                req.getCode(),
                req.getEmail(),
                req.getStatus(),
                req.getCreatedFrom(),
                req.getCreatedTo(),
                req.getIsEnabled(),
                pageable
        );
        List<UserProfileRes> userRes = users.stream()
                .map(item -> UserProfileRes.valueOf(item, item.getProfile()))
                .collect(Collectors.toList());
        return new PageImpl<>(userRes, pageable, users.getTotalElements());
    }

    @Override
    public void lockUser(String username) {
        User user = findByUserName(username);
        user.setStatus(ECommonStatus.INACTIVE);
        userRepository.save(user);
    }

    @Override
    public void unlockUser(String username) {
        User user = userRepository.findUserByUsernameAndStatus(username, ECommonStatus.INACTIVE)
                .orElseThrow(
                        () -> new BusinessException(ErrorCode.USER_NOT_FOUND, "Username not found")
                );
        user.setStatus(ECommonStatus.ACTIVE);
        userRepository.save(user);
    }

    @Override
    public UserSummaryRes getUserSummary(Integer months) {
        int active = userRepository.countUserByStatus(ECommonStatus.ACTIVE);
        int inactive = userRepository.countUserByStatus(ECommonStatus.INACTIVE);
        List<LocalDateTime> monthsList = new ArrayList<>();
        List<Integer> res = new ArrayList<>();
        List<UserSummaryRes.UserPerMonth> userPerMonths = new ArrayList<>();
        LocalDateTime now = LocalDateTime.now();
        for (int i = 0; i < months; i++) {
            monthsList.add(now.minusMonths(i).withDayOfMonth(1).withMinute(0).withSecond(0));
            res.add(userRepository.countUserThisMonth(now.minusMonths(i).withDayOfMonth(1).withMinute(0).withSecond(0)));
        }
        for (int i = res.size() - 1; i >= 1; i--) {
            UserSummaryRes.UserPerMonth upm = UserSummaryRes.UserPerMonth.builder()
                    .month(monthsList.get(i).getMonth().toString())
                    .total(res.get(i) - res.get(i - 1))
                    .build();
            userPerMonths.add(upm);
        }
        UserSummaryRes.UserPerMonth upm = UserSummaryRes.UserPerMonth.builder()
                .month(monthsList.get(0).getMonth().toString())
                .total(res.get(0))
                .build();
        userPerMonths.add(upm);
        return UserSummaryRes.builder()
                .totalActiveUser(active)
                .totalInactiveUser(inactive)
                .newUsersPerMonth(userPerMonths)
                .build();
    }

    @Override
    public List<UserProfileRes> searchFriend(String name) {
        User user = this.getAuthUser();
        List<User> users = userRepository.searchFriend("%" + name + "%", ECommonStatus.ACTIVE);
        List<UserFriend> userFriends = friendRepository.getAllByUserAndStatus(user, ECommonStatus.ACTIVE);
        List<User> friends = null;
        List<UserProfileRes> userProfileRes = users.stream()
                .map(item -> UserProfileRes.valueOf(item, item.getProfile()))
                .collect(Collectors.toList());
        userProfileRes.forEach(item -> item.setIsFriend(Boolean.FALSE));
        if (!CollectionUtils.isEmpty(userFriends)) {
            friends = userFriends
                    .stream()
                    .map(UserFriend::getFriend)
                    .filter(friend -> friend.getStatus().equals(ECommonStatus.ACTIVE))
                    .collect(Collectors.toList());
            for (UserProfileRes u : userProfileRes) {
                for (User f : friends) {
                    if (u.getUsername().equals(f.getUsername())) {
                        u.setIsFriend(true);
                    }
                }
            }
        }

        return userProfileRes;
    }

    @Override
    public void addDeviceToken(String token) {
        User user = this.getAuthUser();
        // List<String> tokens = user.getDeviceTokens();
        ArrayList<String> tokens = new ArrayList<>(user.getDeviceTokens());
        if (CollectionUtils.isEmpty(tokens)) {
            user.setDeviceTokens(Collections.singletonList(token));
        } else {
            if (!tokens.contains(token)) {
                tokens.add(token);
            }
            user.setDeviceTokens(tokens);
        }

        userRepository.save(user);
    }

    @Override
    public void save(User user) {
        userRepository.save(user);
    }

    @Override
    public User getAuthUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getPrincipal().toString();
        return userRepository.findUserByUsernameAndStatus(username, ECommonStatus.ACTIVE).orElseThrow(
                () -> new BusinessException(ErrorCode.USER_NOT_FOUND, "User not found")
        );
    }

    @Override
    public Optional<User> findByEmail(String email) {
        return userRepository.findUserByEmailAndStatus(email, ECommonStatus.ACTIVE);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        boolean accountNonExpired = true;
        boolean credentialsNonExpired = true;
        boolean accountNonLocked = true;
        User user = userRepository.findUserByUsernameAndStatus(username, ECommonStatus.ACTIVE).orElseThrow(
                () -> new BusinessException(ErrorCode.ACCESS_DENIED, "User not found")
        );
        if (user == null) {
            throw new BusinessException(ErrorCode.USER_NOT_FOUND, "User not found");
        }
        Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();

        return new org.springframework.security.core.userdetails.User(
                user.getUsername(),
                user.getPassword(),
                true,
                accountNonExpired,
                credentialsNonExpired,
                accountNonLocked,
                new ArrayList<>());
    }
}
