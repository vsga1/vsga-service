package com.example.vstudy.service.impl;

import com.example.vstudy.constant.ECommonStatus;
import com.example.vstudy.constant.ENoteStatus;
import com.example.vstudy.constant.ErrorCode;
import com.example.vstudy.entity.Room;
import com.example.vstudy.entity.RoomMember;
import com.example.vstudy.entity.RoomNote;
import com.example.vstudy.entity.User;
import com.example.vstudy.exception.BusinessException;
import com.example.vstudy.model.req.RoomNoteCreateReq;
import com.example.vstudy.model.req.RoomNoteUpdateReq;
import com.example.vstudy.model.res.RoomNoteRes;
import com.example.vstudy.repository.RoomMemberRepository;
import com.example.vstudy.repository.RoomNoteRepository;
import com.example.vstudy.repository.RoomRepository;
import com.example.vstudy.service.AccountService;
import com.example.vstudy.service.RoomNoteService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class RoomNoteServiceImpl implements RoomNoteService {
    private final AccountService accountService;
    private final RoomRepository roomRepository;
    private final RoomNoteRepository roomNoteRepository;
    private final RoomMemberRepository roomMemberRepository;


    public Room getRoomByCode(String code) {
        return roomRepository
                .findRoomByCodeAndStatus(code, ECommonStatus.ACTIVE)
                .orElseThrow(() -> new BusinessException(ErrorCode.ROOM_NOT_FOUND, "Room not found."));
    }

    @Override
    public RoomNoteRes createRoomNote(RoomNoteCreateReq req) {
        Room room = this.getRoomByCode(req.getRoomCode());
        if (roomNoteRepository.findByRoom(room).isPresent()) {
            throw new BusinessException(ErrorCode.ROOM_NOTE_EXISTED, "Room note existed");
        }

        RoomNote roomNote = new RoomNote();
        roomNote.setRoom(room);
        roomNote.setHeader(req.getHeader());
        roomNote.setDescription(req.getDescription());
        roomNote.setStatus(ENoteStatus.OPEN);

        roomNoteRepository.save(roomNote);

        return RoomNoteRes.valueOf(roomNote);
    }

    @Override
    public RoomNoteRes getRoomNote(String roomCode) {
        Room room = this.getRoomByCode(roomCode);
        RoomNote roomNote = roomNoteRepository.findByRoom(room).orElseThrow(
                () -> new BusinessException(ErrorCode.ROOM_NOTE_NOT_FOUND, "room note not found")
        );

        return RoomNoteRes.valueOf(roomNote);
    }

    @Override
    public RoomNoteRes updateRoomNote(RoomNoteUpdateReq req) {
        Room room = this.getRoomByCode(req.getRoomCode());
        User user = accountService.getAuthUser();
        RoomMember roomMember = roomMemberRepository.findRoomMemberByUserAndRoomAndStatus(user, room, ECommonStatus.ACTIVE)
                .orElseThrow(
                        () -> new BusinessException(ErrorCode.ROOM_MEMBER_NOT_FOUND, "User is not join room")
                );
        RoomNote roomNote = roomNoteRepository.findByRoom(room).orElseThrow(
                () -> new BusinessException(ErrorCode.ROOM_NOTE_NOT_FOUND, "room note not found")
        );
        if(req.getStatus() != null) {
            roomNote.setStatus(req.getStatus());
        }
        if(req.getHeader() != null) {
            roomNote.setHeader(req.getHeader());
        }
        if(req.getDescription() != null) {
            roomNote.setDescription(req.getDescription());
        }
        roomNoteRepository.save(roomNote);
        return RoomNoteRes.valueOf(roomNote);
    }

}
