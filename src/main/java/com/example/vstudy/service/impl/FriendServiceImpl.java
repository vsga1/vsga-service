package com.example.vstudy.service.impl;

import com.example.vstudy.constant.ECommonStatus;
import com.example.vstudy.entity.User;
import com.example.vstudy.entity.UserFriend;
import com.example.vstudy.model.res.UserProfileRes;
import com.example.vstudy.repository.FriendRepository;
import com.example.vstudy.service.AccountService;
import com.example.vstudy.service.FriendService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class FriendServiceImpl implements FriendService {
    private final FriendRepository friendRepository;
    private final AccountService accountService;
    @Override
    public void addFriend(String username) {
        User user = accountService.getAuthUser();
        User friend = accountService.findByUserName(username);
        UserFriend userFriend1 = null;
        UserFriend userFriend2 = null;
        Optional<UserFriend> record1 = friendRepository.findUserFriendByUserAndFriendAndStatus(user,friend,ECommonStatus.ACTIVE);
        Optional<UserFriend> record2 = friendRepository.findUserFriendByUserAndFriendAndStatus(friend,user,ECommonStatus.ACTIVE);

        if(record1.isEmpty()) {
            userFriend1 = new UserFriend();
            userFriend1.setUser(user);
            userFriend1.setFriend(friend);
            userFriend1.setStatus(ECommonStatus.ACTIVE);
            friendRepository.save(userFriend1);
        }
        if(record2.isEmpty()) {
            userFriend2 = new UserFriend();
            userFriend2.setUser(friend);
            userFriend2.setFriend(user);
            userFriend2.setStatus(ECommonStatus.ACTIVE);
            friendRepository.save(userFriend2);
        }
    }

    @Override
    public void unfriend(String name) {
        User user = accountService.getAuthUser();
        User friend = accountService.findByUserName(name);
        Optional<UserFriend> record1 = friendRepository.findUserFriendByUserAndFriendAndStatus(user,friend, ECommonStatus.ACTIVE);
        Optional<UserFriend> record2 = friendRepository.findUserFriendByUserAndFriendAndStatus(friend,user, ECommonStatus.ACTIVE);

        if(record1.isPresent()) {
            record1.get().setStatus(ECommonStatus.INACTIVE);
            friendRepository.save(record1.get());
        }
        if(record2.isPresent()) {
            record2.get().setStatus(ECommonStatus.INACTIVE);
            friendRepository.save(record2.get());
        }
    }

    @Override
    public Page<UserProfileRes> getListFriend(Pageable pageable) {
        User user = accountService.getAuthUser();
        Page<UserFriend> friends = friendRepository.findUserFriendByUserAndStatus(user,ECommonStatus.ACTIVE,pageable);
        List<UserProfileRes> friendRes = friends
                .stream()
                .filter(item -> item.getFriend().getStatus().equals(ECommonStatus.ACTIVE))
                .map(item -> UserProfileRes.valueOf(item.getFriend(),item.getFriend().getProfile()))
                .collect(Collectors.toList());
        return new PageImpl<>(friendRes,pageable,friends.getTotalElements());
    }
}
