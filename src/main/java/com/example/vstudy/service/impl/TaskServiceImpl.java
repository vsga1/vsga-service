package com.example.vstudy.service.impl;

import com.example.vstudy.constant.ECommonStatus;
import com.example.vstudy.constant.ERoomRoleType;
import com.example.vstudy.constant.ETaskStatus;
import com.example.vstudy.constant.ETaskType;
import com.example.vstudy.constant.ErrorCode;
import com.example.vstudy.constant.SeqName;
import com.example.vstudy.entity.Room;
import com.example.vstudy.entity.RoomMember;
import com.example.vstudy.entity.Task;
import com.example.vstudy.entity.User;
import com.example.vstudy.exception.BusinessException;
import com.example.vstudy.model.req.CreateTaskReq;
import com.example.vstudy.model.req.DeleteTaskReq;
import com.example.vstudy.model.req.GetDoneTaskReq;
import com.example.vstudy.model.req.SearchTaskParam;
import com.example.vstudy.model.req.UpdateTaskReq;
import com.example.vstudy.model.res.DoneTaskRes;
import com.example.vstudy.model.res.TaskRes;
import com.example.vstudy.model.res.UserProfileRes;
import com.example.vstudy.repository.RoomMemberRepository;
import com.example.vstudy.repository.TaskRepository;
import com.example.vstudy.service.AccountService;
import com.example.vstudy.service.RoomService;
import com.example.vstudy.service.SequenceService;
import com.example.vstudy.service.TaskService;
import com.example.vstudy.util.Strings;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class TaskServiceImpl implements TaskService {
    private static final int TASK_SEQ_LENGTH = 6;
    private static final int SCHEME_SEQ_LENGTH = 4;
    private final TaskRepository taskRepository;
    private final AccountService accountService;
    private final RoomService roomService;
    private final SequenceService sequenceService;
    private final RoomMemberRepository roomMemberRepository;

    @Override
    @Transactional
    public TaskRes createTask(CreateTaskReq req) {
        User user = accountService.getAuthUser();
        Room room = roomService.getRoomByCode(req.getRoomCode());
        RoomMember roomMember = roomMemberRepository.findRoomMemberByUserAndRoomAndStatus(user, room, ECommonStatus.ACTIVE)
                .orElseThrow(
                        () -> new BusinessException(ErrorCode.ROOM_MEMBER_NOT_FOUND, "room member not found")
                );
        if (ETaskType.ROOM_TASK.equals(req.getType())) {
            List<RoomMember> memberList = roomMemberRepository.getCurrentMembers(room);
            List<Task> membersTask = new ArrayList<>();
            String schemeCode = this.buildSchemeCode(room.getId());
            Consumer<RoomMember> consumer = c -> {
                Task task = createMembersTask(c.getUser(), room, req, schemeCode);
                membersTask.add(task);
            };
            memberList.forEach(consumer);
            return TaskRes.valueOf(membersTask.get(0));
        }
        Task task = createMembersTask(user, room, req, null);

        return TaskRes.valueOf(task);
    }

    private Task createMembersTask(User user, Room room, CreateTaskReq req, String schemeCode) {
        Task task = new Task();
        task.setUser(user);
        task.setRoom(room);
        task.setCode(this.buildTaskCode(req.getType()));
        task.setScheme(schemeCode);
        task.setDescription(req.getDescription());
        if (ETaskType.ROOM_TASK.equals(req.getType())) {
            task.setScheme(schemeCode);
        }
        task.setTaskType(req.getType());
        task.setStatus(ETaskStatus.UNDONE);

        return taskRepository.save(task);
    }

    @Override
    public void updateTask(UpdateTaskReq req) {
        Task task = taskRepository.findTaskByCode(req.getCode()).orElseThrow(
                () -> new BusinessException(ErrorCode.TASK_CODE_NOT_FOUND,
                        String.format("Task code not found %s", req.getCode()))
        );
        Room room = roomService.getRoomByCode(req.getRoomCode());
        User user = accountService.getAuthUser();
        RoomMember roomMember = roomMemberRepository.findRoomMemberByUserAndRoomAndStatus(user, room, ECommonStatus.ACTIVE).
                orElseThrow(
                        () -> new BusinessException(ErrorCode.ROOM_MEMBER_NOT_FOUND, "room member not found")
                );
        if (!StringUtils.isEmpty(req.getScheme())) {
            if (!roomMember.getRoleType().equals(ERoomRoleType.MEMBER)) {
                if (req.getStatus() != null && req.getStatus() != ETaskStatus.DELETED) {
                    task.setStatus(req.getStatus());
                }
                this.updateRoomTask(room, req);
            }
        } else {
            task.setStatus(req.getStatus());
            taskRepository.save(task);
        }
    }

    @Transactional(Transactional.TxType.REQUIRES_NEW)
    public void updateRoomTask(Room room, UpdateTaskReq req) {
        List<Task> tasks = taskRepository.findRoomTaskByScheme(room, req.getScheme());
        Consumer<Task> consumer = c -> {
            c.setDescription(req.getDescription());
            if (ETaskStatus.DELETED.equals(req.getStatus())) {
                c.setStatus(req.getStatus());
            }
        };
        tasks.forEach(consumer);
        taskRepository.saveAll(tasks);
    }

    @Override
    public void deleteTask(DeleteTaskReq req) {
        Room room = roomService.getRoomByCode(req.getRoomCode());
        if(StringUtils.isNotEmpty(req.getScheme())) {
            List<Task> tasks = taskRepository.findRoomTaskByScheme(room,req.getScheme());
            for(Task t : tasks) {
                t.setStatus(ETaskStatus.DELETED);
            }
            taskRepository.saveAll(tasks);
            return;
        }
        Task task = taskRepository.findTaskByCode(req.getCode()).orElseThrow(
                () -> new BusinessException(ErrorCode.TASK_CODE_NOT_FOUND,
                        String.format("Task code not found %s", req.getCode()))
        );
        task.setStatus(ETaskStatus.DELETED);
        taskRepository.save(task);
    }


    @Override
    public List<TaskRes> getTaskList(SearchTaskParam params) {
        User user = accountService.getAuthUser();
        return taskRepository.getTaskList(params.getRoomCode(), user, params.getStatus(), params.getType(), params.getScheme())
                .stream()
                .map(TaskRes::valueOf)
                .collect(Collectors.toList());
    }

    @Override
    public DoneTaskRes getMemberDoneTask(GetDoneTaskReq req) {
        Room room = roomService.getRoomByCode(req.getRoomCode());
        User user = accountService.getAuthUser();
        RoomMember roomMember = roomMemberRepository.findRoomMemberByUserAndRoomAndStatus(user, room, ECommonStatus.ACTIVE).
                orElseThrow(
                        () -> new BusinessException(ErrorCode.ROOM_MEMBER_NOT_FOUND, "room member not found")
                );
        List<Task> tasks = taskRepository.findRoomTaskByScheme(room, req.getScheme());
        List<User> userDone = new ArrayList<>();

        Consumer<Task> consumer = c -> {
            if (ETaskStatus.DONE.equals(c.getStatus())) {
                userDone.add(c.getUser());
            }
        };
        tasks.forEach(consumer);

        DoneTaskRes res = new DoneTaskRes();
        res.setTotal(userDone.size());
        res.setMembers(userDone.stream()
                .map(item -> UserProfileRes.valueOf(item,item.getProfile()))
                .collect(Collectors.toList()));
        return res;
    }

    public  String buildTaskCode(ETaskType type) {
        String yy = String.valueOf(LocalDate.now().getYear()).substring(2);
        int curVal = sequenceService.getNextSeq(SeqName.TASK_CODE, yy);
        String seqVal = Strings.formatWithZeroPrefix(curVal, TASK_SEQ_LENGTH);

        return String.format("%s%s%s", type.getValue(), yy, seqVal);
    }

    public String buildSchemeCode(Integer roomId) {
        String yy = String.valueOf(LocalDate.now().getYear()).substring(2);
        int curVal = sequenceService.getNextSeq(SeqName.SCHEME_CODE, roomId, yy);
        String seqVal = Strings.formatWithZeroPrefix(curVal, SCHEME_SEQ_LENGTH);

        return String.format("%s%s%s", roomId.toString(), yy, seqVal);
    }
}
