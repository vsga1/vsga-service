package com.example.vstudy.service.impl;

import com.example.vstudy.config.WhiteboardClient;
import com.example.vstudy.constant.ECommonStatus;
import com.example.vstudy.constant.ERoomRoleType;
import com.example.vstudy.constant.ErrorCode;
import com.example.vstudy.constant.SeqName;
import com.example.vstudy.entity.Room;
import com.example.vstudy.entity.RoomConfiguration;
import com.example.vstudy.entity.RoomFile;
import com.example.vstudy.entity.RoomMember;
import com.example.vstudy.entity.RoomTopic;
import com.example.vstudy.entity.Topic;
import com.example.vstudy.entity.User;
import com.example.vstudy.exception.BusinessException;
import com.example.vstudy.model.req.CreateRoomReq;
import com.example.vstudy.model.req.RoomNoteCreateReq;
import com.example.vstudy.model.req.SearchRoomReq;
import com.example.vstudy.model.req.TimerCreateReq;
import com.example.vstudy.model.req.UpdateRoomPasswordReq;
import com.example.vstudy.model.req.UpdateRoomReq;
import com.example.vstudy.model.res.RoomConfigurationRes;
import com.example.vstudy.model.res.RoomRes;
import com.example.vstudy.model.res.RoomSummaryRes;
import com.example.vstudy.model.res.TopicRes;
import com.example.vstudy.model.res.WhiteboardClientRes;
import com.example.vstudy.repository.RoomConfigurationRepository;
import com.example.vstudy.repository.RoomFileRepository;
import com.example.vstudy.repository.RoomMemberRepository;
import com.example.vstudy.repository.RoomRepository;
import com.example.vstudy.repository.RoomTopicRepository;
import com.example.vstudy.repository.TopicRepository;
import com.example.vstudy.repository.UserProfileRepository;
import com.example.vstudy.service.AccountService;
import com.example.vstudy.service.RoomNoteService;
import com.example.vstudy.service.RoomService;
import com.example.vstudy.service.RoomTimerService;
import com.example.vstudy.service.SequenceService;
import com.example.vstudy.service.StorageService;
import com.example.vstudy.util.OffsetPageableRequest;
import com.example.vstudy.util.Strings;
import com.google.api.services.drive.model.File;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class RoomServiceImpl implements RoomService {
    private static final int ROOM_SEQ_LENGTH = 5;
    private final RoomRepository roomRepository;
    private final RoomConfigurationRepository roomConfigurationRepository;
    private final AccountService accountService;
    private final RoomTimerService roomTimerService;
    private final RoomNoteService roomNoteService;
    private final SequenceService sequenceService;
    private final StorageService storageService;
    private final RoomMemberRepository roomMemberRepository;
    private final UserProfileRepository userProfileRepository;
    private final RoomFileRepository roomFileRepository;
    private final TopicRepository topicRepository;
    private final WhiteboardClient whiteboardClient;
    private final RoomTopicRepository roomTopicRepository;


    @Override
    public RoomRes getRoomDetail(String code) {
        User user = accountService.getAuthUser();
        Room room = roomRepository
                .findRoomByCodeAndStatus(code, ECommonStatus.ACTIVE)
                .orElseThrow(() -> new BusinessException(ErrorCode.ROOM_NOT_FOUND, "Room not found."));
        RoomConfiguration roomConfiguration = roomConfigurationRepository.findByRoom(room);
        RoomRes res = RoomRes.valueOf(room);
        res.setFavored(room.getFavoredBy().contains(user.getUsername()));
        res.setConfiguration(RoomConfigurationRes.valueOf(roomConfiguration));
        res.setTotalMember(this.countRoomMembers(room));
        return res;
    }

    @Override
    public Page<RoomRes> getRoomList(Pageable pageReq) {
        return roomRepository.findAll(pageReq).map(RoomRes::valueOf);
    }

    @Override
    public List<RoomRes> getLatestRooms() {
        User user = accountService.getAuthUser();
        List<RoomMember> roomMemberList = roomMemberRepository.getRoomsByMember(user, ECommonStatus.ACTIVE);
        return roomMemberList
                .stream()
                .limit(5)
                .filter(item -> item.getRoom().getStatus().equals(ECommonStatus.ACTIVE))
                .map(item -> {
                    RoomRes roomRes = RoomRes.valueOf(item.getRoom());
                    roomRes.setFavored(item.getRoom().getFavoredBy().contains(user.getUsername()));
                    roomRes.setTotalMember(this.countRoomMembers(item.getRoom()));
                    return roomRes;
                })
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public Page<RoomRes> searchRoom(SearchRoomReq req, Pageable pageReq) {
        List<String> topics;
        List<Topic> topicList = new ArrayList<>();
        if (!CollectionUtils.isEmpty(req.getTopics())) {
            topics = req.getTopics().stream()
                    .map(item -> item.toUpperCase())
                    .collect(Collectors.toList());
            for (String name : topics) {
                name = name.toUpperCase();
                Topic topic = topicRepository.findTopicByName(name)
                        .orElseThrow(
                                () -> new BusinessException(ErrorCode.TOPIC_NAME_NOT_FOUND, "Topic not found")
                        );
                topicList.add(topic);
            }
        } else {
            topics = null;
        }
        User user = accountService.getAuthUser();
        req.setUserId(user.getId());
        req.setUsername(user.getUsername());

        Page<Room> rooms = roomRepository.searchRoom(req,topicList, pageReq);
        List<Room> roomAfterFilter = rooms.getContent();
        if (!CollectionUtils.isEmpty(topicList)) {
            roomAfterFilter = roomAfterFilter.stream().filter(
                    item -> {
                        if (CollectionUtils.isEmpty(item.getTopics())) return false;
                        for (Topic topic : topicList) {
                            List<Topic> roomTopics = item.getTopics()
                                    .stream()
                                    .map(RoomTopic::getTopic)
                                    .collect(Collectors.toList());
                            if (!roomTopics.contains(topic)) return false;
                        }
                        return true;
                    }
            ).collect(Collectors.toList());
        }
        List<RoomRes> roomRes = roomAfterFilter.stream().map(item -> {
            RoomRes roomResTmp = RoomRes.valueOf(item);
            roomResTmp.setFavored(item.getFavoredBy().contains(user.getUsername()));
            roomResTmp.setTotalMember(this.countRoomMembers(item));
            return roomResTmp;
        }).collect(Collectors.toList());
        return new PageImpl<>(roomRes, pageReq, rooms.getTotalElements());
    }

    private int countRoomMembers(Room room) {
       return roomMemberRepository.countCurrentMember(room,Boolean.TRUE,ECommonStatus.ACTIVE);
    }
    @Override
    public Page<RoomRes> adminSearchRoom(SearchRoomReq req, Pageable pageReq) {
        List<String> topics;
        List<Topic> topicList = new ArrayList<>();
        if (!CollectionUtils.isEmpty(req.getTopics())) {
            topics = req.getTopics().stream()
                    .map(String::toUpperCase)
                    .collect(Collectors.toList());
            for (String name : topics) {
                name = name.toUpperCase();
                Topic topic = topicRepository.findTopicByName(name)
                        .orElseThrow(
                                () -> new BusinessException(ErrorCode.TOPIC_NAME_NOT_FOUND, "Topic not found")
                        );
                topicList.add(topic);
            }
        } else {
            topics = null;
        }
        Page<Room> rooms = roomRepository.searchRoomAdmin(req, pageReq);
        List<Room> roomAfterFilter = rooms.getContent();
        if (!CollectionUtils.isEmpty(topicList)) {
            roomAfterFilter = roomAfterFilter.stream().filter(
                    item -> {
                        if (CollectionUtils.isEmpty(item.getTopics())) return false;
                        for (Topic topic : topicList) {
                            List<Topic> roomTopics = item.getTopics()
                                    .stream()
                                    .map(RoomTopic::getTopic)
                                    .collect(Collectors.toList());
                            if (!roomTopics.contains(topic)) return false;
                        }
                        return true;
                    }
            ).collect(Collectors.toList());
        }
        List<RoomRes> roomRes = roomAfterFilter.stream().map(RoomRes::valueOf).collect(Collectors.toList());
        return new PageImpl<>(roomRes, pageReq, rooms.getTotalElements());
    }


    @Override
    public Page<RoomRes> getRoomListByUser(int offset, int limit) {
        User authUser = accountService.getAuthUser();

        OffsetPageableRequest offsetPageableRequest = new OffsetPageableRequest(offset, limit);
        return roomRepository.findAllByUser(authUser, offsetPageableRequest).map(RoomRes::valueOf);
    }

    @Override
    @Transactional
    public RoomRes createRoom(CreateRoomReq req) throws GeneralSecurityException, IOException {
        User authUser = accountService.getAuthUser();

        Room room = new Room();
        room.setCode(UUID.randomUUID().toString().replaceAll("-", ""));
        room.setName(req.getName());
        room.setDescription(req.getDescription());
        room.setUser(authUser);
        room.setRoomType(req.getRoomType());
        room.setCapacity(req.getCapacity());
        room.setTotalMember(1);
        room.setStatus(ECommonStatus.ACTIVE);
        // create room whiteboard
        WhiteboardClientRes clientRes = whiteboardClient.getWhiteboardId();
        room.setWhiteboardId(clientRes.getUuid());

        RoomRes roomRes = RoomRes.valueOf(roomRepository.saveAndFlush(room));

        roomRes.setConfiguration(createRoomConfiguration(room, req));

        this.createHostMember(room);
        // create timer
        TimerCreateReq timerReq = new TimerCreateReq();
        timerReq.setRoomCode(room.getCode());
        timerReq.setUpdatedAt(OffsetDateTime.now());
        timerReq.setHour(0);
        timerReq.setMinute(0);
        timerReq.setSecond(0);
        roomTimerService.createRoomTimer(timerReq);

        // create room note
        RoomNoteCreateReq noteCreateReq = new RoomNoteCreateReq();
        noteCreateReq.setRoomCode(room.getCode());
        noteCreateReq.setHeader("Room Note");
        noteCreateReq.setDescription(" -- Description --");
        roomNoteService.createRoomNote(noteCreateReq);

        // Create room topics
        createRoomTopics(req.getTopics(), room);


        // create room storage folder
        room.setStorageFolderId(storageService.createFolder(room.getName()).getId());
        roomRepository.save(room);
        return roomRes;
    }

    @Transactional
    private List<RoomTopic> createRoomTopics(List<String> topics, Room room) {
        List<RoomTopic> roomTopics = new ArrayList<>();
        for (String name : topics) {
            name = name.toUpperCase();
            Topic topic = topicRepository.findTopicByName(name)
                    .orElseThrow(
                            () -> new BusinessException(ErrorCode.TOPIC_NAME_NOT_FOUND, "Topic not found")
                    );
            RoomTopic roomTopic = new RoomTopic();
            roomTopic.setTopic(topic);
            roomTopic.setRoom(room);
            roomTopics.add(roomTopic);
        }
        return roomTopicRepository.saveAllAndFlush(roomTopics);
    }

    @Override
    @Transactional
    public RoomRes updateRoom(String roomCode, UpdateRoomReq req) {
        Room room = this.getRoomByCode(roomCode);
        RoomConfiguration roomConfiguration = roomConfigurationRepository.findByRoom(room);
        room.setName(req.getName());
        roomRepository.save(room);
        roomConfigurationRepository.save(roomConfiguration);
        RoomRes res = RoomRes.valueOf(room);
        res.setTotalMember(this.countRoomMembers(room));
        return res;
    }

    @Override
    public void updateRoomPassword(String roomCode, UpdateRoomPasswordReq req) {
        Room room = this.getRoomByCode(roomCode);
        RoomConfiguration roomConfiguration = roomConfigurationRepository.findByRoom(room);
        if (StringUtils.isBlank(roomConfiguration.getPassword())) {
            roomConfiguration.setPassword(req.getNewPassword());
        } else {
            if (!req.getCurrentPassword().equals(roomConfiguration.getPassword())) {
                throw new BusinessException(ErrorCode.ROOM_PASSWORD_IN_CORRECT, "Current password incorrect");
            } else {
                roomConfiguration.setPassword(req.getNewPassword());
            }
        }
        roomConfigurationRepository.save(roomConfiguration);
    }

    private RoomConfigurationRes createRoomConfiguration(Room room, CreateRoomReq req) {
        RoomConfiguration roomConfiguration = new RoomConfiguration();
        roomConfiguration.setRoom(room);
        if (StringUtils.isNotEmpty(req.getPassword())) {
            roomConfiguration.setPassword(req.getPassword());
        }
        return RoomConfigurationRes
                .valueOf(roomConfigurationRepository.save(roomConfiguration));
    }


    @Override
    public Room getRoomByCode(String code) {
        return roomRepository
                .findRoomByCodeAndStatus(code, ECommonStatus.ACTIVE)
                .orElseThrow(() -> new BusinessException(ErrorCode.ROOM_NOT_FOUND, "Room not found."));
    }

    @Override
    @Transactional(Transactional.TxType.SUPPORTS)
    public Room lockAndCheckRoom(String code) {
        Room room = roomRepository
                .lockRoom(code, ECommonStatus.ACTIVE)
                .orElseThrow(() -> new BusinessException(ErrorCode.ROOM_NOT_FOUND, "Room not found."));
        int countMember = roomMemberRepository.countCurrentMember(room,Boolean.TRUE,ECommonStatus.ACTIVE);
        if (countMember + 1 > room.getCapacity()) {
            throw new BusinessException(ErrorCode.ROOM_IS_FULL, "Room is full");
        }
        return room;
    }


    private String buildRoomCode(String roomType) {
        String yy = String.valueOf(LocalDate.now().getYear()).substring(2);
        int curVal = sequenceService.getNextSeq(SeqName.GROUP_CODE, yy);
        String seqVal = Strings.formatWithZeroPrefix(curVal, ROOM_SEQ_LENGTH);

        return String.format("%s%s%s", roomType, yy, seqVal);
    }

    @Override
    public RoomMember findUserInRoom(String roomCode) {
        User user = accountService.getAuthUser();
        Room room = this.getRoomByCode(roomCode);

        return roomMemberRepository.findRoomMemberByUserAndRoomAndStatus(user, room, ECommonStatus.ACTIVE).orElse(null);
    }

    @Override
    public RoomMember createRoomMember(Room room, User user, ERoomRoleType roleType) {
        RoomMember roomMember = new RoomMember();
        roomMember.setUser(user);
        roomMember.setRoom(room);
        roomMember.setRoleType(roleType);
        roomMember.setIsJoining(Boolean.TRUE);
        roomMember.setStatus(ECommonStatus.ACTIVE);

        return roomMemberRepository.save(roomMember);
    }

    @Override
    public void memberJoinRoom(RoomMember roomMember) {
        roomMember.setIsJoining(Boolean.TRUE);
        roomMemberRepository.save(roomMember);
    }

    @Override
    public void memberOutRoom(RoomMember roomMember) {
        roomMember.setIsJoining(Boolean.FALSE);
        roomMemberRepository.save(roomMember);
    }

    @Override
    @Transactional
    public List<RoomMember> getCurrentMembers(String roomCode) {
        Room room = this.getRoomByCode(roomCode);
        return roomMemberRepository.getCurrentMembers(room);
    }

    @Override
    public void createHostMember(Room room) {
        User user = accountService.getAuthUser();
        RoomMember roomMember = new RoomMember();
        roomMember.setUser(user);
        roomMember.setRoom(room);
        roomMember.setRoleType(ERoomRoleType.HOST);
        roomMember.setIsJoining(Boolean.TRUE);
        roomMember.setStatus(ECommonStatus.ACTIVE);
        roomMemberRepository.save(roomMember);
    }

    @Override
    public void addFavoriteRoom(String roomCode) {
        User user = accountService.getAuthUser();
        Room room = getRoomByCode(roomCode);

        ArrayList<String> favoriteRooms = new ArrayList<>(room.getFavoredBy());
        if (!favoriteRooms.contains(user.getUsername())) {
            favoriteRooms.add(user.getUsername());
        }
        room.setFavoredBy(favoriteRooms);
        roomRepository.save(room);
    }

    @Override
    public void removeFavoriteRoom(String roomCode) {
        User user = accountService.getAuthUser();
        Room room = getRoomByCode(roomCode);

        ArrayList<String> favoriteRooms = new ArrayList<>(room.getFavoredBy());
        favoriteRooms.remove(user.getUsername());
        room.setFavoredBy(favoriteRooms);
        roomRepository.save(room);
    }

    @Override
    public List<File> getRoomResources(String roomCode) throws GeneralSecurityException, IOException {
        Room room = this.getRoomByCode(roomCode);
        if (StringUtils.isEmpty(room.getStorageFolderId())) {
            throw new BusinessException(ErrorCode.ROOM_STORAGE_NOT_FOUND, "Room storage not found");
        }
        return storageService.getRoomFiles(room.getStorageFolderId());
    }

    @Override
    public File createResource(String roomCode, MultipartFile file) throws GeneralSecurityException, IOException {
        Room room = this.getRoomByCode(roomCode);
        return storageService.createFile(file, room.getStorageFolderId());
    }

    @Override
    public File createFolder(String roomCode) throws GeneralSecurityException, IOException {
        Room room = this.getRoomByCode(roomCode);
        File folder = storageService.createFolder(room.getName());
        room.setStorageFolderId(folder.getId());
        roomRepository.save(room);
        return folder;
    }

    @Override
    public File createFolder2() throws GeneralSecurityException, IOException {
        return storageService.createFolder("Avatar");
    }

    @Override
    @Transactional
    public void deleteFile(String roomCode, String fileId) throws GeneralSecurityException, IOException {
        Room room = this.getRoomByCode(roomCode);
        RoomFile roomFile = roomFileRepository.findRoomFileByStorageFolderIdAndFileId(room.getStorageFolderId(), fileId)
                .orElseThrow(
                        () -> new BusinessException(ErrorCode.FILE_NOT_FOUND, "File not found")
                );
        storageService.deleteFile(fileId);
        roomFileRepository.delete(roomFile);
    }

    @Override
    public void lockRoom(String code) {
        Room room = this.getRoomByCode(code);
        room.setStatus(ECommonStatus.INACTIVE);
        roomRepository.save(room);
    }

    @Override
    public void unlockRoom(String code) {
        Room room = roomRepository
                .findRoomByCodeAndStatus(code, ECommonStatus.INACTIVE)
                .orElseThrow(() -> new BusinessException(ErrorCode.ROOM_NOT_FOUND, "Room not found."));
        room.setStatus(ECommonStatus.ACTIVE);
        roomRepository.save(room);
    }

    @Override
    public RoomSummaryRes roomSummary() {
        Integer countRoomActive = roomRepository.countRoomByStatus(ECommonStatus.ACTIVE);
        Integer countRoomInactive = roomRepository.countRoomByStatus(ECommonStatus.INACTIVE);
        RoomSummaryRes res = new RoomSummaryRes();
        res.setTotalInactiveRoom(countRoomInactive);
        res.setTotalActiveRoom(countRoomActive);

        List<Object[]> topicSummaryObj = roomTopicRepository.summaryTopic();
        List<RoomSummaryRes.TopicSummary> topicRes = new ArrayList<>();
        for (int i=0;i<5;i++) {
            Object[] obj = topicSummaryObj.get(i);
            RoomSummaryRes.TopicSummary topic = new RoomSummaryRes.TopicSummary();
            topic.setQuantity((Long) obj[1]);
            topic.setTopic((String) obj[0]);
            topicRes.add(topic);
        }
        res.setTopicSummary(topicRes);
        return res;
    }

    private List<TopicRes> getRoomTopicRes(List<String> topics) {
        if (CollectionUtils.isEmpty(topics)) {
            return null;
        }
        List<Topic> topicList = topicRepository.findAllByNameIn(topics);
        return topicList.stream()
                .map(TopicRes::valueOf)
                .collect(Collectors.toList());
    }
}
