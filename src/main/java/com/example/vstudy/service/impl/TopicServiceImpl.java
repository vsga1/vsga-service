package com.example.vstudy.service.impl;

import com.example.vstudy.constant.ErrorCode;
import com.example.vstudy.entity.RoomTopic;
import com.example.vstudy.entity.Topic;
import com.example.vstudy.exception.BusinessException;
import com.example.vstudy.model.req.CreateTopicReq;
import com.example.vstudy.model.res.TopicRes;
import com.example.vstudy.repository.RoomTopicRepository;
import com.example.vstudy.repository.TopicRepository;
import com.example.vstudy.service.TopicService;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class TopicServiceImpl implements TopicService {
    private final TopicRepository topicRepository;
    private final RoomTopicRepository roomTopicRepository;

    @Override
    public void createTopics(CreateTopicReq req) {
        String checkName = req.getName().toUpperCase();
        Optional<Topic> record = topicRepository.findTopicByName(checkName);
        if(record.isPresent()) {
            throw new BusinessException(ErrorCode.TOPIC_NAME_IS_EXIST,"Topic is exist");
        }
        Topic topic = new Topic();
        topic.setCode(UUID.randomUUID().toString().replaceAll("-", ""));
        if(StringUtils.isEmpty(req.getColor())) {
            topic.setColor("#0000EE");
        } else {
            topic.setColor(req.getColor());
        }

        topic.setName(checkName);

        topicRepository.save(topic);
    }

    @Override
    public void deleteTopic(String name) {
        String checkName = name.toUpperCase();
        Optional<Topic> record = topicRepository.findTopicByName(checkName);
        if(record.isEmpty()) {
            throw new BusinessException(ErrorCode.TOPIC_NAME_INVALID_EXIST,"Topic invalid");
        } else {
            topicRepository.delete(record.get());
        }
    }

    @Override
    public List<TopicRes> getTopics(String topic) {
        return topicRepository.findTopics("%" + topic + "%")
                .stream()
                .map(TopicRes::valueOf)
                .collect(Collectors.toList());
    }

    @Override
    public List<TopicRes> suggestTopics() {
        return topicRepository.suggestTopics()
                .stream()
                .map(TopicRes::valueOf)
                .collect(Collectors.toList());
    }

    @Override
    public Page<TopicRes> searchTopicAdmin(String name, Pageable pageable) {
        String topic = null;
        if(name != null) {
            topic = "%" + name + "%";
        }
        Page<Topic> topics = topicRepository.searchTopics(topic,pageable);
        List<TopicRes> topicRes = topics.stream().map(TopicRes::valueOf).collect(Collectors.toList());
        return new PageImpl<>(topicRes,pageable,topics.getTotalElements());
    }

    @Override
    public void lock(String topic) {
        Topic topicEntity = topicRepository.findTopicByName(topic)
                .orElseThrow(
                        () -> new BusinessException(ErrorCode.TOPIC_NAME_NOT_FOUND,"Topic not found")
                );

        List<RoomTopic> roomTopics = roomTopicRepository.findAllByTopic(topicEntity);
        roomTopicRepository.deleteAll(roomTopics);
        topicRepository.delete(topicEntity);
    }

}
