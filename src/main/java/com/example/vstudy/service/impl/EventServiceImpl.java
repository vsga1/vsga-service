package com.example.vstudy.service.impl;

import com.example.vstudy.constant.ECommonStatus;
import com.example.vstudy.constant.ERoomRoleType;
import com.example.vstudy.constant.ETaskStatus;
import com.example.vstudy.constant.ETaskType;
import com.example.vstudy.constant.ErrorCode;
import com.example.vstudy.constant.SeqName;
import com.example.vstudy.entity.Room;
import com.example.vstudy.entity.RoomConfiguration;
import com.example.vstudy.entity.RoomMember;
import com.example.vstudy.entity.Task;
import com.example.vstudy.entity.User;
import com.example.vstudy.entity.UserProfile;
import com.example.vstudy.exception.BusinessException;
import com.example.vstudy.model.req.DemoteReq;
import com.example.vstudy.model.req.JoinRoomReq;
import com.example.vstudy.model.req.PromoteReq;
import com.example.vstudy.model.res.CurrentRoomMemberRes;
import com.example.vstudy.model.res.JoinRoomRes;
import com.example.vstudy.model.res.RoomConfigurationRes;
import com.example.vstudy.model.res.RoomMemberRes;
import com.example.vstudy.model.res.RoomRes;
import com.example.vstudy.repository.RoomConfigurationRepository;
import com.example.vstudy.repository.RoomMemberRepository;
import com.example.vstudy.repository.RoomRepository;
import com.example.vstudy.repository.RoomTimerRepository;
import com.example.vstudy.repository.TaskRepository;
import com.example.vstudy.service.AccountService;
import com.example.vstudy.service.EventService;
import com.example.vstudy.service.RoomService;
import com.example.vstudy.service.SequenceService;
import com.example.vstudy.util.Strings;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;


@Service
@RequiredArgsConstructor
public class EventServiceImpl implements EventService {
    private static final int TASK_SEQ_LENGTH = 6;
    private static final int SCHEME_SEQ_LENGTH = 4;

    private final AccountService accountService;

    private final RoomService roomService;

    private final RoomConfigurationRepository roomConfigurationRepository;

    private final RoomMemberRepository roomMemberRepository;

    private final RoomRepository roomRepository;

    private final TaskRepository taskRepository;

    private final RoomTimerRepository timerRepository;

    private final SequenceService sequenceService;

    @Override
    public RoomMember checkUserInRoom(String roomCode) {
        return roomService.findUserInRoom(roomCode);
    }

    @Override
    @Transactional
    public JoinRoomRes enterRoom(JoinRoomReq req) {
        User user = accountService.getAuthUser();
        Room room = roomService.lockAndCheckRoom(req.getRoomCode());
        RoomConfiguration roomConfiguration = roomConfigurationRepository.findByRoom(room);
        if (!StringUtils.isBlank(room.getConfiguration().getPassword())) {
            if (req.getPassword() == null) {
                throw new BusinessException(ErrorCode.MISSING_PASSWORD_VALUE, "Missing password value");
            } else {
                if (!req.getPassword().equals(roomConfiguration.getPassword())) {
                    throw new BusinessException(ErrorCode.ROOM_PASSWORD_INVALID, "Password invalid");
                }
            }
        }

        RoomMember roomMember = checkUserInRoom(req.getRoomCode());
        CurrentRoomMemberRes currentRoomMemberRes = this.getCurrentMembers(req.getRoomCode());
        if (roomMember == null) {
            if(CollectionUtils.isEmpty(currentRoomMemberRes.getMembers())) {
                throw new BusinessException(ErrorCode.PLEASE_WAIT_FOR_THE_HOST_TO_START,"Host not start");
            }
            roomMember = roomService.createRoomMember(room, user, ERoomRoleType.MEMBER);
            room.incr(1);
            this.createRoomTaskForNewMember(room, user);
        } else {
            if (roomMember.getRoleType().equals(ERoomRoleType.MEMBER)) {
                if (CollectionUtils.isEmpty(currentRoomMemberRes.getMembers())) {
                    throw new BusinessException(ErrorCode.PLEASE_WAIT_FOR_THE_HOST_TO_START, "Host not start");
                }
                this.checkRoomCondition(currentRoomMemberRes.getMembers());
            }
            if (roomMember.getIsJoining().equals(Boolean.FALSE)) {
                roomMember.setIsJoining(Boolean.TRUE);
                room.incr(1);
            }
        }
        roomRepository.save(room);
        roomMemberRepository.save(roomMember);

        RoomRes roomRes = RoomRes.valueOf(room);
        roomRes.setConfiguration(RoomConfigurationRes.valueOf(roomConfiguration));

        // set room info
        JoinRoomRes res = new JoinRoomRes();
        res.setRoomInfo(roomRes);
        res.setRole(roomMember.getRoleType());

        res.setTotalMember(currentRoomMemberRes.getCurrentMember());
        res.setMembers(currentRoomMemberRes.getMembers());
        return res;
    }

    private void checkRoomCondition(List<RoomMemberRes> members) {
        Optional<RoomMemberRes> host = members.stream().filter(item -> item.getRole().equals(ERoomRoleType.HOST)
                || item.getRole().equals(ERoomRoleType.CO_HOST)).findFirst();
        if(host.isEmpty()) {
            throw new BusinessException(ErrorCode.PLEASE_WAIT_FOR_THE_HOST_TO_START,"Host not start");
        }
    }

    @Transactional(Transactional.TxType.REQUIRES_NEW)
    public void createRoomTaskForNewMember(Room room, User user) {
        RoomMember host = roomMemberRepository.getHostByRoom(room);
        List<Task> listRoomTask = taskRepository.findRoomTask(room, host.getUser());
        List<Task> newTask = new ArrayList<>();
        Consumer<Task> consumer = t -> {
            Task tmp = new Task();
            tmp.setRoom(room);
            tmp.setUser(user);
            tmp.setTaskType(ETaskType.ROOM_TASK);
            tmp.setDescription(t.getDescription());
            tmp.setStatus(ETaskStatus.UNDONE);
            tmp.setCode(this.buildTaskCode(ETaskType.ROOM_TASK));
            tmp.setScheme(t.getScheme());

            newTask.add(tmp);
        };
        listRoomTask.forEach(consumer);
        taskRepository.saveAll(newTask);
    }

    @Override
    @Transactional
    public void outRoom(String roomCode) {
        User user = accountService.getAuthUser();
        Room room = roomService.getRoomByCode(roomCode);
        RoomMember roomMember = roomMemberRepository.findRoomMemberByUserAndRoomAndStatus(user, room, ECommonStatus.ACTIVE)
                .orElseThrow(
                        () -> new BusinessException(ErrorCode.ROOM_MEMBER_NOT_FOUND, "Room member not found")
                );
        if (roomMember.getIsJoining()) {
            room.incr(-1);
            roomMember.setIsJoining(Boolean.FALSE);
        }
        roomRepository.save(room);
        roomMemberRepository.saveAndFlush(roomMember);

        List<RoomMember> currentMembers = roomMemberRepository.getCurrentMembers(room);
        if(!CollectionUtils.isEmpty(currentMembers)) {
         // kick out all member
            if(currentMembers.stream().noneMatch(item  -> item.getRoleType().equals(ERoomRoleType.HOST)
                    || item.getRoleType().equals(ERoomRoleType.CO_HOST))) {
                this.kickOutAllMember(currentMembers);
            }
            room.incr(-currentMembers.size());
            roomRepository.save(room);
        }
    }
    @Transactional(Transactional.TxType.SUPPORTS)
    public void kickOutAllMember(List<RoomMember> members) {
        for(RoomMember rm : members) {
            if (rm.getIsJoining()) {
                rm.setIsJoining(Boolean.FALSE);
            }
        }
    }
    @Override
    public CurrentRoomMemberRes getCurrentMembers(String roomCode) {
        List<RoomMember> roomMemberList = roomService.getCurrentMembers(roomCode);
        CurrentRoomMemberRes res = new CurrentRoomMemberRes();
        if (CollectionUtils.isEmpty(roomMemberList)) {
            res.setCurrentMember(0);
        } else {
            res.setCurrentMember(roomMemberList.size());
            List<RoomMemberRes> memberRes = new ArrayList<>();
            Consumer<RoomMember> consumer = c -> {
                RoomMemberRes roomMemberRes = new RoomMemberRes();
                roomMemberRes.setUsername(c.getUser().getUsername());
                if (c.getUser().getProfile() != null) {
                    UserProfile profile = c.getUser().getProfile();
                    roomMemberRes.setFullName(profile.getFullName());
                    roomMemberRes.setDisplayName(profile.getDisplayName());
                    roomMemberRes.setDescription(profile.getDescription());
                    roomMemberRes.setGender(profile.getGender());
                    roomMemberRes.setAge(profile.getAge());
                    roomMemberRes.setAvatarImgUrl(profile.getAvatarImgUrl());
                }
                roomMemberRes.setUsername(c.getUser().getUsername());
                roomMemberRes.setRole(c.getRoleType());
                roomMemberRes.setRoomCode(c.getRoom().getCode());
                memberRes.add(roomMemberRes);
            };
            roomMemberList.forEach(consumer);
            res.setMembers(memberRes);
        }
        return res;
    }

    @Override
    @Transactional
    public void promoteMember(PromoteReq req) {
        User maker = accountService.getAuthUser();
        User promoter = accountService.findByUserName(req.getUsername());
        Room room = roomService.getRoomByCode(req.getRoomCode());

        RoomMember makerMember = roomMemberRepository.lockAndCheck(maker, room, ECommonStatus.ACTIVE)
                .orElseThrow(
                        () -> new BusinessException(ErrorCode.ROOM_MEMBER_NOT_FOUND, "Room member maker not found")
                );
        RoomMember promoterMember = roomMemberRepository.lockAndCheck(promoter, room, ECommonStatus.ACTIVE)
                .orElseThrow(
                        () -> new BusinessException(ErrorCode.ROOM_MEMBER_NOT_FOUND, "Room member promoter not found")
                );

        if (ERoomRoleType.MEMBER.equals(makerMember.getRoleType())) {
            throw new BusinessException(ErrorCode.MEMBER_CANNOT_PROMOTE, "Member cannot promote");
        } else {
            if (promoterMember.getRoleType().equals(ERoomRoleType.HOST)) {
                throw new BusinessException(ErrorCode.CANNOT_ACTION_ON_HOST, "Cannot action on host");
            }
            promoterMember.setRoleType(ERoomRoleType.CO_HOST);
        }
        roomMemberRepository.save(promoterMember);
    }

    @Override
    @Transactional
    public void demoteMember(DemoteReq req) {
        User maker = accountService.getAuthUser();
        User demoted = accountService.findByUserName(req.getUsername());
        Room room = roomService.getRoomByCode(req.getRoomCode());

        RoomMember makerMember = roomMemberRepository.lockAndCheck(maker, room, ECommonStatus.ACTIVE)
                .orElseThrow(
                        () -> new BusinessException(ErrorCode.ROOM_MEMBER_NOT_FOUND, "Room member maker not found")
                );
        RoomMember demotedMember = roomMemberRepository.lockAndCheck(demoted, room, ECommonStatus.ACTIVE)
                .orElseThrow(
                        () -> new BusinessException(ErrorCode.ROOM_MEMBER_NOT_FOUND, "Room member promoter not found")
                );
        if (demotedMember.getRoleType().equals(ERoomRoleType.HOST)) {
            throw new BusinessException(ErrorCode.MEMBER_CANNOT_PROMOTE, "Member cannot promote");
        } else {
            if (!makerMember.getRoleType().equals(ERoomRoleType.HOST)) {
                throw new BusinessException(ErrorCode.ONLY_HOST_CAN_DEMOTE, "Only HOST can demote");
            } else if (demotedMember.getRoleType().equals(ERoomRoleType.CO_HOST)) {
                demotedMember.setRoleType(ERoomRoleType.MEMBER);
                roomMemberRepository.save(demotedMember);
            }
        }
    }


    public String buildTaskCode(ETaskType type) {
        String yy = String.valueOf(LocalDate.now().getYear()).substring(2);
        int curVal = sequenceService.getNextSeq(SeqName.TASK_CODE, yy);
        String seqVal = Strings.formatWithZeroPrefix(curVal, TASK_SEQ_LENGTH);

        return String.format("%s%s%s", type.getValue(), yy, seqVal);
    }

    public String buildSchemeCode(Integer roomId) {
        String yy = String.valueOf(LocalDate.now().getYear()).substring(2);
        int curVal = sequenceService.getNextSeq(SeqName.SCHEME_CODE, roomId, yy);
        String seqVal = Strings.formatWithZeroPrefix(curVal, SCHEME_SEQ_LENGTH);

        return String.format("%s%s%s", roomId.toString(), yy, seqVal);
    }
}
