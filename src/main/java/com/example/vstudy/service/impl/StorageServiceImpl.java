package com.example.vstudy.service.impl;

import com.example.vstudy.VStudyApplication;
import com.example.vstudy.constant.ErrorCode;
import com.example.vstudy.entity.RoomFile;
import com.example.vstudy.exception.BusinessException;
import com.example.vstudy.repository.RoomFileRepository;
import com.example.vstudy.service.StorageService;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.batch.BatchRequest;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.http.FileContent;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import com.google.api.services.drive.model.Permission;

import javax.transaction.Transactional;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Service
@AllArgsConstructor
public class StorageServiceImpl implements StorageService {
    private final RoomFileRepository roomFileRepository;
    /**
     * Application name.
     */
    public static final String APPLICATION_NAME = "Google Drive API Java Quickstart";
    /**
     * Global instance of the JSON factory.
     */
    private static final JsonFactory JSON_FACTORY = GsonFactory.getDefaultInstance();
    /**
     * Directory to store authorization tokens for this application.
     */
    private static final String TOKENS_DIRECTORY_PATH = "tokens";

    /**
     * Global instance of the scopes required by this quickstart.
     * If modifying these scopes, delete your previously saved tokens/ folder.
     */
    private static final List<String> SCOPES =
            Collections.singletonList(DriveScopes.DRIVE_FILE);
    private static final String CREDENTIALS_FILE_PATH = "/credentials.json";


    public Drive buildAuthorized() throws GeneralSecurityException, IOException {
        // Build a new authorized API client service.
        final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
        return new Drive.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
                .setApplicationName(APPLICATION_NAME)
                .build();
    }

    private Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT)
            throws IOException {
        // Load client secrets.
        InputStream in = VStudyApplication.class.getResourceAsStream(CREDENTIALS_FILE_PATH);
        if (in == null) {
            throw new FileNotFoundException("Resource not found: " + CREDENTIALS_FILE_PATH);
        }
        GoogleClientSecrets clientSecrets =
                GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

        // Build flow and trigger user authorization request.
        GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
                HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES)
                .setDataStoreFactory(new FileDataStoreFactory(new java.io.File(TOKENS_DIRECTORY_PATH)))
                .setAccessType("offline")
                .build();
        LocalServerReceiver receiver = new LocalServerReceiver.Builder().setPort(8888).build();
        Credential credential = new AuthorizationCodeInstalledApp(flow, receiver).authorize("user");
        //returns an authorized Credential object.
        return credential;
    }

    @Override
    public List<File> getAllFiles() throws GeneralSecurityException, IOException {
        // Print the names and IDs for up to 10 files.
        Drive service = this.buildAuthorized();
        FileList result = service.files().list()
                .setPageSize(10)
                .setFields("nextPageToken, files(id, name)")
                .execute();
        List<File> files = result.getFiles();
        if (files == null || files.isEmpty()) {
            System.out.println("No files found.");
        } else {
            System.out.println("Files:");
            for (com.google.api.services.drive.model.File file : files) {
                System.out.printf("%s (%s)\n", file.getName(), file.getId());
            }
        }
        return files;
    }

    @Override
    public File createFile(MultipartFile uploadFile, String folderId) throws IOException, GeneralSecurityException {
        if(uploadFile.isEmpty()) {
            throw new BusinessException(ErrorCode.FILE_CANNOT_NULL,"File cannot null");
        }
        Drive service = this.buildAuthorized();

        File fileMetadata = new File();
        fileMetadata.setName(uploadFile.getOriginalFilename());
        fileMetadata.setParents(Collections.singletonList(folderId));
        // File's content.
        java.io.File filePath = convertToFile(uploadFile);
        // Specify media type and file-path for file.
        FileContent mediaContent = new FileContent(uploadFile.getContentType(), filePath);


        try {
            File file = service.files().create(fileMetadata, mediaContent)
                    .setFields("id,name,webViewLink")
                    .execute();
            RoomFile roomFile = new RoomFile();
            roomFile.setStorageFolderId(folderId);
            roomFile.setFileId(file.getId());
            roomFileRepository.save(roomFile);
            return file;
        } catch (GoogleJsonResponseException e) {
            // TODO(developer) - handle error appropriately
            System.err.println("Unable to upload file: " + e.getDetails());
            throw e;
        }
    }

    public java.io.File convertToFile(MultipartFile multipartFile) {
        java.io.File file = new java.io.File("targetFile.tmp");

        try (OutputStream os = new FileOutputStream(file)) {
            os.write(multipartFile.getBytes());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return file;
    }

    @Override
    @Transactional
    public File createFolder(String roomName) throws IOException, GeneralSecurityException {
        Drive service = this.buildAuthorized();
        // File's metadata.
        File fileMetadata = new File();
        fileMetadata.setName(roomName);
        fileMetadata.setMimeType("application/vnd.google-apps.folder");

        //Permission
        Permission userPermission = new Permission()
                .setType("anyone")
                .setRole("writer");
        BatchRequest batch = service.batch();

        try {
            File file = service.files().create(fileMetadata)
                    .setFields("id")
                    .execute();
            System.out.println("Folder ID: " + file.getId());
            service.permissions().create(file.getId(), userPermission).execute();

            return file;
        } catch (GoogleJsonResponseException e) {
            // TODO(developer) - handle error appropriately
            System.err.println("Unable to create folder: " + e.getDetails());
            throw e;
        }
    }

    @Override
    public List<File> getRoomFiles(String folderId) throws IOException, GeneralSecurityException {
        Drive service = this.buildAuthorized();
        List<File> files = new ArrayList<File>();

        String pageToken = null;
        do {
            FileList result = service.files().list()
                    .setQ(String.format("%s in parents", "'" + folderId + "'"))
                    .setFields("nextPageToken, files(id,name,webViewLink,thumbnailLink,webContentLink,size,iconLink,parents)")
                    .setPageToken(pageToken)
                    .execute();
            for (File file : result.getFiles()) {
                System.out.printf("Found file: %s (%s)\n",
                        file.getName(), file.getId());
            }

            files.addAll(result.getFiles());

            pageToken = result.getNextPageToken();
        } while (pageToken != null);

        return files;
    }

    @Override
    public File getFile(String fileId) throws IOException, GeneralSecurityException {
        Drive service = this.buildAuthorized();
        File result = service.files().get(fileId)
                .setFields("id,name,webViewLink,thumbnailLink,webContentLink")
                .execute();
        return result;
    }

    @Override
    public void deleteFile(String fileId) throws GeneralSecurityException, IOException {
        Drive service = this.buildAuthorized();
        try {
            service.files().delete(fileId).execute();
        } catch (IOException e) {
            System.out.println("An error occurred: " + e);
        }
    }

}
