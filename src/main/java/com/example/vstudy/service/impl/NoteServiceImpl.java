package com.example.vstudy.service.impl;

import com.example.vstudy.constant.ECommonStatus;
import com.example.vstudy.constant.ErrorCode;
import com.example.vstudy.constant.SeqName;
import com.example.vstudy.entity.Note;
import com.example.vstudy.entity.Room;
import com.example.vstudy.entity.User;
import com.example.vstudy.exception.BusinessException;
import com.example.vstudy.model.req.CreateNoteReq;
import com.example.vstudy.model.req.UpdateNoteReq;
import com.example.vstudy.model.res.NoteRes;
import com.example.vstudy.repository.NoteRepository;
import com.example.vstudy.repository.RoomRepository;
import com.example.vstudy.service.AccountService;
import com.example.vstudy.service.NoteService;
import com.example.vstudy.service.SequenceService;
import com.example.vstudy.util.Strings;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class NoteServiceImpl implements NoteService {
    private static final int NOTE_SEQ_LENGTH = 5;
    private final NoteRepository noteRepository;
    private final RoomRepository roomRepository;
    private final AccountService accountService;
    private final SequenceService sequenceService;

    @Override
    public List<NoteRes> getNoteListByRoomAndUser(String roomCode) {
        User authUser = accountService.getAuthUser();

        Room room = roomRepository.findRoomByCodeAndStatus(roomCode, ECommonStatus.ACTIVE)
                .orElseThrow(() -> new BusinessException(ErrorCode.ROOM_NOT_FOUND, "Room not found."));

        return noteRepository.findAllByRoomAndUserAndStatus(room, authUser, ECommonStatus.ACTIVE)
                .stream().map(NoteRes::valueOf)
                .collect(Collectors.toList());
    }

    @Override
    public NoteRes createNote(CreateNoteReq req, String roomCode) {
        User authUser = accountService.getAuthUser();

        Room room = roomRepository.findRoomByCodeAndStatus(roomCode, ECommonStatus.ACTIVE)
                .orElseThrow(() -> new BusinessException(ErrorCode.ROOM_NOT_FOUND, "Room not found."));
        Note note = new Note();
        note.setRoom(room);
        note.setUser(authUser);
        note.setCode(this.buildNoteCode());
        if (StringUtils.isEmpty(req.getHeader())) {
            note.setHeader("Note");
        } else {
            note.setHeader(req.getHeader());
        }
        note.setDescription(req.getDescription());
        note.setStatus(ECommonStatus.ACTIVE);
        noteRepository.save(note);
        return NoteRes.valueOf(note);
    }

    @Override
    public NoteRes updateNote(UpdateNoteReq noteReq,String roomCode) {
        Note note = getActiveNoteByCode(noteReq.getCode());
        note.setDescription(noteReq.getDescription());
        if (noteReq.getStatus() != null) {
            note.setStatus(noteReq.getStatus());
        }
        if (noteReq.getHeader() != null) {
            note.setHeader(noteReq.getHeader());
        }
        return NoteRes.valueOf(noteRepository.save(note));
    }

    private String buildNoteCode() {
        String yy = String.valueOf(LocalDate.now().getYear()).substring(2);
        int curVal = sequenceService.getNextSeq(SeqName.NOTE_CODE, yy);
        String seqVal = Strings.formatWithZeroPrefix(curVal, NOTE_SEQ_LENGTH);

        return String.format("%s%s%s", "NOTE", yy, seqVal);
    }

    public Note getActiveNoteByCode(String noteCode) {
        return noteRepository
                .findNoteByCodeAndStatus(noteCode, ECommonStatus.ACTIVE)
                .orElseThrow(() -> new BusinessException(ErrorCode.NOTE_NOT_FOUND, "Note not found."));
    }
}
