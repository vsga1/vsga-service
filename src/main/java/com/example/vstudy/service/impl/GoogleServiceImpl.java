package com.example.vstudy.service.impl;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.example.vstudy.constant.EAccountRole;
import com.example.vstudy.constant.EAuthProvider;
import com.example.vstudy.constant.ECommonStatus;
import com.example.vstudy.constant.ErrorCode;
import com.example.vstudy.entity.User;
import com.example.vstudy.entity.UserProfile;
import com.example.vstudy.exception.BusinessException;
import com.example.vstudy.model.req.CreateAccountReq;
import com.example.vstudy.model.res.LoginRes;
import com.example.vstudy.service.AccountService;
import com.example.vstudy.service.GoogleService;
import com.example.vstudy.service.MailService;
import com.example.vstudy.service.UserProfileService;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Collections;
import java.util.Optional;
import java.util.UUID;

@Service
@AllArgsConstructor
public class GoogleServiceImpl implements GoogleService {

    private final AccountService accountService;
    private final UserProfileService userProfileService;
    private final ApplicationEventPublisher eventPublisher;


    private static final JsonFactory JSON_FACTORY = GsonFactory.getDefaultInstance();

    @Override
    @Transactional
    public LoginRes verifyToken(String token) throws GeneralSecurityException, IOException {
        final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
        GoogleIdTokenVerifier verifier = new GoogleIdTokenVerifier.Builder(new NetHttpTransport(), JSON_FACTORY)
                .setAudience(Collections.singletonList("648873905726-9po4cuvd8hd0t5r268lo6n5fr62bs07p.apps.googleusercontent.com"))
                .setIssuer("https://accounts.google.com")
                .build();

        GoogleIdToken idToken = verifier.verify(token);
        if (idToken != null) {
            GoogleIdToken.Payload payload = idToken.getPayload();

            // Print user identifier
            String userId = payload.getSubject();
            System.out.println("User ID: " + userId);

            // Get profile information from payload
            String email = payload.getEmail();
            boolean emailVerified = Boolean.valueOf(payload.getEmailVerified());
            String name = (String) payload.get("name");
            String pictureUrl = (String) payload.get("picture");
            String locale = (String) payload.get("locale");
            String familyName = (String) payload.get("family_name");
            String givenName = (String) payload.get("given_name");

            Optional<User> user = accountService.findByEmail(email);
            if (user.isPresent()) {
                Algorithm algorithm = Algorithm.HMAC512("secret".getBytes());
                String access_token = JWT.create()
                        .withSubject(user.get().getUsername())
                        .sign(algorithm);
                return LoginRes.builder()
                        .accessToken(access_token)
                        .build();
            } else {
                this.createNewAccount(CreateAccountReq
                        .builder()
                        .username(email.split("@")[0])
                        .email(email)
                        .build());
                Algorithm algorithm = Algorithm.HMAC512("secret".getBytes());
                String access_token = JWT.create()
                        .withSubject(email)
                        .sign(algorithm);
                return LoginRes.builder()
                        .accessToken(access_token)
                        .build();
            }

        } else {
            throw new BusinessException(ErrorCode.LOGIN_FAILED, "Login failed");
        }
    }

    @Transactional
    public void createNewAccount(CreateAccountReq req) {
        User user = new User();
        user.setUsername(req.getUsername());
        user.setEmail(req.getEmail());
        user.setAuthProvider(EAuthProvider.GOOGLE);
        UUID uuid = UUID.randomUUID();
        user.setCode(uuid.toString());
        user.setStatus(ECommonStatus.ACTIVE);
        user.setRole(EAccountRole.USER);
        accountService.save(user);

        UserProfile userProfile = new UserProfile();
        userProfile.setUser(user);
        userProfile.setAvatarImgUrl("https://res.cloudinary.com/huydoan1908/image/upload/v1685714074/default_avatar_yzg7zi.webp");
        userProfileService.save(userProfile);

        if (!StringUtils.isEmpty(user.getEmail())) {
            eventPublisher.publishEvent(user.getEmail());
        }
    }
}
