package com.example.vstudy.service.impl;

import com.example.vstudy.entity.UserProfile;
import com.example.vstudy.repository.UserProfileRepository;
import com.example.vstudy.service.UserProfileService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class UserProfileImpl implements UserProfileService {
    private final UserProfileRepository userProfileRepository;


    @Override
    public void save(UserProfile userProfile) {
        userProfileRepository.save(userProfile);
    }
}
