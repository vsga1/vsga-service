package com.example.vstudy.service.impl;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.example.vstudy.service.MailService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.event.TransactionalEventListener;

import java.time.Instant;
import java.util.UUID;

@Service
@AllArgsConstructor
public class MailServiceImpl implements MailService {
    @Qualifier("getClientBasePath")
    private String clientBasePath;

    @Qualifier("getClientResetPasswordEndpoint")
    private String clientResetPasswordEndpoint;

    @Qualifier("getClientActivateEmailEndpoint")
    private String clientActivateEmailEndpoint;

    private final JavaMailSender mailSender;

    @Override
    @Transactional
    @Async("backgroundProcessThreadPool")
    @TransactionalEventListener
    public void sendMailVerify(String emailAddress) {
        String token = this.buildToken(emailAddress,86400);
        String subject = "Registration Confirmation";
        String confirmationUrl = this.clientBasePath + this.clientActivateEmailEndpoint + token;

        SimpleMailMessage email = new SimpleMailMessage();
        email.setTo(emailAddress);
        email.setSubject(subject);
        email.setText("\r\n" + "Link verify email: " + confirmationUrl);
        mailSender.send(email);
    }

    @Override
    public void sendMailResetPassword(String emailAddress) {
        String token = this.buildToken(emailAddress,6000);
        String subject = "Reset Password VStudy Application";
        String hyperLink = this.clientBasePath + clientResetPasswordEndpoint + token;

        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(emailAddress);
        mailMessage.setSubject(subject);
        mailMessage.setText("\r\n" + "Link reset password: " + hyperLink);
        mailSender.send(mailMessage);
    }

    private String buildToken(String email,long expiredTime) {
        Algorithm algorithm = Algorithm.HMAC512("secret".getBytes());
        String token = JWT.create()
                .withSubject(email)
                .withExpiresAt(Instant.now().plusSeconds(expiredTime))
                .sign(algorithm);
        return token;
    }
}
