package com.example.vstudy.service.impl;

import com.example.vstudy.entity.Sequence;
import com.example.vstudy.service.SequenceService;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.HashMap;
import java.util.Map;

@Service
public class SequenceServiceImpl implements SequenceService {
    private static final String SEQ_NAME_PART = "-";

    @PersistenceContext
    private EntityManager entityManager;

    Map<String, Object> properties = new HashMap<String, Object>() {
        private static final long serialVersionUID = 6769836911719418266L;

        {
            put("javax.persistence.lock.timeout", 500L);
        }
    };

    @Override
    @Transactional
    public int getNextSeq(String key, Object... args) {
        String seqName = buildSeqName(key, args);
        Sequence confSeq = entityManager.find(Sequence.class, seqName, LockModeType.PESSIMISTIC_WRITE, properties);
        if (confSeq == null) {
            confSeq = new Sequence(seqName, 1);
        } else {
            confSeq.incr(1);
        }
        entityManager.persist(confSeq);

        return confSeq.getCurVal();
    }

    private String buildSeqName(String key, Object[] args) {
        StringBuilder builder = new StringBuilder(key);
        if (args != null) {
            for (Object obj : args) {
                builder.append(SEQ_NAME_PART).append(obj);
            }
        }
        return builder.toString();
    }
}
