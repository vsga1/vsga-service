package com.example.vstudy.service.impl;

import com.example.vstudy.constant.ECommonStatus;
import com.example.vstudy.constant.ERoomRoleType;
import com.example.vstudy.constant.ETimerStatus;
import com.example.vstudy.constant.ErrorCode;
import com.example.vstudy.entity.Room;
import com.example.vstudy.entity.RoomMember;
import com.example.vstudy.entity.RoomTimer;
import com.example.vstudy.entity.User;
import com.example.vstudy.exception.BusinessException;
import com.example.vstudy.model.req.TimerCreateReq;
import com.example.vstudy.model.req.TimerUpdateReq;
import com.example.vstudy.model.res.TimerRes;
import com.example.vstudy.repository.RoomMemberRepository;
import com.example.vstudy.repository.RoomRepository;
import com.example.vstudy.repository.RoomTimerRepository;
import com.example.vstudy.service.AccountService;
import com.example.vstudy.service.RoomTimerService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class RoomTimerServiceImpl implements RoomTimerService {
    private final AccountService accountService;
    private final RoomRepository roomRepository;
    private final RoomTimerRepository timerRepository;
    private final RoomMemberRepository roomMemberRepository;


    public Room getRoomByCode(String code) {
        return roomRepository
                .findRoomByCodeAndStatus(code, ECommonStatus.ACTIVE)
                .orElseThrow(() -> new BusinessException(ErrorCode.ROOM_NOT_FOUND, "Room not found."));
    }

    @Override
    public TimerRes createRoomTimer(TimerCreateReq req) {
        Room room = this.getRoomByCode(req.getRoomCode());
        if (timerRepository.findByRoom(room).isPresent()) {
            throw new BusinessException(ErrorCode.ROOM_TIMER_EXISTED, "Room timer existed");
        }

        RoomTimer roomTimer = new RoomTimer();
        roomTimer.setRoom(room);
        roomTimer.setUpdatedAt(req.getUpdatedAt());
        roomTimer.setStatus(ETimerStatus.IDLE);
        roomTimer.setLeftHour(req.getHour());
        roomTimer.setLeftMinute(req.getMinute());
        roomTimer.setLeftSecond(req.getSecond());

        timerRepository.save(roomTimer);

        return TimerRes.valueOf(roomTimer);
    }

    @Override
    public TimerRes getRoomTimer(String roomCode) {
        Room room = this.getRoomByCode(roomCode);
        RoomTimer roomTimer = timerRepository.findByRoom(room).orElseThrow(
                () -> new BusinessException(ErrorCode.ROOM_TIMER_NOT_FOUND, "room timer ot found")
        );

        return TimerRes.valueOf(roomTimer);
    }

    @Override
    public TimerRes updateRoomTimer(TimerUpdateReq req) {
        Room room = this.getRoomByCode(req.getRoomCode());
        User user = accountService.getAuthUser();
        RoomMember roomMember = roomMemberRepository.findRoomMemberByUserAndRoomAndStatus(user, room, ECommonStatus.ACTIVE)
                .orElseThrow(
                        () -> new BusinessException(ErrorCode.ROOM_MEMBER_NOT_FOUND, "User is not join room")
                );
        if (ERoomRoleType.MEMBER.equals(roomMember.getRoleType())) {
            throw new BusinessException(ErrorCode.ACCESS_DENIED, "Member cannot update timer");
        }
        RoomTimer roomTimer = timerRepository.findByRoom(room).orElseThrow(
                () -> new BusinessException(ErrorCode.ROOM_TIMER_NOT_FOUND, "room timer ot found")
        );
        roomTimer.setUpdatedAt(req.getUpdatedAt());
        roomTimer.setStatus(req.getStatus());
        roomTimer.setLeftHour(req.getHour());
        roomTimer.setLeftMinute(req.getMinute());
        roomTimer.setLeftSecond(req.getSecond());

        timerRepository.save(roomTimer);
        return TimerRes.valueOf(roomTimer);
    }

}
