package com.example.vstudy.service;

import com.example.vstudy.model.req.RoomNoteCreateReq;
import com.example.vstudy.model.req.RoomNoteUpdateReq;
import com.example.vstudy.model.res.RoomNoteRes;

public interface RoomNoteService {
    RoomNoteRes createRoomNote(RoomNoteCreateReq req);

    RoomNoteRes getRoomNote(String roomCode);

    RoomNoteRes updateRoomNote(RoomNoteUpdateReq req);
}
