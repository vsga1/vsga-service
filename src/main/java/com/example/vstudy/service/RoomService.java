package com.example.vstudy.service;

import com.example.vstudy.constant.ERoomRoleType;
import com.example.vstudy.entity.Room;
import com.example.vstudy.entity.RoomMember;
import com.example.vstudy.entity.User;
import com.example.vstudy.model.req.CreateRoomReq;
import com.example.vstudy.model.req.SearchRoomReq;
import com.example.vstudy.model.req.UpdateRoomPasswordReq;
import com.example.vstudy.model.req.UpdateRoomReq;
import com.example.vstudy.model.res.RoomRes;
import com.example.vstudy.model.res.RoomSummaryRes;
import com.google.api.services.drive.model.File;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.List;

public interface RoomService {
    RoomRes getRoomDetail(String code);

    Page<RoomRes> getRoomList(Pageable pageReq);

    List<RoomRes> getLatestRooms();

    Page<RoomRes> searchRoom(SearchRoomReq req,Pageable pageReq);

    Page<RoomRes> adminSearchRoom(SearchRoomReq req,Pageable pageReq);

    Page<RoomRes> getRoomListByUser(int offset, int limit);

    RoomRes createRoom(CreateRoomReq req) throws GeneralSecurityException, IOException;

    RoomRes updateRoom(String roomCode,UpdateRoomReq req);

    void updateRoomPassword(String roomCode, UpdateRoomPasswordReq req);

    Room getRoomByCode(String code);

    Room lockAndCheckRoom(String code);

    RoomMember findUserInRoom(String roomCode);

    RoomMember createRoomMember(Room room, User user, ERoomRoleType roleType);

    void memberJoinRoom(RoomMember roomMember);

    void memberOutRoom(RoomMember roomMember);

    List<RoomMember> getCurrentMembers(String roomCode);

    void createHostMember(Room room);

    void addFavoriteRoom(String roomCode);

    void removeFavoriteRoom(String roomCode);

    List<File> getRoomResources(String roomCode) throws GeneralSecurityException, IOException;

    File createResource(String roomCode, MultipartFile file) throws GeneralSecurityException, IOException;

    File createFolder(String roomCode) throws GeneralSecurityException, IOException;
    File createFolder2() throws GeneralSecurityException, IOException;

    void deleteFile(String roomCode,String fileId) throws GeneralSecurityException, IOException;

    void lockRoom(String code);

    void unlockRoom(String code);

    RoomSummaryRes roomSummary();

}
