package com.example.vstudy.service;

import com.example.vstudy.entity.Topic;
import com.example.vstudy.model.req.CreateTopicReq;
import com.example.vstudy.model.res.TopicRes;
import com.example.vstudy.support.resolver.OffsetPageable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface TopicService {
    void createTopics(CreateTopicReq req);
    void deleteTopic(String name);

    List<TopicRes> getTopics(String topic);

    List<TopicRes> suggestTopics();

    Page<TopicRes> searchTopicAdmin(String name, Pageable pageable);

    void lock(String topic);

}
