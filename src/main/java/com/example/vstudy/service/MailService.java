package com.example.vstudy.service;

public interface MailService {
    void sendMailVerify(String email);
    void sendMailResetPassword(String email);
}
