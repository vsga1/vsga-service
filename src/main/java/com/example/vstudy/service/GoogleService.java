package com.example.vstudy.service;

import com.example.vstudy.model.res.LoginRes;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;

import java.io.IOException;
import java.security.GeneralSecurityException;

public interface GoogleService {

    LoginRes verifyToken(String token) throws GeneralSecurityException, IOException;

}
