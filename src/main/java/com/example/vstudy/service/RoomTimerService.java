package com.example.vstudy.service;

import com.example.vstudy.model.req.TimerCreateReq;
import com.example.vstudy.model.req.TimerUpdateReq;
import com.example.vstudy.model.res.TimerRes;

public interface RoomTimerService {
    TimerRes createRoomTimer(TimerCreateReq req);

    TimerRes getRoomTimer(String roomCode);

    TimerRes updateRoomTimer(TimerUpdateReq req);
}
