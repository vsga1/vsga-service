package com.example.vstudy.config;

import com.example.vstudy.model.res.WhiteboardClientRes;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(value = "whiteboard", url = "https://api.netless.link/v5/rooms",configuration = FeignClientConfig.class)
public interface WhiteboardClient {
    @PostMapping("")
    WhiteboardClientRes getWhiteboardId();
}
