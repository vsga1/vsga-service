package com.example.vstudy.config;

import feign.RequestInterceptor;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;


@Configuration
@EnableFeignClients
public class FeignClientConfig {
    private static String WHITEBOARD_TOKEN = "NETLESSSDK_YWs9WC1nZ2p1Z1JRdWY5OHZnbiZub25jZT01ZjZhOTIyMC1kOWQzLTExZWQtYmQ5MS1iYjVhYTZjYzQ2NGUmcm9sZT0wJnNpZz02ZmFlZWQwN2ZmZjRjZGQwYmZiYTMzYTBlN2Y4ZmFjOTg2NzRlNmU0OGY5NjEzOTIwNmEyOTY2MWY0YTI5MjM4";

    @Bean
    RequestInterceptor requestInterceptor() {
        return requestTemplate -> {
            requestTemplate.header(HttpHeaders.CONTENT_TYPE,"application/json");
            requestTemplate.header("token",WHITEBOARD_TOKEN);
            requestTemplate.header("region","us-sv");
        };
    }
}
