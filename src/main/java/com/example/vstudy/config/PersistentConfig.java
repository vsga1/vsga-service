package com.example.vstudy.config;

import com.example.vstudy.service.AccountService;
import com.example.vstudy.support.audit.AppAuditorAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@Configuration
@EnableJpaAuditing(auditorAwareRef = "auditorProvider")
public class PersistentConfig {

    @Bean("auditorProvider")
    AuditorAware<String> auditorProvider(AccountService accountService) {
        return new AppAuditorAware(accountService);
    }
}
