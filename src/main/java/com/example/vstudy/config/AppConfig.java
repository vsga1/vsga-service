package com.example.vstudy.config;

import com.example.vstudy.security.JWTAuthorizationFilter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.List;

@Configuration
public class AppConfig {
    @Value("${app.api.ignoring.antMatchers:}")
    private String[] apiIgnoringEndpoints;

    @Value("${app.client.base-path:}")
    private  String clientBasePath;
    @Value("${app.client.api.endpoint.reset-password:}")
    private  String clientResetPasswordEndpoint;

    @Value("${app.client.api.endpoint.activate-email:}")
    private  String clientActivateEmailEndpoint;

    @Value("${app.google-drive.avatar-folder-id}")
    private String avatarFolderId;
    @Bean
    BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    JWTAuthorizationFilter permissionFilter() {
        return new JWTAuthorizationFilter(List.of(apiIgnoringEndpoints));
    }

    @Bean(name = "clientBasePath")
    public String getClientBasePath() {return this.clientBasePath;}

    @Bean(name = "clientResetPasswordEndpoint")
    public String getClientResetPasswordEndpoint() {return this.clientResetPasswordEndpoint;}

    @Bean(name = "clientActivateEmailEndpoint")
    public String getClientActivateEmailEndpoint() {return this.clientActivateEmailEndpoint;}

    @Bean(name = "avatarFolderId")
    public String getAvatarFolderId() {return this.avatarFolderId;}

}
