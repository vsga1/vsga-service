package com.example.vstudy.exception;

import com.example.vstudy.constant.ErrorCode;
import lombok.Getter;
import org.springframework.security.core.AuthenticationException;

@Getter
public class AuthException extends AuthenticationException {
    private static final long serialVersionUID = -8550021653877130833L;

    public AuthException(String msg, ErrorCode code) {
        super(msg);
        this.code = code;
    }

    private final ErrorCode code;

    public Integer getCode() {
        return this.code.getValue();
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }
}
