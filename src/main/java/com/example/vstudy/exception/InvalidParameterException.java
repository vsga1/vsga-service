package com.example.vstudy.exception;

import lombok.Getter;

@Getter
public class InvalidParameterException extends RuntimeException {
    private static final long serialVersionUID = -7152294695273573695L;

    public InvalidParameterException(String message) {
        super(message);
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }
}
