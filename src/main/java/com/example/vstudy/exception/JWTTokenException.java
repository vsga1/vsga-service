package com.example.vstudy.exception;

import com.example.vstudy.constant.ErrorCode;
import lombok.Getter;

@Getter
public class JWTTokenException extends RuntimeException {
    private static final long serialVersionUID = 1445048812692858310L;

    private final ErrorCode code;

    public JWTTokenException(ErrorCode code, String message) {
        super(message);
        this.code = code;
    }

    public Integer getCode() {
        return this.code.getValue();
    }
    @Override
    public String getMessage() {
        return super.getMessage();
    }
}
