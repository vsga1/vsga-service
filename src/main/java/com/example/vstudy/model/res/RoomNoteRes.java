package com.example.vstudy.model.res;

import com.example.vstudy.constant.ENoteStatus;
import com.example.vstudy.entity.RoomNote;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RoomNoteRes {
    private String header;
    private String description;
    private ENoteStatus status;

    public static RoomNoteRes valueOf(RoomNote roomNote) {
        return RoomNoteRes.builder()
                .header(roomNote.getHeader())
                .description(roomNote.getDescription())
                .status(roomNote.getStatus())
                .build();
    }
}
