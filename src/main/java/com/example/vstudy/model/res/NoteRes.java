package com.example.vstudy.model.res;

import com.example.vstudy.entity.Note;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
public class NoteRes {
    private String code;
    private String roomCode;
    private String header;
    private String description;

    public static NoteRes valueOf(Note note) {
        return NoteRes.builder()
                .code(note.getCode())
                .roomCode(note.getRoom().getCode())
                .header(note.getHeader())
                .description(note.getDescription())
                .build();
    }
}
