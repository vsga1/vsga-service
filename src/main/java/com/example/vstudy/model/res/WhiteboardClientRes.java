package com.example.vstudy.model.res;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
public class WhiteboardClientRes implements Serializable {
    private final static long serialVersionUID = 1281865478158499479L;

    private String uuid;
    private String teamUUID;
    private String appUUID;
    private Boolean isRecord;
}
