package com.example.vstudy.model.res;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class CurrentRoomMemberRes {
    private int currentMember;
    private List<RoomMemberRes> members;

}
