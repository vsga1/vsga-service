package com.example.vstudy.model.res;

import com.example.vstudy.constant.EGender;
import com.example.vstudy.constant.ERoomRoleType;
import com.example.vstudy.entity.RoomMember;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RoomMemberRes {
    private String username;
    private String fullName;
    private String displayName;
    private String description;
    private String avatarImgUrl;
    private EGender gender;
    private Integer age;
    private String roomCode;
    private ERoomRoleType role;

    public static RoomMemberRes valueOf(RoomMember roomMember) {
        return RoomMemberRes.builder()
                .username(roomMember.getUser().getUsername())
                .roomCode(roomMember.getRoom().getCode())
                .role(roomMember.getRoleType())
                .build();
    }
}
