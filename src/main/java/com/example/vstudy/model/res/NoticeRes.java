package com.example.vstudy.model.res;

import com.example.vstudy.constant.EEventStatus;
import com.example.vstudy.entity.Note;
import com.example.vstudy.entity.NotificationEntity;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
@AllArgsConstructor
@Builder
public class NoticeRes {
    private Integer id;
    private String title;
    private String body;
    @JsonProperty("code")
    private String payload;
    private Map<String,String> data;
    private UserProfileRes sender;
    private EEventStatus status;

    public static NoticeRes valueOf(NotificationEntity entity) {
        Map<String,String> data = new HashMap<>();
        data.put("type",entity.getEventType().toString());
        data.put("time",entity.getEventTime().toString());
        data.put("code",entity.getPayload());
        return NoticeRes.builder()
                .id(entity.getId())
                .title(entity.getTitle())
                .body(entity.getBody())
                .data(data)
                .sender(UserProfileRes.valueOf(entity.getSender(),entity.getSender().getProfile()))
                .status(entity.getStatus())
                .payload(entity.getPayload())
                .build();
    }
}
