package com.example.vstudy.model.res;

import com.example.vstudy.constant.ECommonStatus;
import com.example.vstudy.entity.Note;
import com.example.vstudy.entity.Topic;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@Builder
public class TopicRes {
    private String name;
    private String color;
    private String createdBy;
    private LocalDateTime createdAt;
    public static TopicRes valueOf(Topic topic) {
        return TopicRes.builder()
                .name(topic.getName())
                .color(topic.getColor())
                .createdAt(topic.getCreatedAt())
                .createdBy(topic.getCreatedBy())
                .build();
    }
}
