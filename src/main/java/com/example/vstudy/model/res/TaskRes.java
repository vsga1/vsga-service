package com.example.vstudy.model.res;

import com.example.vstudy.constant.ETaskStatus;
import com.example.vstudy.constant.ETaskType;
import com.example.vstudy.entity.Room;
import com.example.vstudy.entity.Task;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class TaskRes {
    private String roomCode;
    private String code;
    private String description;
    private ETaskStatus status;
    private ETaskType type;
    private String scheme;

    public static TaskRes valueOf(Task task) {
        return TaskRes.builder()
                .description(task.getDescription())
                .code(task.getCode())
                .status(task.getStatus())
                .roomCode(task.getRoom().getCode())
                .type(task.getTaskType())
                .scheme(task.getScheme())
                .build();
    }
}
