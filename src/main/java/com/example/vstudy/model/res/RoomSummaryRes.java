package com.example.vstudy.model.res;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RoomSummaryRes {
    private Integer totalActiveRoom;
    private Integer totalInactiveRoom;
    private List<TopicSummary> topicSummary;

    @Setter
    @Getter
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class TopicSummary {
        private String topic;
        private long quantity;
    }
}
