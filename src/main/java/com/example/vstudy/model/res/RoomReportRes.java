package com.example.vstudy.model.res;


import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class RoomReportRes {
    private Integer createdByDay;

}
