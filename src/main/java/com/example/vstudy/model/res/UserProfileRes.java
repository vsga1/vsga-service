package com.example.vstudy.model.res;

import com.example.vstudy.constant.ECommonStatus;
import com.example.vstudy.constant.EGender;
import com.example.vstudy.entity.User;
import com.example.vstudy.entity.UserProfile;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@Builder
public class UserProfileRes {
    private String username;
    private String code;
    private String description;
    private String displayName;
    private String email;
    private String fullName;
    private String address;
    private EGender gender;
    private int age;
    private String avatarImgUrl;
    private Boolean isFriend;
    private ECommonStatus status;
    private LocalDateTime createdAt;

    public static UserProfileRes valueOf(User user, UserProfile userProfile) {
        return UserProfileRes.builder()
                .username(user.getUsername())
                .code(user.getCode())
                .email(user.getEmail())
                .description(userProfile.getDescription())
                .displayName(userProfile.getDisplayName())
                .fullName(userProfile.getFullName())
                .gender(userProfile.getGender())
                .address(userProfile.getAddress())
                .age(userProfile.getAge() == null ? 0 : userProfile.getAge())
                .avatarImgUrl(userProfile.getAvatarImgUrl())
                .status(user.getStatus())
                .createdAt(user.getCreatedAt())
                .build();
    }
}
