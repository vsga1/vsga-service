package com.example.vstudy.model.res;

import com.example.vstudy.entity.RoomConfiguration;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RoomConfigurationRes {
    private String backgroundUrl;

    @JsonProperty(value = "is_protected")
    private boolean isProtected;

    public static RoomConfigurationRes valueOf(RoomConfiguration roomConfiguration) {
        return RoomConfigurationRes.builder()
                .backgroundUrl(roomConfiguration.getBackgroundUrl())
                .isProtected(!StringUtils.isBlank(roomConfiguration.getPassword()))
                .build();
    }
}
