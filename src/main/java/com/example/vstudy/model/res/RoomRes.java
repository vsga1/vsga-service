package com.example.vstudy.model.res;

import com.example.vstudy.constant.ECommonStatus;
import com.example.vstudy.constant.ERoomType;
import com.example.vstudy.entity.Room;
import com.example.vstudy.entity.RoomTopic;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@Builder
public class RoomRes {
    private String code;
    private String name;
    private String description;
    private ERoomType roomType;
    private int capacity;
    private int totalMember;
    @JsonProperty("is_favored")
    private boolean isFavored;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;
    private String createdBy;
    private RoomConfigurationRes configuration;
    private String storageFolderId;
    private String hostAvatarUrl;
    private String whiteboardId;
    private ECommonStatus status;
    private List<TopicRes> topics;

    public static RoomRes valueOf(Room room) {
        RoomConfigurationRes configurationRes = null;
        if(room.getConfiguration() != null) {
            configurationRes = new RoomConfigurationRes();
            configurationRes.setBackgroundUrl(room.getConfiguration().getBackgroundUrl());
            configurationRes.setProtected(!StringUtils.isBlank(room.getConfiguration().getPassword()));
        }
        List<TopicRes> topics = null;
        if(!CollectionUtils.isEmpty(room.getTopics())) {
            topics = new ArrayList<>();
            topics = room.getTopics().stream().map(item ->
            {
                return TopicRes.valueOf(item.getTopic());
            }).collect(Collectors.toList());
        }
        return RoomRes.builder()
                .code(room.getCode())
                .name(room.getName())
                .hostAvatarUrl(room.getUser().getProfile().getAvatarImgUrl())
                .description(room.getDescription())
                .roomType(room.getRoomType())
                .capacity(room.getCapacity())
                .createdAt(room.getCreatedAt())
                .createdBy(room.getCreatedBy())
                .updatedAt(room.getUpdatedAt())
                .configuration(configurationRes)
                .topics(topics)
                .storageFolderId(room.getStorageFolderId())
                .whiteboardId(room.getWhiteboardId())
                .status(room.getStatus())
                .build();
    }
}
