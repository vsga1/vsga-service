package com.example.vstudy.model.res;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserSummaryRes {
    private Integer totalActiveUser;
    private Integer totalInactiveUser;
    private List<UserPerMonth> newUsersPerMonth;

    @Setter
    @Getter
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class UserPerMonth {
        private String month;
        private Integer total;
    }
}
