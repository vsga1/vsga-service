package com.example.vstudy.model.res;

import com.example.vstudy.constant.ETimerStatus;
import com.example.vstudy.entity.RoomTimer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.OffsetDateTime;

@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TimerRes {
    private OffsetDateTime updatedAt;

    private ETimerStatus status;

    private TimerLeft timerLeft;


    @Setter
    @Getter
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public  static class TimerLeft {
        private int hour;
        private int minute;
        private int second;
    }

    public static TimerRes valueOf(RoomTimer roomTimer) {
        TimerLeft timerLeft = TimerLeft.builder()
                .hour(roomTimer.getLeftHour() != null ? roomTimer.getLeftHour() : 0)
                .minute(roomTimer.getLeftMinute() != null ? roomTimer.getLeftMinute() : 0)
                .second(roomTimer.getLeftSecond() != null ? roomTimer.getLeftSecond() : 0)
                .build();
        return TimerRes.builder()
                .updatedAt(roomTimer.getUpdatedAt())
                .status(roomTimer.getStatus())
                .timerLeft(timerLeft)
                .build();
    }
}
