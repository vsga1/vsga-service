package com.example.vstudy.model.res;

import com.example.vstudy.entity.UserProfile;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
public class DoneTaskRes {
    private int total;

    List<UserProfileRes> members;
}
