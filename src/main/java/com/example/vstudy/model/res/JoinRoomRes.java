package com.example.vstudy.model.res;

import com.example.vstudy.constant.ERoomRoleType;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class JoinRoomRes {
    private ERoomRoleType role;

    private RoomRes roomInfo;

    private int totalMember;

    private List<RoomMemberRes> members;
}
