package com.example.vstudy.model.req;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Setter
@Getter
public class DemoteReq {
    @NotBlank
    private String roomCode;

    @NotBlank
    private String username;

}
