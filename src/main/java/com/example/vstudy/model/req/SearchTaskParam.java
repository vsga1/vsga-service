package com.example.vstudy.model.req;

import com.example.vstudy.constant.ETaskStatus;
import com.example.vstudy.constant.ETaskType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SearchTaskParam {
    private String roomCode;
    private ETaskType type;
    private ETaskStatus status;
    private String scheme;
}
