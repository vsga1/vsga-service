package com.example.vstudy.model.req;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Setter
@Getter
public class UpdatePasswordReq {
    @NotNull(message = "password cannot be null")
    private String currentPassword;
    @NotNull(message = "password cannot be null")
    private String newPassword;
}
