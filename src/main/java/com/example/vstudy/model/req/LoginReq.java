package com.example.vstudy.model.req;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class LoginReq {
    private String username;
    private String password;
    private String token;
}
