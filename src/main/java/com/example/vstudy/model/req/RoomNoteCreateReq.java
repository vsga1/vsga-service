package com.example.vstudy.model.req;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Setter
@Getter
public class RoomNoteCreateReq {
    @NotBlank
    private String roomCode;
    private String header;
    private String description;

}
