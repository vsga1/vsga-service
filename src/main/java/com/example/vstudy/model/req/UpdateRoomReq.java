package com.example.vstudy.model.req;

import lombok.Getter;

@Getter
public class UpdateRoomReq {
    private String name;
}
