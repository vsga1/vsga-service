package com.example.vstudy.model.req;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Setter
@Getter
public class DeleteTaskReq {
    @NotBlank(message = "room code cannot be blank")
    private String roomCode;
    private String code;
    private String scheme;

}
