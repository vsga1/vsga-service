package com.example.vstudy.model.req;

import com.example.vstudy.constant.ETaskType;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Setter
@Getter
public class CreateTaskReq {
    @NotBlank(message = "room code cannot be blank")
    private String roomCode;

    private String description = "";

    private ETaskType type;

}
