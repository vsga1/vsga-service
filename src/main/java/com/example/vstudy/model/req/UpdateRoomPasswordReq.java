package com.example.vstudy.model.req;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class UpdateRoomPasswordReq {
    private String currentPassword;
    private String newPassword;

}
