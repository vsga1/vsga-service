package com.example.vstudy.model.req;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import java.time.OffsetDateTime;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TimerCreateReq {
    @NotBlank
    private String roomCode;

    private OffsetDateTime updatedAt;

    private Integer hour;

    private Integer minute;

    private Integer second;

}
