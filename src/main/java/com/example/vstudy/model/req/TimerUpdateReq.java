package com.example.vstudy.model.req;

import com.example.vstudy.constant.ETimerStatus;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;

@Setter
@Getter
public class TimerUpdateReq {
    @NotBlank
    private String roomCode;

    private OffsetDateTime updatedAt;

    @NotNull
    private ETimerStatus status;

    private Integer hour;

    private Integer minute;

    private Integer second;
}
