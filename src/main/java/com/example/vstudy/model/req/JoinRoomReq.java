package com.example.vstudy.model.req;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class JoinRoomReq {
    private String roomCode;
    private String password;
}
