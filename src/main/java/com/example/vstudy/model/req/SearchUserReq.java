package com.example.vstudy.model.req;

import com.example.vstudy.constant.ECommonStatus;
import com.example.vstudy.constant.ERoomType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import net.minidev.json.annotate.JsonIgnore;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.util.Set;

@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SearchUserReq {
    private String code;
    private LocalDateTime createdFrom;
    private LocalDateTime createdTo;
    private String username;
    private ECommonStatus status;
    private Boolean isEnabled;
    private String email;

}
