package com.example.vstudy.model.req;

import com.example.vstudy.constant.ERoomType;
import com.example.vstudy.entity.Topic;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import net.minidev.json.annotate.JsonIgnore;

import java.time.OffsetDateTime;
import java.util.Set;

@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SearchRoomReq {
    private String name;
    private Set<String> topics;
    private String code;
    private String keyword;
    private OffsetDateTime createdFrom;
    private OffsetDateTime createdTo;
    private String createdBy;
    private ERoomType roomType;
    @JsonIgnore
    private int userId;
    @JsonIgnore
    private String username;
    private Boolean isFavored = Boolean.FALSE;
    private Boolean isMyRoom = Boolean.FALSE;
}
