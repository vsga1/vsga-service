package com.example.vstudy.model.req;

import com.example.vstudy.constant.EGender;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.List;

@Setter
@Getter
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class UpdateProfileReq {
    private String fullName;
    private String displayName;
    private String description;
    private EGender gender;
    private Integer age;
    private String address;
    private List<TopicReq> topics;

    @Setter
    @Getter
    @NotNull
    public static class TopicReq {
        private String code;
    }
}
