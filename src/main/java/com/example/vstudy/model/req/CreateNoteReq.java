package com.example.vstudy.model.req;

import lombok.Getter;

@Getter
public class CreateNoteReq {

    private String description = "";

    private String header;

}
