package com.example.vstudy.model.req;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Getter
public class UploadAvatarReq implements Serializable {
    private static final long serialVersionUID = 315528926336040943L;
    @JsonProperty("url")
    private String url;
}
