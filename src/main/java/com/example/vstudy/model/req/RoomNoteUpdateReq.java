package com.example.vstudy.model.req;

import com.example.vstudy.constant.ENoteStatus;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Setter
@Getter
public class RoomNoteUpdateReq {
    @NotBlank
    private String roomCode;
    private String header;
    private String description;
    private ENoteStatus status;
}
