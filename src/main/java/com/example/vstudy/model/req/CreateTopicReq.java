package com.example.vstudy.model.req;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Setter
@Getter
public class CreateTopicReq {
    @NotNull
    private String name;
    private String color;
}
