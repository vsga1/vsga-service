package com.example.vstudy.model.req;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Setter
@Getter
public class AddFriendReq {

    @NotBlank(message = "username must not be blank")
    private String username;
}
