package com.example.vstudy.model.req;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Setter
@Getter
public class ResetPasswordReq {
    @NotNull(message = "token cannot be null")
    private String token;

    @NotNull(message = "password cannot be null")
    private String password;
}
