package com.example.vstudy.model.req;

import com.example.vstudy.constant.ERoomType;
import lombok.Getter;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
public class CreateRoomReq {
    @NotBlank(message = "Name is required.")
    private String name;

    private String description;

    @NotNull(message = "room type cannot be null")
    private ERoomType roomType;

    @Min(value = 1, message= "Capacity must not be less than 1.")
    @Max(value = 100, message= "Capacity must not be greater than 100.")
    private int capacity;

    private String password;

    private List<String> topics;
}
