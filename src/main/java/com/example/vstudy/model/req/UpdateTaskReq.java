package com.example.vstudy.model.req;

import com.example.vstudy.constant.ETaskStatus;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Setter
@Getter
public class UpdateTaskReq {
    @NotBlank(message = "room code cannot be blank")
    private String roomCode;

    private String code;

    private String description = "";

    private ETaskStatus status;

    private String scheme;

}
