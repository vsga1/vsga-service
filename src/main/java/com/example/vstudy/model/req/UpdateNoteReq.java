package com.example.vstudy.model.req;

import com.example.vstudy.constant.ECommonStatus;
import lombok.Getter;

@Getter
public class UpdateNoteReq {

    private String code;

    private String description = "";

    private String header;

    private ECommonStatus status = ECommonStatus.ACTIVE;

}
