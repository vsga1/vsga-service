package com.example.vstudy.constant;

import com.example.vstudy.converter.PersistentEnumConverter;
import com.example.vstudy.exception.BusinessException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.HashMap;
import java.util.Map;

public enum EAuthProvider implements PersistentEnum<String> {
    BASIC("BASIC", "BASIC"),
    GOOGLE("GOOGLE", "GOOGLE"),
    DISCORD("DISCORD", "DISCORD"),
    FACEBOOK("FACEBOOK", "FACEBOOK");

    private static final Map<String, EAuthProvider> mapByValue;

    static {
        mapByValue = new HashMap<>();
        for (EAuthProvider e : values()) {
            mapByValue.put(e.getValue(), e);
        }
    }

    private EAuthProvider(String value, String displayName) {
        this.value = value;
        this.displayName = displayName;
    }

    private final String value;

    private final String displayName;

    @Override
    @JsonValue
    public String getValue() {
        return this.value;
    }

    @JsonCreator
    public static EAuthProvider of(String value) {
        return mapByValue.get(value);
    }

    @Override
    public String getDisplayName() {
        return this.displayName;
    }

    public static class Converter extends PersistentEnumConverter<EAuthProvider, String> {
        public Converter() {
            super(EAuthProvider.class);
        }
    }

    public static class RequestParamConverter implements org.springframework.core.convert.converter.Converter<String, EAuthProvider> {
        @Override
        public EAuthProvider convert(String source) {
            EAuthProvider commonStatus = of(source);
            if (commonStatus == null) {
                throw new BusinessException(
                        ErrorCode.BAD_REQUEST,
                        String.format("Status is mismatch, status value: %s", source)
                );
            }

            return commonStatus;
        }
    }
}
