package com.example.vstudy.constant;

public interface PersistentEnum<T> {
    T getValue();

    String getDisplayName();
}