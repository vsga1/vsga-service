package com.example.vstudy.constant;

import com.example.vstudy.converter.PersistentEnumConverter;
import com.example.vstudy.exception.BusinessException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.HashMap;
import java.util.Map;

public enum ENoteStatus implements PersistentEnum<String> {
    EDIT("EDIT", "Edit"),
    OPEN("OPEN", "Open");

    private static final Map<String, ENoteStatus> mapByValue;

    static {
        mapByValue = new HashMap<>();
        for (ENoteStatus e : values()) {
            mapByValue.put(e.getValue(), e);
        }
    }

    private ENoteStatus(String value, String displayName) {
        this.value = value;
        this.displayName = displayName;
    }

    private final String value;

    private final String displayName;

    @Override
    @JsonValue
    public String getValue() {
        return this.value;
    }

    @JsonCreator
    public static ENoteStatus of(String value) {
        return mapByValue.get(value);
    }

    @Override
    public String getDisplayName() {
        return this.displayName;
    }

    public static class Converter extends PersistentEnumConverter<ENoteStatus, String> {
        public Converter() {
            super(ENoteStatus.class);
        }
    }

    public static class RequestParamConverter implements org.springframework.core.convert.converter.Converter<String, ENoteStatus> {
        @Override
        public ENoteStatus convert(String source) {
            ENoteStatus commonStatus = of(source);
            if (commonStatus == null) {
                throw new BusinessException(
                        ErrorCode.BAD_REQUEST,
                        String.format("Status is mismatch, status value: %s", source)
                );
            }

            return commonStatus;
        }
    }
}
