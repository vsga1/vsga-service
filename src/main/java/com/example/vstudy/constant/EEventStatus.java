package com.example.vstudy.constant;

import com.example.vstudy.converter.PersistentEnumConverter;
import com.example.vstudy.exception.BusinessException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.HashMap;
import java.util.Map;

public enum EEventStatus implements PersistentEnum<String> {
    READ("READ", "READ"),
    UNREAD("UNREAD", "UNREAD");

    private static final Map<String, EEventStatus> mapByValue;

    static {
        mapByValue = new HashMap<>();
        for (EEventStatus e : values()) {
            mapByValue.put(e.getValue(), e);
        }
    }

    private EEventStatus(String value, String displayName) {
        this.value = value;
        this.displayName = displayName;
    }

    private final String value;

    private final String displayName;

    @Override
    @JsonValue
    public String getValue() {
        return this.value;
    }

    @JsonCreator
    public static EEventStatus of(String value) {
        return mapByValue.get(value);
    }

    @Override
    public String getDisplayName() {
        return this.displayName;
    }

    public static class Converter extends PersistentEnumConverter<EEventStatus, String> {
        public Converter() {
            super(EEventStatus.class);
        }
    }

    public static class RequestParamConverter implements org.springframework.core.convert.converter.Converter<String, EEventStatus> {
        @Override
        public EEventStatus convert(String source) {
            EEventStatus commonStatus = of(source);
            if (commonStatus == null) {
                throw new BusinessException(
                        ErrorCode.BAD_REQUEST,
                        String.format("Status is mismatch, status value: %s", source)
                );
            }

            return commonStatus;
        }
    }
}
