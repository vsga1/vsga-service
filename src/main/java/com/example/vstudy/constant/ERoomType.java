package com.example.vstudy.constant;

import com.example.vstudy.converter.PersistentEnumConverter;
import com.example.vstudy.exception.BusinessException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.HashMap;
import java.util.Map;

public enum ERoomType implements PersistentEnum<String> {
    PUBLIC("PUBLIC", "Public"),
    PRIVATE("PRIVATE", "PRIVATE");

    private static final Map<String, ERoomType> mapByValue;

    static {
        mapByValue = new HashMap<>();
        for (ERoomType e : values()) {
            mapByValue.put(e.getValue(), e);
        }
    }

    private ERoomType(String value, String displayName) {
        this.value = value;
        this.displayName = displayName;
    }

    private final String value;

    private final String displayName;

    @Override
    @JsonValue
    public String getValue() {
        return this.value;
    }

    @JsonCreator
    public static ERoomType of(String value) {
        return mapByValue.get(value);
    }

    @Override
    public String getDisplayName() {
        return this.displayName;
    }

    public static class Converter extends PersistentEnumConverter<ERoomType, String> {
        public Converter() {
            super(ERoomType.class);
        }
    }

    public static class RequestParamConverter implements org.springframework.core.convert.converter.Converter<String, ERoomType> {
        @Override
        public ERoomType convert(String source) {
            ERoomType commonStatus = of(source);
            if (commonStatus == null) {
                throw new BusinessException(
                        ErrorCode.BAD_REQUEST,
                        String.format("Status is mismatch, status value: %s", source)
                );
            }

            return commonStatus;
        }
    }
}
