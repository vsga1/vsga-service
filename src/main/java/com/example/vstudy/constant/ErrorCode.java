package com.example.vstudy.constant;

import lombok.Getter;

@Getter
public enum ErrorCode {
    SUCCESS(200),
    NOT_FOUND(404),
    BAD_REQUEST(400),
    SERVER_ERROR(500),
    UNAUTHORIZED(401),
    ACCESS_DENIED(403),

    // User error code
    USER_NOT_FOUND(4001001),
    EMAIL_NOT_FOUND(4001002),
    LOGIN_FAILED(4001003),
    TOKEN_HAS_EXPIRED(4001004),
    EMAIL_HAS_BEEN_USED(4001005),
    USERNAME_HAS_BEEN_USED(4001006),
    CURRENT_PASSWORD_INCORRECT(4001007),
    CONFIRM_PASSWORD_MISMATCH(4001008),
    PERMISSION_DENIED(4001009),
    USER_INACTIVE(4001010),

    // Room error code
    ROOM_NOT_FOUND(4002001),
    ROOM_PASSWORD_IN_CORRECT(4002002),

    // Note error code
    NOTE_NOT_FOUND(4003001),
    NOTE_MARK_DELETED(4003002),
    ROOM_NOTE_NOT_FOUND(4003003),
    ROOM_NOTE_EXISTED(4003004),

    // Task error code
    TASK_CODE_NOT_FOUND(4004001),

    // Room member error code
    ROOM_MEMBER_NOT_FOUND(4005001),
    ROOM_IS_FULL(4005002),
    ROOM_PASSWORD_INVALID(4005003),
    MISSING_PASSWORD_VALUE(4005004),
    MEMBER_CANNOT_PROMOTE(4005005),
    CANNOT_ACTION_ON_HOST(4005006),
    ONLY_HOST_CAN_DEMOTE(4005007),
    PLEASE_WAIT_FOR_THE_HOST_TO_START(4005008),

    // Timer
    ROOM_TIMER_NOT_FOUND(4006001),
    ROOM_TIMER_EXISTED(4006002),

    // Storage
    ROOM_STORAGE_NOT_FOUND(4007001),
    FILE_NOT_FOUND(4007002),
    FILE_CANNOT_NULL(4007003),
    FILE_FORMAT_INVALID(4007004),

    // Topic
    TOPIC_NAME_IS_EXIST(4008001),
    TOPIC_NAME_INVALID_EXIST(4008002),
    TOPIC_NAME_NOT_FOUND(4008003),
    INVITE_ROOM_CODE_INVALID(4008004),
    NOTIFICATION_NOT_FOUND(4008005),
    ;



    private final int value;

    ErrorCode(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public String getName() {
        return this.name();
    }
}
