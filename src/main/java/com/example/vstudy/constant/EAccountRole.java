package com.example.vstudy.constant;

import com.example.vstudy.converter.PersistentEnumConverter;
import com.example.vstudy.exception.BusinessException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.HashMap;
import java.util.Map;

public enum EAccountRole implements PersistentEnum<String> {
    USER("USER", "User"),
    ADMIN("ADMIN", "Admin");

    private static final Map<String, EAccountRole> mapByValue;

    static {
        mapByValue = new HashMap<>();
        for (EAccountRole e : values()) {
            mapByValue.put(e.getValue(), e);
        }
    }

    private EAccountRole(String value, String displayName) {
        this.value = value;
        this.displayName = displayName;
    }

    private final String value;

    private final String displayName;

    @Override
    @JsonValue
    public String getValue() {
        return this.value;
    }

    @JsonCreator
    public static EAccountRole of(String value) {
        return mapByValue.get(value);
    }

    @Override
    public String getDisplayName() {
        return this.displayName;
    }

    public static class Converter extends PersistentEnumConverter<EAccountRole, String> {
        public Converter() {
            super(EAccountRole.class);
        }
    }

    public static class RequestParamConverter implements org.springframework.core.convert.converter.Converter<String, EAccountRole> {
        @Override
        public EAccountRole convert(String source) {
            EAccountRole commonStatus = of(source);
            if (commonStatus == null) {
                throw new BusinessException(
                        ErrorCode.BAD_REQUEST,
                        String.format("Status is mismatch, status value: %s", source)
                );
            }

            return commonStatus;
        }
    }
}
