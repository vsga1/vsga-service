package com.example.vstudy.constant;

import com.example.vstudy.converter.PersistentEnumConverter;
import com.example.vstudy.exception.BusinessException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.HashMap;
import java.util.Map;

public enum ETaskType implements PersistentEnum<String> {
    PERSONAL_TASK("PERSONAL_TASK", "PERSONAL TASK"),
    ROOM_TASK("ROOM_TASK", "ROOM TASK");

    private static final Map<String, ETaskType> mapByValue;

    static {
        mapByValue = new HashMap<>();
        for (ETaskType e : values()) {
            mapByValue.put(e.getValue(), e);
        }
    }

    private ETaskType(String value, String displayName) {
        this.value = value;
        this.displayName = displayName;
    }

    private final String value;

    private final String displayName;

    @Override
    @JsonValue
    public String getValue() {
        return this.value;
    }

    @JsonCreator
    public static ETaskType of(String value) {
        return mapByValue.get(value);
    }

    @Override
    public String getDisplayName() {
        return this.displayName;
    }

    public static class Converter extends PersistentEnumConverter<ETaskType, String> {
        public Converter() {
            super(ETaskType.class);
        }
    }

    public static class RequestParamConverter implements org.springframework.core.convert.converter.Converter<String, ETaskType> {
        @Override
        public ETaskType convert(String source) {
            ETaskType commonStatus = of(source);
            if (commonStatus == null) {
                throw new BusinessException(
                        ErrorCode.BAD_REQUEST,
                        String.format("Status is mismatch, status value: %s", source)
                );
            }

            return commonStatus;
        }
    }
}
