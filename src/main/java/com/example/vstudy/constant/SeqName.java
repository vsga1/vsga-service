package com.example.vstudy.constant;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class SeqName {
    public static final String GROUP_CODE = "room-code";
    public static final String NOTE_CODE = "note-code";
    public static final String TASK_CODE = "task-code";
    public static final String SCHEME_CODE = "scheme-code";
}
