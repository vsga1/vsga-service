package com.example.vstudy.constant;

import com.example.vstudy.converter.PersistentEnumConverter;
import com.example.vstudy.exception.BusinessException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.HashMap;
import java.util.Map;

public enum EEventType implements PersistentEnum<String> {
    FRIEND_REQUEST("FRIEND_REQUEST", "FRIEND_REQUEST"),
    JOIN_ROOM_REQUEST("JOIN_ROOM_REQUEST", "JOIN_ROOM_REQUEST"),
    DECLINE_FRIEND_REQUEST("DECLINE_FRIEND_REQUEST", "DECLINE_FRIEND_REQUEST");

    private static final Map<String, EEventType> mapByValue;

    static {
        mapByValue = new HashMap<>();
        for (EEventType e : values()) {
            mapByValue.put(e.getValue(), e);
        }
    }

    private EEventType(String value, String displayName) {
        this.value = value;
        this.displayName = displayName;
    }

    private final String value;

    private final String displayName;

    @Override
    @JsonValue
    public String getValue() {
        return this.value;
    }

    @JsonCreator
    public static EEventType of(String value) {
        return mapByValue.get(value);
    }

    @Override
    public String getDisplayName() {
        return this.displayName;
    }

    public static class Converter extends PersistentEnumConverter<EEventType, String> {
        public Converter() {
            super(EEventType.class);
        }
    }

    public static class RequestParamConverter implements org.springframework.core.convert.converter.Converter<String, EEventType> {
        @Override
        public EEventType convert(String source) {
            EEventType commonStatus = of(source);
            if (commonStatus == null) {
                throw new BusinessException(
                        ErrorCode.BAD_REQUEST,
                        String.format("Status is mismatch, status value: %s", source)
                );
            }

            return commonStatus;
        }
    }
}
