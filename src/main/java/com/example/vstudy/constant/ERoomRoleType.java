package com.example.vstudy.constant;

import com.example.vstudy.converter.PersistentEnumConverter;
import com.example.vstudy.exception.BusinessException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.HashMap;
import java.util.Map;

public enum ERoomRoleType implements PersistentEnum<String> {
    HOST("HOST", "HOST"),
    CO_HOST("CO_HOST", "CO_HOST"),
    MEMBER("MEMBER", "MEMBER");

    private static final Map<String, ERoomRoleType> mapByValue;

    static {
        mapByValue = new HashMap<>();
        for (ERoomRoleType e : values()) {
            mapByValue.put(e.getValue(), e);
        }
    }

    private ERoomRoleType(String value, String displayName) {
        this.value = value;
        this.displayName = displayName;
    }

    private final String value;

    private final String displayName;

    @Override
    @JsonValue
    public String getValue() {
        return this.value;
    }

    @JsonCreator
    public static ERoomRoleType of(String value) {
        return mapByValue.get(value);
    }

    @Override
    public String getDisplayName() {
        return this.displayName;
    }

    public static class Converter extends PersistentEnumConverter<ERoomRoleType, String> {
        public Converter() {
            super(ERoomRoleType.class);
        }
    }

    public static class RequestParamConverter implements org.springframework.core.convert.converter.Converter<String, ERoomRoleType> {
        @Override
        public ERoomRoleType convert(String source) {
            ERoomRoleType commonStatus = of(source);
            if (commonStatus == null) {
                throw new BusinessException(
                        ErrorCode.BAD_REQUEST,
                        String.format("Status is mismatch, status value: %s", source)
                );
            }

            return commonStatus;
        }
    }
}
