package com.example.vstudy.constant;

import com.example.vstudy.converter.PersistentEnumConverter;
import com.example.vstudy.exception.BusinessException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.HashMap;
import java.util.Map;

public enum ETimerStatus implements PersistentEnum<String> {
    RUN("RUN", "Run"),
    PAUSE("PAUSE", "Pause"),
    IDLE("IDLE", "Idle");

    private static final Map<String, ETimerStatus> mapByValue;

    static {
        mapByValue = new HashMap<>();
        for (ETimerStatus e : values()) {
            mapByValue.put(e.getValue(), e);
        }
    }

    private ETimerStatus(String value, String displayName) {
        this.value = value;
        this.displayName = displayName;
    }

    private final String value;

    private final String displayName;

    @Override
    @JsonValue
    public String getValue() {
        return this.value;
    }

    @JsonCreator
    public static ETimerStatus of(String value) {
        return mapByValue.get(value);
    }

    @Override
    public String getDisplayName() {
        return this.displayName;
    }

    public static class Converter extends PersistentEnumConverter<ETimerStatus, String> {
        public Converter() {
            super(ETimerStatus.class);
        }
    }

    public static class RequestParamConverter implements org.springframework.core.convert.converter.Converter<String, ETimerStatus> {
        @Override
        public ETimerStatus convert(String source) {
            ETimerStatus commonStatus = of(source);
            if (commonStatus == null) {
                throw new BusinessException(
                        ErrorCode.BAD_REQUEST,
                        String.format("Status is mismatch, status value: %s", source)
                );
            }

            return commonStatus;
        }
    }
}
