package com.example.vstudy.constant;

import com.example.vstudy.converter.PersistentEnumConverter;
import com.example.vstudy.exception.BusinessException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.HashMap;
import java.util.Map;

public enum ETaskStatus implements PersistentEnum<String> {
    UNDONE("UNDONE", "Undone"),
    DONE("DONE", "Done"),
    DELETED("DELETED", "Deleted");

    private static final Map<String, ETaskStatus> mapByValue;

    static {
        mapByValue = new HashMap<>();
        for (ETaskStatus e : values()) {
            mapByValue.put(e.getValue(), e);
        }
    }

    private ETaskStatus(String value, String displayName) {
        this.value = value;
        this.displayName = displayName;
    }

    private final String value;

    private final String displayName;

    @Override
    @JsonValue
    public String getValue() {
        return this.value;
    }

    @JsonCreator
    public static ETaskStatus of(String value) {
        return mapByValue.get(value);
    }

    @Override
    public String getDisplayName() {
        return this.displayName;
    }

    public static class Converter extends PersistentEnumConverter<ETaskStatus, String> {
        public Converter() {
            super(ETaskStatus.class);
        }
    }

    public static class RequestParamConverter implements org.springframework.core.convert.converter.Converter<String, ETaskStatus> {
        @Override
        public ETaskStatus convert(String source) {
            ETaskStatus commonStatus = of(source);
            if (commonStatus == null) {
                throw new BusinessException(
                        ErrorCode.BAD_REQUEST,
                        String.format("Status is mismatch, status value: %s", source)
                );
            }

            return commonStatus;
        }
    }
}
