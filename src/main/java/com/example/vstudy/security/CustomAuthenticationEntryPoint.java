package com.example.vstudy.security;

import com.example.vstudy.constant.ErrorCode;
import com.example.vstudy.support.web.BaseResponseData;
import com.example.vstudy.util.JsonUtil;
import lombok.AllArgsConstructor;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component("customAuthenticationEntryPoint")
@AllArgsConstructor
public class CustomAuthenticationEntryPoint implements AuthenticationEntryPoint {

    @Override
    public void commence(
            HttpServletRequest request,
            HttpServletResponse response,
            AuthenticationException authException) throws IOException, ServletException {
        BaseResponseData responseData = BaseResponseData.error(
                ErrorCode.UNAUTHORIZED.getValue(),
                "Unauthorized"
        );

        String msg = JsonUtil.writeValueAsString(responseData);
        response.setStatus(ErrorCode.UNAUTHORIZED.getValue());
        response.setContentType("application/json");
        response.getOutputStream().write(msg.getBytes());
    }
}
